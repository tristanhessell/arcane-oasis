'use strict';
/*eslint-env node, es6*/

const gulp = require('gulp');
const browserify = require('browserify');
const buffer = require('gulp-buffer');
const sourcemaps = require('gulp-sourcemaps');
const changed = require('gulp-changed');
const babel = require('babelify');
const pug = require('gulp-pug');
const uglify = require('gulp-uglify');

var source = require('vinyl-source-stream');
gulp.task('default', ['template-html', 'mv-assets', 'browjs']);

//move assets
gulp.task('mv-assets', () => {
  return gulp.src('./frontend/assets/**/*.*', {base: './frontend/'})
    .pipe(gulp.dest('./public/'))
});

gulp.task('template-html', () => {
  return gulp.src('./frontend/**/*.pug')
    .pipe(changed('./public/', {extension: '.html'}))
    .pipe(pug())
    .pipe(gulp.dest('./public/'));
});

//browserify, babelify the files in components and move them flat to public/
gulp.task('browjs', () => {
  const brows = browserify({
    entries: './frontend/app/app.module.js',
    debug:true,
    transform: [babel],
  });

  return brows.bundle()
    .pipe(source('app.module.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./public/'));
});