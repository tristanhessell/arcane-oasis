'use strict';
/* jshint esnext: true, node: true*/


/******************************************************************************
* This is a stand alone test wrapper so you can use the headless tester at the 
* command line
*
* Example Usage: node headlessTest.js -g ygo -d booster -n 2 -s file1 file2 file3
******************************************************************************/


const headless = require('./headless.js')(true);

/**
 * Reads in the arguments from the array and returns the relevant information in an object.
 * 
 * @param  {Array} args An array containing the parameters to the simulation
 * @return {Object}     Object containing the run time configuration information.
 */
var getConfig = (args) => {
	const gameType   = getArgument(args, '-g', 'ygo'    );
	const draftType  = getArgument(args, '-d', 'booster');
	const numPlayers = Number(tryGetArgument(args, '-n', 2));
	const sets       = getVariadicArgument(args, '-s', ['file1', 'file2','file3','file4']);

	return Object.freeze({
		gameType,
		draftType,
		numPlayers,
		sets
	});
};

/**
 * Prints the usage instructions - called if something goes wrong with getting the configuration settings
 * or if the -h CL flag is used.
 *
 * usage: node headless.js -g ygo -d draft -n 2 -s file1 file2 file3
 */
var printHelp = () => {
	console.log('A headless tester for the drafting system.');
	console.log('\tnode headless.js -g <gameType> -d <draftType> -n <numPlayers> -s <file1> <file2>');
	console.log('Parameters:');
	console.log('\t-h: Displays this help text');
	console.log('\t-g: Game type (YGO)');
	console.log('\t-d: Draft type (booster|rochester)');
	console.log('\t-n: Number of players ( # > 2)');
	console.log('\t-s: The sets in space-seperated form');
	console.log('NOTE: -s must be the last flag in the list. There are no other order requirements');
	console.log('Example Usage:');
	console.log('\tnode headless.js -g ygo -d draft -n 2 -s file1 file2 file3');
};

/**
 * Gets the argument from the arguments array that is situated after the flag.
 * If the flag cannot be found, an exception is thrown.
 * 
 * @param  {Array}  args An array containing flag and data values
 * @param  {String} flag String containing the CL flag
 *
 * @throws {String} If the flag cannot be found in the arguments array
 * @return {String} The value after the CL flag, the default value, or undefined 
 */
var getArgument = (args, flag) => {
	const index = args.indexOf(flag);

	//if you cant find the argument, throw an exception
	if(index === -1) {
		throw flag + ' flag is required.' + '\n ';
	}

	return args[ index + 1 ];
};

/**
 * Gets the argument after the flag from the specified arguments. If the flag cannot be found, the default value
 * (if specified) is returned.
 * 
 * @param  {Array}  args         An array containing flag and data values
 * @param  {String} flag         String containing the CL flag
 * @param  {?}      defaultValue The default value if the flag cannot be found in args
 * 
 * @return {?}                   The value after the CL flag, the default value, or undefined 
 */
var tryGetArgument = (args, flag, defaultValue) => {
	const index = args.indexOf(flag);

	//if you cant find the argument, use the default value (which may be undefined)
	if(index === -1) {
		return defaultValue;
	}

	//returns the argument that is after the flag
	return args[ index + 1 ];
};

/**
 * Gets the arguments found after the specified flag.
 * All elements in the array after the index will be returned, including any other flags and their values.
 * 
 * @param  {Array}  args         An array containing flag and data values
 * @param  {String} flag         String containing the CL flag
 * @param  {?}      defaultValue The default value if the flag cannot be found in args
 * 
 * @return {?}                   The value after the CL flag, the default value, or undefined 
 */
var getVariadicArgument = (args, flag, defaultValue) => {
	const index = args.indexOf(flag);

	//if you cant find the argument, use the default value (which may be undefined)
	if(index === -1) {
		return defaultValue;
	}

	//returns the arguments after the flag to the end of the 
	return args.slice( index + 1 );
};


//if the command line contains the help flag, print the help
if( process.argv.indexOf('-h') !== -1 ) {
	printHelp();
	//also need to stop the thing from running
}
else {
	let config;

	//if there is a problem with getting the configuration data from the CL,
	//print the help (they probably typed something wrong)
	try
	{
		config = getConfig(process.argv);
	}
	catch (ex)
	{
		printHelp();
		return;
	}

	//dont catch exceptions around here - we want to see failures
	headless.startGame( config );
}