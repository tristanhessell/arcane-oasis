/* jshint node: true, esnext: true*/

module.exports = exports = (LOG) => {
	'use strict';

	//Getting the relevant modules from other files
	const table = require('./Table.js');
	const draftConstructors = {
		rochester : require('./OpenDraft.js'   ),
		booster   : require('./BoosterDraft.js'),
	};
	const YGOgen    = require('./ygoGenerator.js'  )();
	const generator = require('./Generator.js'     )( YGOgen, require('fs') );

	/**
	 * Decorates the inSocket with the on and emit events, needed for the drafting to work properly.
	 * 
	 * @param  {Object} inSocket The socket object, at minimum needs an 'id' property
	 * @param  {OpenDraft|BoosterDraft} draft    The draft object
	 * @return {Object}          The decorated socket
	 */
	var makeRochesterSocket = (inSocket, draft) => {
		//pack is required as a variable here as the drafting is simulated through the
		//'your turn' event - which doesnt give the pack as a part of the event.
		//This all boils down to the fact that the pack is shared between all players (at once)
		//rather than passed around (like in the booster draft)
		let pack;

		//on simulates what happens when the client receives a message
		const on = (title, data) => {
			//mocking recieving a message from the server
			switch(title) {
				case "new pack":
					//when a new pack is distributed, replace the pack you have
					pack = data;
					LOG && console.log('Player ' + inSocket.id + ' received pack: ');
					//print out the pack a bit cleaner
					LOG && console.log(pack.map((card) => card.name));
				break;
				case "your turn":
					//the 'your turn' event is used to tell the user than can pick a card
					//In this case, the sim will do exactly that. 
					LOG && console.log('Player ' + inSocket.id + ' picked card \"' + pack[0].name + '\"');
					draft.pickCard(inSocket, pack[0].name);
				break;
				case "drafting over":
					//if the draft is done, just print it out.
					LOG && console.log("Drafting Over");
				break;
			}
		};

		//when emit is called on the socket, it should just cause the client to do something
		const emit = (title, data) => {
			//mocking sending a message to the 'client'
			on(title, data);
		};

		//apply the decorations to the socket
		inSocket.on = on;
		inSocket.emit = emit;

		return inSocket;
	};

	/**
	 * Takes the socket and decorates it with the on and emits function -
	 * the only requirement really needed to run the simulation.
	 * 
	 * @param  {Object} inSocket Object representing a socket. Needs a .id property minimum
	 * @param  {OpenDraft|BoosterDraft} draft    The draft object
	 * @return {Object}          The socket object, with on & emit decorations
	 */
	var makeBoosterSocket = (inSocket, draft) => {

		//on is called when the client would receive a message.
		//This is where the clients actions are simulated.
		const on = (title, data) => {
			switch(title) {
				case "new pack":
					//when you receive a new pack, pick the first card from it.
					LOG && console.log( 'Player ' + inSocket.id + ' received pack: ' );
					//print out the pack a bit cleaner
					LOG && console.log( data.map(function(card){return card.name;}) );
					//pick the actual card
					draft.pickCard( inSocket, data[0].name );
				break;
				case "drafting over":
					//when the drafting is over, just print it out.
					//The simulation will end as no other functions are called.
					LOG && console.log("Drafting Over");
				break;
			}
		};

		//when emit is called, this means that the client is going
		//to receive a message.
		//In this case, we use emit to  call on - which is how the client
		//would receive the message.
		const emit = (title, data) => {
			//mocking sending a message to the 'client'
			on(title, data);
		};

		//add the actual functions to the socket
		inSocket.on = on;
		inSocket.emit = emit;

		return inSocket;
	};


	//i hate this here
	var socketConstructors = {
		rochester : makeRochesterSocket,
		booster   : makeBoosterSocket
	};



	/**
	 * Starts the run-through of the draft by creating a draft object and adding
	 * the specified number of players to the game
	 * 
	 * The config object needs to contain:
	 * {
	 *	 gameType,      // the type of game (probably 'ygo')
	 *	 draftType,     // the type of draft
	 *	 numPlayers,    // the number of players
	 *	 sets,          // the array of set names
	 * } 
	 *
	 *
	 * @param  {Object} config Map containing the configuration data for the draft
	 */
	var startGame = (config) => {
		const draftType = config.draftType;
		const numPlayers = config.numPlayers;
		const draftConfig = {
			table: table(numPlayers),
			sets : config.sets,
			generator
			//numPlayers, packSize
		};

		if( !socketConstructors[draftType] || !draftConstructors[draftType] ) {
			throw 'Invalid draft type: ' + draftType;
		}

		//add the players to the draft - this will cause the game to start
		addPlayers( draftConstructors[draftType](draftConfig), socketConstructors[draftType], numPlayers );
	};

	/**
	 * Adds the specified number of players to the draft.
	 * 
	 * @param {BoosterDraft|OpenDraft} draft      A draft object where the game will run
	 * @param {Object} socketConstructor  The socket factory function
	 * @param {Number} numPlayers The number of players in the game
	 */
	var addPlayers = (draft, socketConstructor, numPlayers) => {
		//add the relevant number of players to the game
		for(let id = 0; id < numPlayers; id += 1) {
			LOG && console.log('Adding player with socket id ' + id);
			draft.addPlayer( socketConstructor({id: id}, draft) );
		}
	};

	return {
		startGame,
	};
};
