'use strict';
/*eslint-env node, es6*/

//variable used to show that logging should be on/off.
//used so that logging isnt on when tests are running.
const express = require('express');
const router  = express.Router();
const jwt     = require('jsonwebtoken');

const accountClient = require('../server/AccountClient.js');

const SECRET = 'secret';

router.use((req, res, next) => {
  console.log(`${req.method}: ${req.originalUrl}`);
  next();
});

//get token
router.post('/login', (req, res) => {
  const {username, password} = req.body;
  const userObj = {username, password};

  // find the user
  accountClient.validateAccount(userObj).then(() => {
    // if user is found and password is right create a token
    const token = jwt.sign(userObj, SECRET, {
      expiresIn: '1d', // expires in 24 hours
    });

    // return the information including token as JSON
    res.json({
      username: username,
      token,
    });
  }).catch((e) => {
    console.log(e);
    res.sendStatus(401);
  });
});

router.use('/users', require('./accounts.js'));

// TODO move this out
router.post('/auth', (req, res) => {
  // check header or url parameters or post parameters for token
  const token = req.body.token ||
    req.query.token ||
    req.headers.authorization;

  // decode token
  if (token) {
    // verifies secret and checks exp
    return jwt.verify(token, SECRET, (err) => {
      if (err) {
        return res
          .status(400)
          .json({
            success: false,
            message: 'Failed to authenticate token',
          });
      }

      res.sendStatus(200);
    });
  }

  res.sendStatus(401);
});

module.exports = router;
