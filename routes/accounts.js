'use strict';
/*eslint-env node, es6*/

const express = require('express');
const router  = express.Router();
const accountClient = require('../server/AccountClient.js');

router.get('/:username', (req,res) => {
  const {username} = req.params;

  accountClient.checkUser(username)
    .then(() => {
      res.status(200).send({found: true});
    })
    .catch(() => {
      res.status(200).send({found: false});
    });
});

//add user
router.post('/', (req, res) => {
  accountClient.addAccount({
    username: req.body.username,
    password: req.body.password,
  }).then(() => {
    res.send({
      //
    });
  }).catch((err) => {
    //TODO i dont think 404 is correct here
    console.log(err);
    res.status(404).send({
      err,
    });
  });
});

//update user
router.put('/:username', (req, res) => {
  accountClient.updateAccount({
    username: req.params.username,
    password: req.body.password,
  }).then(() => {
    res.send({
      //
    });
  }).catch(() => {
    res.sendStatus(404);
  });
});

router.delete('/', (req, res) => {
  accountClient.deleteAccount(req.body)
    .then(() => {
      //send back saying that it removed the account
      res.send({
        message: 'ok',
      });
    })
    .catch((message) => {
      res.status(400).send({
        message,
      });
    });
});

module.exports = router;