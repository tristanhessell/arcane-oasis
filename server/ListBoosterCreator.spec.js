'use strict';
/*eslint-env node, es6, mocha*/

const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const boosterCreator = require('./ListBoosterCreator.js');

describe('ListBoosterCreator', () => {
  const twentyCards = [
    { name: "Asura Priest", cardType:"Effect", type:""},
    { name: "Battle Fader", cardType:"Effect", type:""},
    { name: "Beast King Barbaros", cardType:"Effect", type:""},
    { name: "Blackwing - Zephyros the Elite", cardType:"Effect", type:""},
    { name: "Blue Thunder T-45", cardType:"Effect", type:""},
    { name: "Breaker the Magical Warrior", cardType:"Effect", type:""},
    { name: "Caius the Shadow Monarch", cardType:"Effect", type:""},
    { name: "Card Guard", cardType:"Effect", type:""},
    { name: "Card Trooper", cardType:"Effect", type:""},
    { name: "Cardcar D", cardType:"Effect", type:""},
    { name: "Chaos Sorcerer", cardType:"Effect", type:""},
    { name: "Cyber Dragon", cardType:"Effect", type:""},
    { name: "Cyber Jar", cardType:"Effect", type:""},
    { name: "D.D. Assailant", cardType:"Effect", type:""},
    { name: "D.D. Warrior Lady", cardType:"Effect", type:""},
    { name: "Don Zaloog", cardType:"Effect", type:""},
    { name: "Doomcaliber Knight", cardType:"Effect", type:""},
    { name: "Ehren, Lightsworn Monk", cardType:"Effect", type:""},
    { name: "Evilswarm Thunderbird", cardType:"Effect", type:""},
    { name: "Gorz the Emissary of Darkness", cardType:"Effect", type:""},
  ];

  const mockCreator = (inCards) => () => Promise.resolve(inCards);


  describe('#getPacks()', () => {
    it('should return packs at the specified size', () => {
      const numPacks = 5;
      const packSize = 4;
      const cards = twentyCards.slice();
      boosterCreator(mockCreator(cards))(numPacks, [''], packSize)
        .then((packs) => {
          assert.equal(numPacks, packs.length);
          assert.equal(true, packs.every((pack) => pack.length === packSize));
        });
    });

    it('should return packs if extra cards are provided', () => {
      const numPacks = 5;
      const packSize = 2;
      const cards = twentyCards.slice();
      boosterCreator(mockCreator(cards))(numPacks, [''], packSize)
        .then((packs) => {
          assert.equal(numPacks, packs.length);
          assert.equal(true, packs.every((pack) => pack.length === packSize));
        });
    });

    it('should throw an error if not enough cards are provided', () => {
      const numPacks = 5;
      const packSize = 8;
      const cards = twentyCards.slice();

      expect(() => {
        boosterCreator(mockCreator(cards))(numPacks, [''], packSize)
          .to.throw('not enough cards');
      });
    });
  });
});
