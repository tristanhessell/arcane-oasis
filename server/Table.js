'use strict';
/*eslint-env node, es6*/
const listMap = require('./ListMap.js');

const MIN_PLAYERS = 2;

module.exports = exports = function(maxPlayers) {
  if (isNaN(maxPlayers) || maxPlayers < MIN_PLAYERS) {
    throw `invalid table size ${maxPlayers}`;
  }

  //this is mutable because we replace if the table is
  //randomised
  let players = listMap();

  const getMaxPlayers = () => maxPlayers;
  const getNumPlayers = () => players.length();
  const isFull = () => getNumPlayers() === maxPlayers;
  const isEmpty = () => getNumPlayers() === 0;

    //return wouldnt be necessart if name was passed in
  const addPlayer = (playerObject) => {
    if (isFull()) {
      throw 'cannot add more players: table is full';
    }

    players.add(playerObject.socket.id, playerObject);
  };

  const removePlayer = (scktID) => players.remove(scktID);

  const getPlayer = (scktID) => players.get(scktID);

  const getNextPlayer = (player, direction) =>
    players.next(player.socket.id, direction);

  const getPlayerNames = () => players.map((player) => player.name);

  const getFirstPlayer = () => players.getHead();

  const broadcastMessage = (title, data) => {
    players.forEach((player) => {
      player.socket.emit(title, data);
    });
  };

  //func takes the current player and the next player as the parameters
  //doing it like the makes it possible to iterate either way without hassle
  //also just learning about tail recursion
  const forEach = (func, inDirection) => {
    players.forEach(func, inDirection);
  };

  //TODO add direction to this
  function map (func) {
    return players.map(func);
  }

  //use the old list to create a new one
  const randomise = () => {
    const oldOrder = players.map((player) => {
      return {
        key: player.socket.id,
        value: player,
      };
    });

    players = listMap(shuffleArray(oldOrder));
  };

  function shuffleArray (inArr) {
    for (let i = 0; i < inArr.length; i += 1) {
      const j = Math.floor(Math.random() * (i + 1));

      [inArr[i], inArr[j]] = [inArr[j], inArr[i]];
    }

    return inArr;
  }

  return {
    broadcastMessage,
    getFirstPlayer,
    getPlayer,
    getNextPlayer,
    getPlayerNames,
    removePlayer,
    addPlayer,
    isEmpty,
    isFull,
    getMaxPlayers,
    forEach,
    map,
    randomise,
  };
};
