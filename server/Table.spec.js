'use strict';
/*eslint-env node, es6, mocha*/

const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;

describe('Table', () => {
  const createTable = require('./Table.js');
  let table;
  let actualData = [];

  const createPlayer = (sckid, packData) =>{
    return {
      socket:{
        id: sckid,
        emit : (title, data) => {
          actualData.push({
            'title': title,
            'data' : data,
          });
        }},
      pack: packData,
      nextPack: [],
    };
  };

  afterEach('After each table test', () => {
    actualData = [];
  });

  describe('#getMaxPlayers() & #setMaxPlayers()', () => {
    it('Should return the number that is set & get', () => {
      table = createTable(2);
      assert.equal(2, table.getMaxPlayers());
    });

    it('should throw exception if < 2 is set', () => {
      expect(() => {createTable(1);})
        .to.throw('invalid table size 1');
    });

    it('should throw exception if NaN is set', () => {
      expect(() => { createTable(NaN);})
        .to.throw('invalid table size NaN');
      expect(() => { createTable('lel');})
        .to.throw('invalid table size lel');
    });
  });

  describe('#addPlayer() & #getPlayer()', () => {
    it('should return player if the player is on the table', () => {
      table = createTable(2);

      table.addPlayer({ socket : { id : 13 } });
      assert.equal(13, table.getPlayer(13).socket.id);
    });

    it('should throw expection is adding more than maxPlayers', () => {
      table = createTable(2);

      table.addPlayer({socket:{id:1} });
      table.addPlayer({socket:{id:2} });
      expect(() => { table.addPlayer({socket:{id:3} });})
        .to.throw('cannot add more players: table is full');
    });


    it('should throw exception if the player is not at the table', () => {
      table = createTable(2);
      table.addPlayer({ socket : { id : 13 } });
      expect(() => { table.getPlayer(-13);})
        .to.throw(`key -13 is not in list`);
    });
  });

  describe('#getNextPlayer()', () => {
    it('should get the left player to the specified player', () => {
      const p1 = createPlayer(1);
      const p2 = createPlayer(2);
      const p3 = createPlayer(3);

      table = createTable(3);
      table.addPlayer(p1);
      table.addPlayer(p2);
      table.addPlayer(p3);

      assert.equal(p3, table.getNextPlayer(p1, 'left'));
      assert.equal(p1, table.getNextPlayer(p2, 'left'));
      assert.equal(p2, table.getNextPlayer(p3, 'left'));
    });

    it('should get the right player to the specified player', () => {
      const p1 = createPlayer(1);
      const p2 = createPlayer(2);
      const p3 = createPlayer(3);

      table = createTable(3);
      table.addPlayer(p1);
      table.addPlayer(p2);
      table.addPlayer(p3);

      assert.equal(p2, table.getNextPlayer(p1, 'right'));
      assert.equal(p3, table.getNextPlayer(p2, 'right'));
      assert.equal(p1, table.getNextPlayer(p3, 'right'));
    });

    it('should throw exception if the player is not at the table', () => {
      const p1 = createPlayer(1);
      const p2 = createPlayer(2);
      const p3 = createPlayer(3);

      table = createTable(2);
      table.addPlayer(p1);
      table.addPlayer(p2);

      expect(() => { table.getNextPlayer(p3, 'left');})
        .to.throw(`no object specified by key ${3}`);
    });
  });

  describe('#removePlayer()', () => {
    it('should return if the player is on the table', () => {
      table = createTable(3);

      table.addPlayer({ socket : { id : 13 }});
      table.addPlayer({ socket : { id : 14 }});
      table.addPlayer({ socket : { id : 15 }});

      assert.deepEqual({ socket : { id : 13 }}, table.removePlayer(13));
      assert.deepEqual({ socket : { id : 14 }}, table.removePlayer(14));
      assert.deepEqual({ socket : { id : 15 }}, table.removePlayer(15));
    });

    it('should false if the player is not at the table', () => {
      table = createTable(2);
      table.addPlayer({socket : { id : 13 }});
      expect(() => { table.removePlayer(-13); })
        .to.throw('cannot remove key not present');
    });

    it('should remove a player correctly (removing the middle case)', () => {
      table = createTable(3);
      table.addPlayer({ socket : { id : 13 }});
      table.addPlayer({ socket : { id : 14 }});
      table.addPlayer({ socket : { id : 15 }});

      assert.deepEqual({ socket : { id : 14 }}, table.removePlayer(14));

      let players = [];

      table.forEach((player) => { players.push(player.socket.id); }, 'right');

      assert.deepEqual(players, [13, 15]);
    });
  });


  describe('#isFull()', () => {
    it('should return false if not full', () => {
      table = createTable(2);
      assert.equal(false, table.isFull());
    });

    it('should return true if full', () => {
      table = createTable(2);
      table.addPlayer({socket:{id:1} });
      table.addPlayer({socket:{id:2} });
      assert.equal(true, table.isFull());
    });
  });

  describe('#isEmpty()', () => {
    it('should return false if not empty', () => {
      table = createTable(2);
      table.addPlayer({socket:{id:1} });
      assert.equal(false, table.isEmpty());
    });

    it('should return true if empty', () => {
      table = createTable(2);
      assert.equal(true, table.isEmpty());
    });
  });

  describe('#broadcastMessage()', () => {
    it('should send message to all players - no data', () => {
      const expectedData = [{
        'title': 'test title',
        'data' : undefined,
      }, {
        'title': 'test title',
        'data' : undefined,
      }];

      table = createTable(2);
      table.addPlayer(createPlayer(1));
      table.addPlayer(createPlayer(2));

      table.broadcastMessage('test title');
      //check that you have got the message
      assert.deepEqual(expectedData,  actualData);
    });

    it('should send message to all players - with data', () => {
      const expectedData = [{
        'title': 'test title',
        'data' : 13,
      }, {
        'title': 'test title',
        'data' : 13,
      }];

      table = createTable(2);
      table.addPlayer(createPlayer(1,13));
      table.addPlayer(createPlayer(2,13));

      table.broadcastMessage("test title", 13);
      //check that you have got the message
      assert.deepEqual(expectedData,  actualData);
    });
  });

  describe('#getPlayerNames()', () => {
    it('should return the names of the players in order', () => {
      const expected = ['P1', 'P2', 'P3', 'P4', 'P5'];

      table = createTable(5);

      table.addPlayer({socket:{id:1  }, name: 'P1' });
      table.addPlayer({socket:{id:'a'}, name: 'P2' });
      table.addPlayer({socket:{id:3  }, name: 'P3' });
      table.addPlayer({socket:{id:'z'}, name: 'P4' });
      table.addPlayer({socket:{id:5  }, name: 'P5' });
      assert.deepEqual(expected, table.getPlayerNames());
    });
  });

  describe('#forEach()', () => {
    var testFunction = (player) => {
      actualData.push(player.socket.id);
    };

    it('should forEach the table left', () => {
      const expected = [1,3,2];

      table = createTable(3);
      table.addPlayer({socket:{id:1} });
      table.addPlayer({socket:{id:2} });
      table.addPlayer({socket:{id:3} });

      table.forEach(testFunction, 'left');
      assert.deepEqual(actualData,expected);
    });

    it('should forEach the table right', () => {
      const expected = [1,2,3];

      table = createTable(3);
      table.addPlayer({socket:{id:1} });
      table.addPlayer({socket:{id:2} });
      table.addPlayer({socket:{id:3} });

      table.forEach(testFunction, 'right');
      assert.deepEqual(actualData,expected);
    });

    it('should forEach the table right by default', () => {
      const expected = [1,2,3];

      table = createTable(3);
      table.addPlayer({socket:{id:1} });
      table.addPlayer({socket:{id:2} });
      table.addPlayer({socket:{id:3} });

      table.forEach(testFunction);
      assert.deepEqual(actualData,expected);
    });
  });

});
