'use strict';
/*eslint-env node, es6, mocha*/

/*
These tests are not doing much - just making sure that http promises
get resolved/rejected.
This is because the client itself doesnt do too much - its
pretty much just an interface for the account microservice
*/

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const sinon = require('sinon');
const http = require('http');
const PassThrough = require('stream').PassThrough;

const acctClient = require('./AccountClient.js');

describe('AccountClient', () => {

  describe('#addAccount()', () => {
    afterEach(() => {
      http.request.restore();
    });

    it('should work properly', () => {
      const inParams = {};
      const response = new PassThrough();

      response.statusCode = 201;
      response.end();

      sinon.stub(http,'request')
        .callsArgWith(1, response)
        .returns(new PassThrough());

      return expect(acctClient.addAccount(inParams))
        .fulfilled;
    });

    it('should work when error', (done) => {
      const inParams = {};
      const request = new PassThrough();
      const expected = 'expected';

      sinon.stub(http,'request').returns(request);

      expect(acctClient.addAccount(inParams))
        .eventually
        .rejected
        .notify(done);

      request.emit('error', expected);
    });
  });

  describe('#updateAccount()', () => {

    afterEach(() => {
      http.request.restore();
    });

    it('should work', () => {
      const inParams = {};
      const response = new PassThrough();

      response.statusCode = 204;
      response.end();

      sinon.stub(http,'request')
        .callsArgWith(1, response)
        .returns(new PassThrough());

      return expect(acctClient.updateAccount(inParams))
        .fulfilled;
    });

    it('should fail', (done) => {
      const inParams = {};
      const request = new PassThrough();
      const expected = 'expected';

      sinon.stub(http,'request')
        .returns(request);

      expect(acctClient.updateAccount(inParams))
        .eventually
        .rejected
        .notify(done);

      request.emit('error', expected);
    });
  });

  describe('#validateAccount()', () => {

    afterEach(() => {
      http.request.restore();
    });

    it('should work', () => {
      const inParams = {};
      const response = new PassThrough();

      response.statusCode = 204;
      response.end();

      sinon.stub(http,'request')
        .callsArgWith(1, response)
        .returns(new PassThrough());

      return expect(acctClient.updateAccount(inParams))
        .fulfilled;
    });

    it('should fail', (done) => {
      const inParams = {};
      const request = new PassThrough();
      const expected = 'expected';

      sinon.stub(http,'request')
        .returns(request);

      expect(acctClient.updateAccount(inParams))
        .eventually
        .rejected
        .notify(done);

      request.emit('error', expected);
    });
  });
});
