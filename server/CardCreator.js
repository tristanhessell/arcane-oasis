'use strict';
/*eslint-env node, es6*/

module.exports = exports = (gameType, dbClient) =>
  (selectedSets, draftFormat, owner) => {
    const proms = selectedSets.map((setName) => dbClient.getCards(gameType, draftFormat, setName, owner));
    return Promise.all(proms)
      .then((sets) =>
        sets.reduce((prev, curr) => prev.concat(curr), [])
      );
  };