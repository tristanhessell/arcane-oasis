'use strict';
/*eslint-env node, es6*/
const winston = require('winston');

module.exports = exports = function(config) {
  let turnStack;
  let currentPack;
  let startingPlayer;
  let finished = false;
  let started = false;

  const {table, packs} = config;

  const hasStarted = () => started;
  const isFinished = () => finished;

  const startDraft = (inTable, inPacks) => {
    winston.info('draft started');
    table.randomise();
    table.broadcastMessage('all players', table.getPlayerNames());
    started = true;
    currentPack = inPacks.pop();
    startingPlayer = inTable.getFirstPlayer();
    turnStack = startNewRound(inTable, currentPack, startingPlayer);
  };

  const finishGame = (inTable) => {
    winston.info('drafting is over');
    inTable.broadcastMessage('draft over');
    finished = true;
  };

  const startNewRound = (inTable, pack, startPlayer) => {
    if (!pack) {
      finishGame(inTable);
      return;
    }

    inTable.broadcastMessage("new pack", pack);
    inTable.broadcastMessage("new turn player", startPlayer.name);

        //load the turn stack based off of that players
    const turnOrder = loadTurnStack(inTable, startPlayer);
        //we already know who the first player is
    turnOrder.pop();

    return turnOrder;
  };

  const startNewTurn = (inTable, playerName) => {
    inTable.broadcastMessage('new turn player', playerName);
  };

  const loadTurnStack = (inTable, startPlayer) => {
    return (function recurse(player, stack) {
      //add the player when you get to the node
      stack.push(player);
      const nextPlayer = inTable.getNextPlayer(player,'right');

      //if you havent looped around to the starting player
      if (nextPlayer !== startPlayer) {
        //add the person to the players left
        recurse(nextPlayer, stack);
      }
      //add the player when you are leaving the node
      stack.push(player);

      return stack;
    }(startPlayer, []));
  };

  const addPlayer = (socket, name) => {
    if (!socket) {
      throw 'cannot add player: no socket included';
    }

    table.addPlayer({
      name,
      socket,
    });

    table.broadcastMessage('all players', table.getPlayerNames());

    if (table.isFull()) {
      table.broadcastMessage('game started');
      startDraft(table, packs);
    }
  };

  const pickCard = (socket, cardName) => {
    const cardIndex = currentPack.findIndex((e) => e.name === cardName);

    if (-1 === cardIndex) {
      //card wasnt in the pack
      throw 'card was not in pack';
    }

    //remove the card from the pack
    currentPack.splice(cardIndex, 1);

    //tell everyone who removed a card
    table.broadcastMessage('card taken', {
      cardName,
      playerName: table.getPlayer(socket.id).name,
    });

    //get the next player
    const turnPlayer = turnStack.pop();

    //if there are no more players in the round
    if (!turnPlayer) {
      //get a new pack (ditching any remaining cards)
      currentPack = packs.pop();
      startingPlayer = table.getNextPlayer(startingPlayer,'right');
      turnStack = startNewRound(table, currentPack, startingPlayer);
      return;
    }

    startNewTurn(table, turnPlayer.name);
  };

  return {
    addPlayer,
    pickCard,
    isFinished,
    hasStarted,
  };
};
