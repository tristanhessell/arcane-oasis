'use strict';
/*eslint-env node, es6, mocha*/

const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;

//TODO fix these tests - structure
//TODO fix these tests - E2E tests cover a lot of this
describe('RochesterDraft', () => {
  const createDraft = require('./RochesterDraft.js');
  let draft;
  let sentSocketData = [];

  const mockCreator = {
    getPacks : () => {
      return [
        [{ name: 0  }, { name: 1 },
         { name: 2  }, { name: 3 },
         { name: 4  }, { name: 5 }],
        [{ name: 6  }, { name: 7 },
         { name: 8  }, { name: 9 },
         { name: 10 }, { name: 11}],
      ];
    },
  };

  const createMockTable = () =>{
    const obj = {
      addPlayer : (player) => {
        if (obj.players.length !== 0) {
          const tailIndex = obj.players.length-1;

          obj.players[0].left = player; //head.left = new node
          obj.players[tailIndex].right = player; //tail.right = new node
          player.left = obj.players[tailIndex]; //new node.left = tail
          player.right = obj.players[0]; //new node.right = head;
        }
        obj.players.push(player);
      },
      broadcastMessage: (title, data) => {
        obj.players.forEach((player) => {
          player.socket.emit(title, data);
        });
      },
      getPlayer: (socketID) => obj.players.filter((player) => player.socket.id === socketID)[0],
      getMaxPlayers: () => 3,
      resetTable : () => { sentSocketData.push('reset called'); },
      isFull: () => obj.players.length === 3,
      getRandomPlayer : () => obj.players[0],
      getNextPlayer : (inPlayer, direction) => inPlayer[direction],
      forEach : (func) => { obj.players.forEach((player) => { func(player); }); },
      randomise : () => {},
      getFirstPlayer: () => obj.players[0],
    };

    obj.players = [];

    return obj;
  };

  var createMockSocket = (scktID) => {
    return {
      id: scktID,
      emit : (msg, data) => {
        const d = data instanceof Array ? data.slice(0) : data;

        sentSocketData.push({
          title: msg,
          data: d,
        });
      },
    };
  };

  beforeEach('Before each open drafts test', () => {
    const config = {
      table: createMockTable(),
      packs: mockCreator.getPacks(),
    };

    draft = createDraft(config);
    sentSocketData.length = 0;
  });

  describe('#addPlayer()', () => {
    it('should only send packs once the game is full', () => {
      const preFullExpected = [
        {title: 'player name', data: 'Player 1'},
        {title: 'player name', data: 'Player 2'},
      ];

      const expected = [
        {title: 'player name', data: 'Player 1'},
        {title: 'player name', data: 'Player 2'},
        {title: 'player name', data: 'Player 3'},
        //not sure why, but data: undefined is needed to pass test
        {title: 'game started', data: undefined},
        {title: 'game started', data: undefined},
        {title: 'game started', data: undefined},
        {title: 'new pack', data: [{ name: 6  }, { name: 7 },
                                   { name: 8  }, { name: 9 },
                                   { name: 10 }, { name: 11}]},
        {title: 'new pack', data: [{ name: 6  }, { name: 7 },
                                   { name: 8  }, { name: 9 },
                                   { name: 10 }, { name: 11}]},
        {title: 'new pack', data: [{ name: 6  }, { name: 7 },
                                   { name: 8  }, { name: 9 },
                                   { name: 10 }, { name: 11}]},
        {title: 'new turn player', data: 'Player 1'},
        {title: 'new turn player', data: 'Player 1'},
        {title: 'new turn player', data: 'Player 1'},
      ];

      draft.addPlayer(createMockSocket(1), 'Player 1');
      draft.addPlayer(createMockSocket(2), 'Player 2');
      assert.deepEqual(sentSocketData, preFullExpected);
      draft.addPlayer(createMockSocket(3), 'Player 3');

      //assert: pack has been sent to all 3 players
      //'your turn' has been sent to a player
      //'new turn player' has been sent to all 3 people
      assert.deepEqual(sentSocketData, expected);
    });


    it('should throw exception if the socket isnt passed in', () => {
      expect(() => {draft.addPlayer();}).to.throw('cannot add player: no socket included');
    });
  });

  describe('#pickCard()', () => {
    it('should play through properly with 3 players', () => {
      const p1 = createMockSocket(1);
      const p2 = createMockSocket(2);
      const p3 = createMockSocket(3);

      //add the players.
      draft.addPlayer(p1, 'Player 1');
      draft.addPlayer(p2, 'Player 2');
      draft.addPlayer(p3, 'Player 3');

      //turn stack: 1,3,2,2,3,1
      //card picks
      draft.pickCard(p2, 11);
      draft.pickCard(p3, 10);
      draft.pickCard(p1, 9);
      draft.pickCard(p1, 8);
      draft.pickCard(p3, 7);
      draft.pickCard(p2, 6);

      //turn stack: 3,2,1,1,2,3
      //new pack - picks
      draft.pickCard(p3, 5);
      draft.pickCard(p1, 4);
      draft.pickCard(p2, 3);
      draft.pickCard(p2, 2);
      draft.pickCard(p1, 1);

      assert.equal(false, draft.isFinished());

      draft.pickCard(p3, 0);

      assert.equal(true, draft.isFinished());

      //game end
      const exp = [
        {title: 'player name',data: 'Player 1'},
        {title: 'player name',data: 'Player 2'},
        {title: 'player name',data: 'Player 3'},
        //not sure why, but data: undefined is needed to pass test
        {title: 'game started', data: undefined},
        {title: 'game started', data: undefined},
        {title: 'game started', data: undefined}, {
          title: 'new pack',
          data: [
            { name: 6  }, { name: 7 },
            { name: 8  }, { name: 9 },
            { name: 10 }, { name: 11},
          ],
        }, {
          title: 'new pack',
          data: [
            { name: 6  }, { name: 7 },
            { name: 8  }, { name: 9 },
            { name: 10 }, { name: 11},
          ],
        }, {
          title: 'new pack',
          data: [
            { name: 6  }, { name: 7 },
            { name: 8  }, { name: 9 },
            { name: 10 }, { name: 11},
          ],
        },
        {title: 'new turn player', data: 'Player 1'},
        {title: 'new turn player', data: 'Player 1'},
        {title: 'new turn player', data: 'Player 1'}, {
          title: "card taken",
          data: { "cardName" : 11, "playerName": 'Player 2'},
        }, {
          title: "card taken",
          data: { "cardName" : 11, "playerName": 'Player 2'},
        },{
          title: "card taken",
          data: { "cardName" : 11, "playerName": 'Player 2'},
        },
        {title: "new turn player", data: 'Player 2'},
        {title: "new turn player", data: 'Player 2'},
        {title: "new turn player", data: 'Player 2'},
        {title: "card taken", data: {
          "cardName" : 10,
          "playerName": 'Player 3',
        }},
        {title: "card taken", data: {
          "cardName" : 10,
          "playerName": 'Player 3',
        }},
        {title: "card taken", data: {
          "cardName" : 10,
          "playerName": 'Player 3',
        }},
        {title: "new turn player", data: 'Player 3'},
        {title: "new turn player", data: 'Player 3'},
        {title: "new turn player", data: 'Player 3'},
        {title: "card taken", data: {
          "cardName" : 9,
          "playerName": 'Player 1',
        }},
        {title: "card taken", data: {
          "cardName" : 9,
          "playerName": 'Player 1',
        }},
        {title: "card taken", data: {
          "cardName" : 9,
          "playerName": 'Player 1',
        }},
        {title: "new turn player", data: 'Player 3'},
        {title: "new turn player", data: 'Player 3'},
        {title: "new turn player", data: 'Player 3'},
        {title: "card taken", data: {
          "cardName" : 8,
          "playerName": 'Player 1',
        }},
        {title: "card taken", data: {
          "cardName" : 8,
          "playerName": 'Player 1',
        }},
        {title: "card taken", data: {
          "cardName" : 8,
          "playerName": 'Player 1',
        }},
        {title: "new turn player", data: 'Player 2'},
        {title: "new turn player", data: 'Player 2'},
        {title: "new turn player", data: 'Player 2'},
        {title: "card taken", data: {
          "cardName" : 7,
          "playerName": 'Player 3',
        }},
        {title: "card taken", data: {
          "cardName" : 7,
          "playerName": 'Player 3',
        }},
        {title: "card taken", data: {
          "cardName" : 7,
          "playerName": 'Player 3',
        }},
        {title: "new turn player", data: 'Player 1'},
        {title: "new turn player", data: 'Player 1'},
        {title: "new turn player", data: 'Player 1'},
        {title: "card taken", data: {
          "cardName" : 6,
          "playerName": 'Player 2',
        }},
        {title: "card taken", data: {
          "cardName" : 6,
          "playerName": 'Player 2',
        }},
        {title: "card taken", data: {
          "cardName" : 6,
          "playerName": 'Player 2',
        }}, {
        //////////////////////////////////////////////////////round two
          title: 'new pack',
          data: [
            { name: 0  }, { name: 1 },
            { name: 2  }, { name: 3 },
            { name: 4  }, { name: 5 },
          ],
        }, {
          title: 'new pack',
          data: [
            { name: 0  }, { name: 1 },
            { name: 2  }, { name: 3 },
            { name: 4  }, { name: 5 },
          ],
        }, {
          title: 'new pack',
          data: [
            { name: 0  }, { name: 1 },
            { name: 2  }, { name: 3 },
            { name: 4  }, { name: 5 },
          ],
        },
        {title: 'new turn player', data: 'Player 2'},
        {title: 'new turn player', data: 'Player 2'},
        {title: 'new turn player', data: 'Player 2'},
        {
          title: "card taken", data: {
            "cardName" : 5,
            "playerName": 'Player 3',
          },
        },
        {
          title: "card taken", data: {
            "cardName" : 5,
            "playerName": 'Player 3',
          },
        },
        {
          title: "card taken", data: {
            "cardName" : 5,
            "playerName": 'Player 3',
          },
        },
        {title: "new turn player", data: 'Player 3'},
        {title: "new turn player", data: 'Player 3'},
        {title: "new turn player", data: 'Player 3'},
        {
          title: "card taken", data: {
            "cardName" : 4,
            "playerName": 'Player 1',
          },
        }, {
          title: "card taken", data: {
            "cardName" : 4,
            "playerName": 'Player 1',
          },
        }, {
          title: "card taken", data: {
            "cardName" : 4,
            "playerName": 'Player 1',
          },
        },
        {title: "new turn player", data: 'Player 1'},
        {title: "new turn player", data: 'Player 1'},
        {title: "new turn player", data: 'Player 1'},
        {title: "card taken", data: {
          "cardName" : 3,
          "playerName": 'Player 2',
        }},
            {title: "card taken", data: {
              "cardName" : 3,
              "playerName": 'Player 2',
            }},
            {title: "card taken", data: {
              "cardName" : 3,
              "playerName": 'Player 2',
            }},
            {title: "new turn player", data: 'Player 1'},
            {title: "new turn player", data: 'Player 1'},
            {title: "new turn player", data: 'Player 1'},

            {title: "card taken", data: {
              "cardName" : 2,
              "playerName": 'Player 2',
            }},
            {title: "card taken", data: {
              "cardName" : 2,
              "playerName": 'Player 2',
            }},
            {title: "card taken", data: {
              "cardName" : 2,
              "playerName": 'Player 2',
            }},
            {title: "new turn player", data: 'Player 3'},
            {title: "new turn player", data: 'Player 3'},
            {title: "new turn player", data: 'Player 3'},


            {title: "card taken", data: {
              "cardName" : 1,
              "playerName": 'Player 1',
            }},
            {title: "card taken", data: {
              "cardName" : 1,
              "playerName": 'Player 1',
            }},
            {title: "card taken", data: {
              "cardName" : 1,
              "playerName": 'Player 1',
            }},
            {title: "new turn player", data: 'Player 2'},
            {title: "new turn player", data: 'Player 2'},
            {title: "new turn player", data: 'Player 2'},

            {title: "card taken", data: {
              "cardName" : 0,
              "playerName": 'Player 3',
            }},
            {title: "card taken", data: {
              "cardName" : 0,
              "playerName": 'Player 3',
            }},
            {title: "card taken", data: {
              "cardName" : 0,
              "playerName": 'Player 3',
            }},

            {title: "draft over", data: undefined},
            {title: "draft over", data: undefined},
            {title: "draft over", data: undefined}];

      //assert that the data has been emitted down the socket
      assert.deepEqual(exp , sentSocketData);
    });

    it('should throw exception if picked card is not in the players pack', () => {
      const p1 = createMockSocket(1);

      draft.addPlayer(p1);
      draft.addPlayer(p1);
      draft.addPlayer(p1); //table needs to be full to start the draft

      expect(() => {draft.pickCard(p1, -13);}).to.throw('card was not in pack');
    });
  });
});
