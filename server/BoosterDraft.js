'use strict';
/*eslint-env node, es6*/
const winston = require('winston');

module.exports = exports = function(config) {
  let passingDirection = 'left';
  let numPlayersWaiting = 0;
  let finished = false;
  let started = false;

  const {table, packs} = config;

  const hasStarted = () => started;
  const isFinished = () => finished;
  const changeDirection = (direction) => (direction === 'left')? 'right': 'left';

  const sendPacks = (passDirection, inTable) => {
    winston.info('sending packs');

    inTable.forEach((inPlayer, next) => {
      next.nextPack = inPlayer.pack
    }, passDirection);

    inTable.forEach((inPlayer) => {
      inPlayer.pack = inPlayer.nextPack;
      inPlayer.socket.emit('new pack', inPlayer.pack);
    });
  };

  const startNewTurn = (passDirection, inTable) => {
    numPlayersWaiting = 0;
    sendPacks(passDirection, inTable);
  };

  const finishGame = (inTable) => {
    winston.info('drafting is over');
    inTable.broadcastMessage('draft over');
    finished = true;
  };

  const startNewRound = (passDirection, inPacks, inTable) => {
    if (inPacks.length < inTable.getMaxPlayers()){
      return finishGame(inTable);
    }

    inTable.forEach((player) => {
      player.pack = inPacks.pop()
    });

    startNewTurn(passDirection, inTable);
  };

  const addPlayer = (socket, name) => {
    if (!socket) {
      throw 'cannot add player: no socket included';
    }

    table.addPlayer({
      name,
      socket,
      pack : [],
      nextPack: [],
    });

    table.broadcastMessage('all players', table.getPlayerNames());

    if (table.isFull()) {
      table.randomise();
      table.broadcastMessage('all players', table.getPlayerNames());
      table.broadcastMessage('game started');
      startNewRound(passingDirection, packs, table);
      started = true;
    }
  };

  const pickCard = (socket, cardName) => {
    const player = table.getPlayer(socket.id);
    const cardIndex = player.pack.findIndex((e) => e.name === cardName);

    winston.info(`Player: ${player.name} Picked Card: ${cardName}`);

    if (-1 === cardIndex) {
      throw 'card was not in pack';
    }

    //remove the card from the pack
    player.pack.splice(cardIndex,1);


    //TODO improve this part
    //I hate how this is here
    numPlayersWaiting += 1;
    if (numPlayersWaiting === table.getMaxPlayers()) {
      if (!player.pack.length) {
        passingDirection = changeDirection(passingDirection);
        startNewRound(passingDirection, packs, table);
      } else {
        startNewTurn(passingDirection, table);
      }
    }
  };

  return {
    addPlayer,
    pickCard,
    isFinished,
    hasStarted,
  };
};
