'use strict';
/*eslint-env node, es6*/

//doubly linked list map
//nodes are accessible via a key value directly
//or via the left/right

module.exports = exports = (inArr) => {
  const list = {};
  let head;
  let tail;

  //if there is an array specified, load the list with that
  //array
  if (inArr) {
    inArr.forEach((item) => {
      add(item.key, item.value);
    });
  }

  //TODO dont really like that this is a function,
  //but it will do for now
  function length () {
    return Object.keys(list).length
  }

  //TODO default the value to the key?
  function add (key, value) {
    if (list[key]) {
      throw `elem with key ${key} already exists`;
    }

    const obj = {
      value,
    };

    if (!head) {
      head = obj;
      tail = obj;
      tail.left = obj;
    }


    head.left = obj;

    obj.left = tail;
    obj.right = head;

    tail.right = obj;
    tail = obj;

    list[key] = obj;

  }

  function remove (key) {
    if (!list[key]) {
      throw 'cannot remove key not present';
    }

    const remObj = list[key];

    list[key].left.right = list[key].right;
    list[key].right.left = list[key].left;

    if (list[key] === head) {
      head = list[key].right;
    }

    if (list[key] === tail) {
      tail = list[key].left;
    }

    delete list[key];

    return remObj.value;
  }

  function get (key) {
    //if the player not exists in the table
    if (!list[key]) {
      throw `key ${key} is not in list`;
    }

    return list[key].value;
  }

  function next (key, direction = 'right') {
    const node = list[key];

    if (!node) {
      throw `no object specified by key ${key}`;
    }

    return node[direction].value;
  }

  //func takes the current player and the next player as the parameters
  //doing it like the makes it possible to iterate either way without hassle
  //also just learning about tail recursion
  function forEach (func, direction = 'right') {
    (function iter(node) {
      const nextNode = node[direction];

      func(node.value, nextNode.value);

      if (nextNode === head) {
        return;
      }

      return iter(nextNode);
    }(head));
  }

  //takes listMap and converts it to a mapped array
  //TODO decide if it would be better to just map
  //each element but keep it as a MapList
  function map (func) {
    const arr = [];

    forEach((val) => {
      arr.push(func(val));
    });

    return arr;
  }

  function getHead () {
    return head.value;
  }

  return {
    getHead,
    get,
    next,
    remove,
    add,
    forEach,
    map,
    length,
  };
};