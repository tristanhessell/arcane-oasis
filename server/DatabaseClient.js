'use strict';
/*eslint-env node, es6*/

const HOSTNAME = process.env.AO_DB_HOST;

const request = require('request-promise-native');

//get the card objects for a list
const getCards = (gameType, draftFormat, list, owner) => {
  const options = {
    uri: `http://${HOSTNAME}/games/${gameType}/drafts/${draftFormat}/lists/${encodeURI(list)}`,
    json: true,
    qs: {owner},
  };

  return request.get(options)
    .then((cards) => cards)
    .catch(() => {
      throw `cannot find list ${list}`;
    });
};

//get a list of lists for a given draftFormat
const getLists = (gameType, draftFormat) => {
  const options = {
    uri: `http://${HOSTNAME}/games/${gameType}/drafts/${draftFormat}/lists`,
    json: true,
  };

  return request.get(options)
    .then((lists) => lists)
    .catch((e) => {
      throw `Cannot get lists for ${draftFormat}: ${e}`;
    });
}

//save a list to the
const addList = (gameType, draftFormat, listObj) => {
  const options = {
    uri: `http://${HOSTNAME}/games/${gameType}/drafts/${draftFormat}/lists`,
    json: listObj,
  };

  return request.post(options)
    .then((data) => data)
    .catch((e) => {
      throw `${e}`;
    });
}

module.exports = {
  getCards,
  getLists,
  addList,
};
