'use strict';
/*eslint-env node, es6*/

const request = require('request-promise-native');

const HOSTNAME = process.env.AO_ACCT_DB_HOST;

//TODO change these to be about users
const checkUser = (username) => {
  const options = {
    json: true,
    uri: `http://${HOSTNAME}/users/${username}`,
  };

  return request.get(options)
    .then((res) => {
      console.log(res)
      console.log('user found')
    })
    .catch((e) => {
      console.log(e.message)
      throw `could not check user: ${e.message}`;
    });
}

const addAccount = (params) => {
  const {username, password} = params;
  const options = {
    uri: `http://${HOSTNAME}/users/`,
    json: {
      username,
      password,
    },
  };

  return request.post(options)
    .then((data) => data)
    .catch((e) => {
      throw `${e}`;
    });
}

const updateAccount = (params) => {
  const {username, password} = params;
  const options = {
    uri: `http://${HOSTNAME}}/users/${username}`,
    json: {
      username,
      password,
    },
  };

  return request.put(options)
    .then((data) => data)
    .catch((e) => {
      throw `${e.message}`;
    });
}

const validateAccount = (params) => {
  const {username, password} = params;
  const options = {
    uri: `http://${HOSTNAME}/users/${username}/authenticate`,
    json: {
      username,
      password,
    },
  };

  return request.post(options)
    .then((data) => data)
    .catch((e) => {
      throw `${e.message}`;
    });
}

//unused at the moment
const deleteAccount = (/*params*/) => {
  throw 'deleteAccount() unimplemented';
};

module.exports = {
  addAccount,
  validateAccount,
  updateAccount,
  checkUser,
  //unused
  deleteAccount,
};
