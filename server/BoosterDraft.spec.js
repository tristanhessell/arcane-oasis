'use strict';
/*eslint-env node, es6, mocha*/

const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;

//TODO fix these tests - structure
//TODO fix these tests - E2E tests cover a lot of this
describe('BoosterDraft', () => {
  const createDraft = require('./BoosterDraft.js');
  let draft;
  let sentSocketData = [];

  const packCreator = {
    getPacks : () => {
      return [[{ name: 0  }, { name: 1 }],
                    [{ name: 2  }, { name: 3 }],
                    [{ name: 4  }, { name: 5 }],
                    [{ name: 6  }, { name: 7 }],
                    [{ name: 8  }, { name: 9 }],
                    [{ name: 10 }, { name: 11}]];
    },
  };

  const createMockTable = () => {
    const obj = {
      players: [],
      addPlayer : (player) => {
        if (obj.players.length !== 0) {
          const tailIndex = obj.players.length-1;

          obj.players[0].left = player; //head.left = new node
          obj.players[tailIndex].right = player; //tail.right = new node
          player.left = obj.players[tailIndex]; //new node.left = tail
          player.right = obj.players[0]; //new node.right = head;
        }
        obj.players.push(player);
      },
      broadcastMessage: (title, data) => {
        obj.players.forEach((player) => {
          player.socket.emit(title, data);
        });
      },
      getPlayer: (socketID) => obj.players.filter((player) => player.socket.id === socketID)[0],
      sendPacks: () => {
        obj.players.forEach(function(player) {
          player.socket.emit('new pack', player.pack);
        });
      },
      getMaxPlayers: () => 3,
      isFull: () => obj.players.length === 3,
      resetTable : () => { sentSocketData.push('reset called from table'); },
      forEach : (func, direction) => {
        obj.players.forEach((player) => {func(player, player[direction]);});
      },
      randomise: () => {},
      getFirstPlayer: () => obj.players[0],
    };

    return obj;
  };

  const createMockSocket = (scktID) => {
    const obj = {
      emit : (msg, data) => {
        sentSocketData.push({
          title: msg,
          'data': data && data.slice(), //hack to copy array HACK HACK HACK
        });
      },
    };
    obj.id = scktID;

    return obj;
  };

  beforeEach('Before each booster test', () => {
    const config = {
      table: createMockTable(),
      packs: packCreator.getPacks(),
    };
    draft = createDraft(config);
    sentSocketData.length = 0;
  });

  describe('#addPlayer()', () => {
    it('should send send a pack to the added player only when the table is full', () => {
      draft.addPlayer(createMockSocket(13));
      draft.addPlayer(createMockSocket(14));
      draft.addPlayer(createMockSocket(15));
            //assert that the data has been emitted down the socket
      let expected = [
                //not sure why, but data: undefined is needed to pass test
                {title: 'game started', data: undefined}, 
                {title: 'game started', data: undefined},
                {title: 'game started', data: undefined},
                {title: 'new pack', data: [{ name: 8  }, { name: 9 }]},
                {title: 'new pack', data: [{ name: 6  }, { name: 7 }]},
                {title: 'new pack', data: [{ name: 10 }, { name: 11}]},
      ];
      assert.deepEqual(sentSocketData, expected);
    });

    it('should throw exception if the socket isnt passed in', () => {
      expect(() => {draft.addPlayer();}).to.throw('cannot add player: no socket included');
    });
  });

  describe('#pickCard() & #isFinished()', () => {
    it('should play through properly with 3 players', () => {
      const p1 = createMockSocket(1);
      const p2 = createMockSocket(2);
      const p3 = createMockSocket(3);

            //add the players.
      draft.addPlayer(p1);
      draft.addPlayer(p2);
      draft.addPlayer(p3);

            //card picks
      draft.pickCard(p1, 8);
      draft.pickCard(p2, 6);
      draft.pickCard(p3,10);

            //passing left
            //card picks
      draft.pickCard(p1, 7);
      draft.pickCard(p2, 11);
      draft.pickCard(p3, 9);

            //new pack
            //1st 2nd 3rd picks
      draft.pickCard(p1, 0);
      draft.pickCard(p2, 4);
      draft.pickCard(p3, 2);
            //Passed right
            //card picks
      draft.pickCard(p1, 3);
      draft.pickCard(p2, 1);

      assert.equal(false, draft.isFinished());

      draft.pickCard(p3, 5);

      assert.equal(true, draft.isFinished());

            //game end
      const exp = [
            //not sure why, but data: undefined is needed to pass test
            {title: 'game started', data: undefined},
            {title: 'game started', data: undefined},
            {title: 'game started', data: undefined},
            {title: "new pack", data: [{name: 8}, {name: 9}]},
            {title: "new pack", data: [{name: 6}, {name: 7}]},
            {title: "new pack", data: [{ name: 10  }, { name: 11 }]},
            {title: "new pack", data: [{ name: 7  }]},
            {title: "new pack", data: [{ name: 11 }]},
            {title: "new pack", data: [{name: 9  }]},
            {title: "new pack", data: [{ name: 0  }, { name: 1 }]},
            {title: "new pack", data: [{ name: 4  }, { name: 5 }]},
            {title: "new pack", data: [{ name: 2  }, { name: 3 }]},
            {title: "new pack", data: [{name: 3 }]},
            {title: "new pack", data: [{name: 1  }]},
            {title: "new pack", data: [{name: 5 }]},
            {title: "draft over", data: undefined},
            {title: "draft over", data: undefined},
            {title: "draft over", data: undefined}];

            //assert that the data has been emitted down the socket
      assert.deepEqual(exp , sentSocketData);
    });

    it('should throw exception if picked card is not in the players pack', () => {
      const p1 = createMockSocket(1);
      draft.addPlayer(p1);
      expect(() => {draft.pickCard(p1, -13);}).to.throw('card was not in pack');
    });
  });
});
