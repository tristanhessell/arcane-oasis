'use strict';
/*eslint-env node, es6*/

module.exports = exports = (a, b) => {
  // //if they are the same type, order on name
  // if(a.cardType === b.cardType) {
  //   //      a comes B4 b           a comes after b       they are equal
  //   return (a.name < b.name) ? -1 : (a.name > b.name) ? 1 : 0;
  // }

  // //can't be equal as the if statement checks this
  // return (a.cardType < b.cardType) ? -1 : 1;
  return a && b;
};
