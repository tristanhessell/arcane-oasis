'use strict';
/*eslint-env node, es6*/

module.exports = exports = (cardCreator, sorter) => {
  const shuffleCards = (cards) => {
    for (let i = 0; i < cards.length; i += 1) {
      const j = Math.floor(Math.random() * (i + 1));

      [cards[i], cards[j]] = [cards[j], cards[i]];
    }

    return cards;
  }

  const splitIntoPacks = (numPacks, cards, packSize) => {
    if (cards.length < numPacks*packSize) {
      throw 'not enough cards';
    }

    cards.length = numPacks*packSize;

    const packs = [];
    const START_INDEX = 0;

        //while there are still cards to go
    while (cards.length >= packSize) {
            //remove N cards and create a pack from them
      packs.push(cards.splice(START_INDEX, packSize).sort(sorter));
    }

    return packs;
  }

  return (opts) => {
    const {numPacks, sets, packSize, owner} = opts;

    return cardCreator(sets, 'cube', owner)
      .then((cards) => splitIntoPacks(numPacks, shuffleCards(cards), packSize));
  }
};
