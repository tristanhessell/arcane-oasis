'use strict';
/*eslint-env node, es6, mocha*/

const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;

describe('ListMap', () => {
  const createListMap = require('./ListMap.js');
  let listMap;

  describe('#add()', () => {
    it('should add the objects to the end of the list', () => {
      const p1 = 1;
      const p2 = 2;
      const p3 = 3;
      const p4 = 4;
      const p5 = 5;
      const p6 = 6;
      const p7 = 7;

      listMap = createListMap();

      listMap.add(1, p1);
      listMap.add(2, p2);
      listMap.add(3, p3);
      listMap.add(4, p4);
      listMap.add(5, p5);
      listMap.add(6, p6);
      listMap.add(7, p7);
      assert.deepEqual(listMap.map((o) => o), [1,2,3,4,5,6,7]);
    });


    it('should throw exception if the key is already used', () => {
      listMap = createListMap();
      listMap.add(13, 'thirteen');
      expect(() => listMap.add(13)).to.throw('elem with key 13 already exists');
    });
  });


  describe('#get()', () => {
    it('should return the object if it is in the list', () => {
      listMap = createListMap();

      listMap.add(13, 'thirteen');
      assert.equal('thirteen', listMap.get(13));
    });

    it('should throw exception if the key is not in the list', () => {
      listMap = createListMap();
      listMap.add(13, 'thirteen');
      expect(() => listMap.get(-13)).to.throw(`key -13 is not in list`);
    });

    it('should throw exception if the key is already used', () => {
      listMap = createListMap();
      listMap.add(13, 'thirteen');
      expect(() => listMap.add(13)).to.throw('elem with key 13 already exists');
    });
  });

  describe('#next()', () => {
    it('should get the left object to the specified objecy', () => {
      const p1 = 'one';
      const p2 = 'two';
      const p3 = 'three';

      listMap = createListMap();
      listMap.add(1, p1);
      listMap.add(2, p2);
      listMap.add(3, p3);

      assert.equal(p3, listMap.next(1, 'left'));
      assert.equal(p1, listMap.next(2, 'left'));
      assert.equal(p2, listMap.next(3, 'left'));
    });

    it('should get the right player to the specified player', () => {
      const p1 = 'one';
      const p2 = 'two';
      const p3 = 'three';

      listMap = createListMap();
      listMap.add(1, p1);
      listMap.add(2, p2);
      listMap.add(3, p3);

      assert.equal(p2, listMap.next(1, 'right'));
      assert.equal(p3, listMap.next(2, 'right'));
      assert.equal(p1, listMap.next(3, 'right'));
    });

        //this is just the same as the right direction test
    it('should default to the right direction', () => {
      const p1 = 'one';
      const p2 = 'two';
      const p3 = 'three';

      listMap = createListMap();
      listMap.add(1, p1);
      listMap.add(2, p2);
      listMap.add(3, p3);

      assert.equal(p2, listMap.next(1));
      assert.equal(p3, listMap.next(2));
      assert.equal(p1, listMap.next(3));
    });

    it('should throw exception if the player is not at the table', () => {
      const p1 = 1;

      listMap = createListMap();
      listMap.add(1, p1);

      expect(() => listMap.next(13)).to.throw(`no object specified by key ${13}`);
    });
  });

  describe('#remove()', () => {
    it('should return the value on the table', () => {
      const p1 = 'one';
      const p2 = 'two';
      const p3 = 'three';

      listMap = createListMap();

      listMap.add(1, p1);
      listMap.add(2, p2);
      listMap.add(3, p3);

      assert.equal(p1, listMap.remove(1));
      assert.equal(listMap.length(), 2)
      assert.equal(p2, listMap.remove(2));
      assert.equal(listMap.length(), 1)
      assert.equal(p3, listMap.remove(3));
      assert.equal(listMap.length(), 0)
    });

    it('should throw if the key is not in the list', () => {
      listMap = createListMap();
      listMap.add(1, 'memes');

      expect(() => listMap.remove(13)).to.throw('cannot remove key not present');
    });

    it('should remove an object correctly (removing the middle case)', () => {
      listMap = createListMap();
      listMap.add(1, 'one');
      listMap.add(2, 'two');
      listMap.add(3, 'three');
      listMap.add(4, 'four');

      assert.equal('two', listMap.remove(2));
      assert.deepEqual(listMap.map((o) => o), ['one', 'three', 'four']);
    });
  });


  describe('#forEach()', () => {
    let actualData;

    const testFunction = (player) => {
      actualData.push(player);
    };

    beforeEach(() => {
      actualData = [];
    });

    it('should forEach the table left', () => {
      const expected = [1,3,2];

      listMap = createListMap();
      listMap.add(1, 1);
      listMap.add(2, 2);
      listMap.add(3, 3);

      listMap.forEach(testFunction, 'left');
      assert.deepEqual(actualData, expected);
    });

    it('should forEach the table right', () => {
      const expected = [1,2,3];

      listMap = createListMap();
      listMap.add(1, 1);
      listMap.add(2, 2);
      listMap.add(3, 3);

      listMap.forEach(testFunction, 'right');
      assert.deepEqual(actualData, expected);
    });

    it('should forEach the table right by default', () => {
      const expected = [1,2,3];

      listMap = createListMap();
      listMap.add(1, 1);
      listMap.add(2, 2);
      listMap.add(3, 3);

      listMap.forEach(testFunction);
      assert.deepEqual(actualData, expected);
    });
  });

  describe('#map()', () => {
    it('should return an array in order of the list', () => {
      listMap = createListMap();
      listMap.add(1, 1);
      listMap.add(2, 2);
      listMap.add(3, 3);
      listMap.add(4, 4);

      assert.deepEqual(listMap.map((o) => o), [1, 2, 3, 4]);
    });
  });
});
