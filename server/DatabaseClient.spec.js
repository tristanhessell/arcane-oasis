'use strict';
/*eslint-env node, es6, mocha*/

//Tests that the packs are of the correct size and the correct number of cards
//TODO test that the contents of the packs is correct (will require fiddling with Math.random)

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const sinon = require('sinon');
const http = require('http');
const PassThrough = require('stream').PassThrough;

const dbClient = require('./DatabaseClient.js');

describe('DatabaseClient', () => {
  const twentyCards = '[' +
    '{"name": "Asura Priest", "cardType":"Effect", "type":""},' +
    '{"name": "Battle Fader", "cardType":"Effect", "type":""},' +
    '{"name": "Beast King Barbaros", "cardType":"Effect", "type":""},' +
    '{"name": "Blackwing - Zeph the Elite", "cardType":"Effect", "type":""},' +
    '{"name": "Blue Thunder T-45", "cardType":"Effect", "type":""},' +
    '{"name": "Breaker the Magical Warrior", "cardType":"Effect", "type":""},' +
    '{"name": "Caius the Shadow Monarch", "cardType":"Effect", "type":""},' +
    '{"name": "Card Guard", "cardType":"Effect", "type":""},' +
    '{"name": "Card Trooper", "cardType":"Effect", "type":""},' +
    '{"name": "Cardcar D", "cardType":"Effect", "type":""},' +
    '{"name": "Chaos Sorcerer", "cardType":"Effect", "type":""},' +
    '{"name": "Cyber Dragon", "cardType":"Effect", "type":""},' +
    '{"name": "Cyber Jar", "cardType":"Effect", "type":""},' +
    '{"name": "D.D. Assailant", "cardType":"Effect", "type":""},' +
    '{"name": "D.D. Warrior Lady", "cardType":"Effect", "type":""},' +
    '{"name": "Don Zaloog", "cardType":"Effect", "type":""},' +
    '{"name": "Doomcaliber Knight", "cardType":"Effect", "type":""},' +
    '{"name": "Ehren, Lightsworn Monk", "cardType":"Effect", "type":""},' +
    '{"name": "Evilswarm Thunderbird", "cardType":"Effect", "type":""},' +
    '{"name": "Gorz the Emissary Darkness", "cardType":"Effect", "type":""}]';

  describe('#getCards()', () => {
    afterEach(() => {
      http.request.restore();
    });

    it('should create an array containing card objects', () => {
      //mock readfile and call the 3rd argument with the parameters undefined and the twenty cards array
      const response = new PassThrough();

      response.write(JSON.stringify(twentyCards));
      response.statusCode = 200;
      response.end();

      sinon.stub(http,'request')
        .callsArgWith(1, response)
        .returns(new PassThrough());

      return expect(dbClient.getCards('ygo', '', '13'))
        .eventually.deep.equal(twentyCards);
    });

    it('should return an error message if something goes wrong', (done) => {
      const request = new PassThrough();
      const expected = 'expected';

      sinon.stub(http,'request').returns(request);

      expect(dbClient.getCards('ygo', '', 'set'))
        .rejectedWith(`cannot find list set`)
        .notify(done);

      request.emit('error', expected);
    });
  });

  describe('getLists()', () => {
    const testList = [
      'list1',
      'list2',
    ];

    const GAME_TYPE = 'ygo';
    const DRAFT_FORMAT = 'df';

    afterEach(() => {
      http.request.restore();
    });

    it('should return an array of filenames', () => {
      const response = new PassThrough();

      response.write(JSON.stringify(testList));
      response.statusCode = 200;
      response.end();

      sinon.stub(http,'request')
        .callsArgWith(1, response)
        .returns(new PassThrough());

      return expect(dbClient.getLists(GAME_TYPE, DRAFT_FORMAT))
        .eventually.deep.equal(testList);
    });

    it('should reject if something goes wrong', (done) => {
      const request = new PassThrough();
      const expected = 'expected';

      sinon.stub(http,'request')
        .returns(request);

      expect(dbClient.getLists(GAME_TYPE, DRAFT_FORMAT))
        .eventually
        .rejectedWith(`Cannot get lists for ${DRAFT_FORMAT}: ${expected}`)
        .notify(done);

      request.emit('error', expected);
    });
  });
});
