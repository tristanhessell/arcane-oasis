'use strict';
/*eslint-env node, es6*/

const sorter         = require('./YGOSort.js');
const boosterCreator = require('./YGOBoosterCreator.js');
const dbClient           = require('../DatabaseClient.js');
const cardCreator        = require('../CardCreator.js')('ygo', dbClient);
const listBoosterCreator = require('../ListBoosterCreator.js');

//draftType, draftFormat, sets, owner
function createBoosters (draftType) {
  return {
    booster: boosterCreator(cardCreator/*, sorter*/),
    cube: listBoosterCreator(cardCreator, sorter),
  }[draftType];
}

module.exports = {
  validateParams: require('./YGOValidator.js'),
  createBoosters,
};
