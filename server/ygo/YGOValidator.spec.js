'use strict';
/*eslint-env node, es6, mocha*/

const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;

const validate = require('./YGOValidator.js');

describe('YGOValidator', () => {
  it('should throw if pack size in a booster draft != 9', () => {
    const inParams = {
      draftType : 'booster',
      packSize: 10,
    };

    expect(() => validate(inParams))
      .to
      .throw('booster size must be 9 cards in booster drafts');
  });

  it('should return true if pack size in a booster draft == 9', () => {
    const inParams = {
      draftType : 'booster',
      packSize: 9,
    };

    assert.equal(validate(inParams), true);
  });

  it('should return true if pack size is !< 2*N players (roch draft)', () => {
    const inParams = {
      draftFormat : 'rochester',
      packSize: 10,
      numPlayers: 5,
    };

    assert.equal(validate(inParams), true);
  });

  it('should throw if pack size in is < 2*N players (roch draft)', () => {
    const inParams = {
      draftFormat : 'rochester',
      packSize: 4,
      numPlayers: 5,
    };

    expect(() => validate(inParams))
      .to
      .throw('invalid number of players for rochester draft');
  });

  it('should return true if the draft format/type cannot be be found', () => {
    const inParams = {
      draftFormat: 'memes',
    };

    assert.equal(validate(inParams), true);
  });
});
