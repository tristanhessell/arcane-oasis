'use strict';
/*eslint-env node, es6*/

const order = [
  'Legend of Blue Eyes' ,
  'Metal Raiders' ,
  'Spell Ruller' ,
  'Pharohs Servant' ,
  'Labrynth of Nightmare' ,
  'Legacy of Darkness' ,
  'Pharonic Guardian' ,
  'Magicians Force' ,
  'Dark Crisis' ,
  'Invasion of Chaos' ,
  'Ancient Sanctuary' ,
  'Soul of the Duelist' ,
  'Rise of Destiny' ,
  'Flames of Eternity' ,
  'The Lost Millenium' ,
  'Cybernetic Revolution' ,
  'Elemental Energy' ,
  'Shadow of Infinity' ,
  'Enemy of Justice' ,
  'Power of the Duelist',
  'Cyberdark Impact',
  'Strike of Neos',
  'Force of the Breaker',
  'Tactical Evolution',
  'Gladiators Assault',
  'Phantom Darkness',
  'Light of Destruction',
  'The Duelist Genesis',
  'Crossroads of Chaos',
  'Crimson Crisis',
  'Raging Battle',
  'Ancient Prophecy',
  'Stardust Overdrive',
  'Absolute Powerforce',
  'The Shining Darkness',
  'Duelist Revolution',
  'Startstrike Blast',
  'Storm of Ragnarok',
  'Extreme Victory',
  'Generation Force',
  'Photon Shockwave',
  'Order of Chaos',
  'Galactic Overlord',
  'Return of the Duelist',
  'Abyss Rising',
  'Cosmo Blazer',
  'Lord of the Tachyon Galaxy',
  'Judgement of the Light',
  'Shadow Specters',
  'Legacy of the Valiant',
  'Primal Origin',
  'Duelist Alliance',
  'The New Challengers',
  'Secrets of Eternity',
  'Crossed Souls',
]


const PACK_NO_SP_PRE_TDGS = {
  cardGroups: ['C', 'R', 'SR', 'UR', 'SCR'],
  groups   : { Cmn: ['C'], NtCmn: ['R', 'SR', 'UR', 'SCR'] },
  packDesc : ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'NtCmn'],
};

// const PACK_NO_SP_POST_TDGS = {
//   cardGroups: ['C', 'R', 'SR', 'UR', 'SCR',],
//   groups   : { Cmn: ['C',],  R: ['R',], NtR: ['C', 'SR', 'UR', 'SCR',], },
//   packDesc : ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'R', 'NtR',],
// };

const PACK_HOLO_RARE = {
  cardGroups: ['C', 'R', 'SR', 'UR', 'SCR'],
  groups   : { Cmn: ['C'],  R: ['R'], Holo: ['SR', 'UR', 'SCR'] },
  packDesc : ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'R', 'Holo'],
};

const PACK_SP_PRE_TDGS = {
  cardGroups: ['C', 'SP', 'R', 'SR', 'UR', 'SCR'],
  groups   : { Cmn: ['C', 'SP'], NtCmn: ['R', 'SR', 'UR', 'SCR'] },
  packDesc : ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'NtCmn'],
};

const PACK_SP_POST_TDGS = {
  cardGroups: ['C', 'SP', 'R', 'SR', 'UR', 'SCR'],
  groups   : { Cmn: ['C', 'SP'],  R: ['R'], NtR: ['C', 'SP', 'SR', 'UR', 'SCR'] },
  packDesc : ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'R', 'NtR'],
};

const PACK_HA = {
  cardGroups : ['SCR', 'SR'],
  groups: {Scr: ['SCR'], Sr: ['SR']},
  packDesc: ['Scr', 'Sr', 'Sr', 'Sr', 'Sr'],
};

const PACK_DP_6 = {
  cardGroups : ['C', 'R', 'SR', 'UR'],
  groups: {Cmn: ['C'], NtCmn: ['R', 'SR', 'UR']},
  packDesc: ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'NtCmn'],
};

const PACK_DP_5 = {
  cardGroups : ['C', 'R', 'SR', 'UR'],
  groups: {Cmn: ['C'], NtCmn: ['R', 'SR', 'UR']},
  packDesc: ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'NtCmn'],
};

const PACK_GOLD = {
  cardGroups : ['C', 'GR'],
  groups: { C: ['C'], GR: ['GR']},
  packDesc: ['C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C',
             'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C',
             'C', 'C', 'C', 'GR', 'GR', 'GR'],
};

const PACK_LC = {
  cardGroups: ['C', 'R', 'SR', 'UR', 'SCR'],
  groups    : { C : ['C'], SR : ['SR'], UR : ['UR'], 'SCR': ['SCR'] },
  packDesc  : ['C', 'C', 'C', 'C', 'C', 'R', 'SR', 'UR', 'SCR'],
};

const PACK_MP = {
  cardGroups: ['C', 'R', 'SR', 'UR', 'SCR'],
  groups    : { C : ['C'], SR : ['SR'], UR : ['UR'], 'SCR': ['SCR'] },
  packDesc  : ['C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C',
               'C', 'C', 'R', 'SR', 'UR', 'SCR'],
};

const PACK_PREMIUM = {
  cardGroups : ['SCR', 'SR'],
  groups: {SCR: ['SCR'], SR: ['SR']},
  packDesc: ['Scr', 'SCR', 'SR', 'SR', 'SR'],
};

const PACK_STAR = {
  cardGroups: ['C'],
  groups    : { C : ['C'] },
  packDesc  : ['C', 'C', 'C'],
};

module.exports = exports = {
  order,
  packSetting: {
    'Legend of Blue Eyes' : PACK_SP_PRE_TDGS,
    'Metal Raiders' : PACK_SP_PRE_TDGS,
    'Spell Ruller' : PACK_SP_PRE_TDGS,
    'Pharohs Servant' : PACK_SP_PRE_TDGS,
    'Labrynth of Nightmare' : PACK_SP_PRE_TDGS,
    'Legacy of Darkness' : PACK_SP_PRE_TDGS,
    'Pharonic Guardian' : PACK_SP_PRE_TDGS,
    'Magicians Force' : PACK_SP_PRE_TDGS,
    'Dark Crisis' : PACK_SP_PRE_TDGS,
    'Invasion of Chaos' : PACK_SP_PRE_TDGS,
    'Ancient Sanctuary' : PACK_SP_PRE_TDGS,
    'Soul of the Duelist' : PACK_NO_SP_PRE_TDGS,
    'Rise of Destiny' : PACK_NO_SP_PRE_TDGS,
    'Flames of Eternity' : PACK_NO_SP_PRE_TDGS,
    'The Lost Millenium' : PACK_NO_SP_PRE_TDGS,
    'Cybernetic Revolution' : PACK_NO_SP_PRE_TDGS,
    'Elemental Energy' : PACK_NO_SP_PRE_TDGS,
    'Shadow of Infinity' : PACK_NO_SP_PRE_TDGS,
    'Enemy of Justice' : PACK_NO_SP_PRE_TDGS,
    'Power of the Duelist': PACK_NO_SP_PRE_TDGS,
    'Cyberdark Impact': PACK_NO_SP_PRE_TDGS,
    'Strike of Neos': PACK_NO_SP_PRE_TDGS,
    'Force of the Breaker': PACK_NO_SP_PRE_TDGS,
    'Tactical Evolution': PACK_NO_SP_PRE_TDGS,
    'Gladiators Assault': PACK_NO_SP_PRE_TDGS,
    'Phantom Darkness': PACK_SP_PRE_TDGS,
    'Light of Destruction': PACK_SP_PRE_TDGS,
    'The Duelist Genesis': PACK_SP_POST_TDGS,
    'Crossroads of Chaos': PACK_SP_POST_TDGS,
    'Crimson Crisis': PACK_SP_POST_TDGS,
    'Raging Battle': PACK_SP_POST_TDGS,
    'Ancient Prophecy': PACK_SP_POST_TDGS,
    'Stardust Overdrive': PACK_SP_POST_TDGS,
    'Absolute Powerforce': PACK_SP_POST_TDGS,
    'The Shining Darkness': PACK_SP_POST_TDGS,
    'Duelist Revolution': PACK_SP_POST_TDGS,
    'Startstrike Blast': PACK_SP_POST_TDGS,
    'Storm of Ragnarok': PACK_SP_POST_TDGS,
    'Extreme Victory': PACK_SP_POST_TDGS,
    'Generation Force': PACK_SP_POST_TDGS,
    'Photon Shockwave': PACK_SP_POST_TDGS,
    'Order of Chaos': PACK_SP_POST_TDGS,
    'Galactic Overlord': PACK_SP_POST_TDGS,
    'Return of the Duelist': PACK_SP_POST_TDGS,
    'Abyss Rising': PACK_SP_POST_TDGS,
    'Cosmo Blazer': PACK_SP_POST_TDGS,
    'Lord of the Tachyon Galaxy': PACK_SP_POST_TDGS,
    'Judgement of the Light': PACK_SP_POST_TDGS,
    'Shadow Specters': PACK_SP_POST_TDGS,
    'Legacy of the Valiant': PACK_SP_POST_TDGS,
    'Primal Origin': PACK_SP_POST_TDGS,
    'Duelist Alliance': PACK_SP_POST_TDGS,
    'The New Challengers': PACK_SP_POST_TDGS,
    'Secrets of Eternity': PACK_SP_POST_TDGS,
    'Crossed Souls': PACK_SP_POST_TDGS,
    //insert other packs here

    //other non-lineal packs
    'Battle Pack: Epic Dawn': {
      cardGroups: ['S1', 'S2', 'S3', 'S4'],
      groups    : { S1 : ['S1'], S2 : ['S2'], S3 : ['S3'], S4 : ['S4'], all: ['S1', 'S2', 'S3', 'S4'] },
      packDesc  : ['S1', 'S2', 'S3', 'S4', 'all'],
    },
    'Battle Pack 2: War of the Giants': {
      cardGroups: ['C', 'R', 'MR'],
      groups    : { C : ['C'], R : ['R'], all : ['C', 'R', 'MR'] },
      packDesc  : ['C', 'C', 'C', 'R', 'all'],
    },
    'Battle Pack 2: Round 2': {
      cardGroups: ['C', 'SR', 'UR'],
      groups    : { C : ['C'], SR : ['SR'], UR: ['UR'] },
      packDesc  : ['C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'SR', 'SR', 'SR', 'SR', 'SR', 'SR', 'UR'],
    },
    'Battle Pack 3: Monster League': {
      cardGroups: ['C', 'R', 'SHR'],
      groups    : { C : ['C'], R : ['R'], all : ['C', 'R', 'SHR'] },
      packDesc  : ['C', 'C', 'C', 'R', 'all'],
    },

    'WGRT': {
      cardGroups: ['C', 'SR', 'UR'],
      groups    : { C : ['C'], SR : ['SR'], UR : ['UR'] },
      packDesc  : ['C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'SR', 'SR', 'SR', 'SR', 'SR', 'SR', 'UR'],
    },

    'numh': PACK_HA,

    //TODO get these packs
    'Hidden Arsenal 01': PACK_HA,
    'Hidden Arsenal 02': PACK_HA,
    'Hidden Arsenal 03': PACK_HA,
    'Hidden Arsenal 04': PACK_HA,
    'Hidden Arsenal 05': PACK_HA,
    'Hidden Arsenal 06': PACK_HA,
    'Hidden Arsenal 07': PACK_HA,
    'DRLG': PACK_HA,
    'DRL2': PACK_HA,
    'DRLU': PACK_HA,
    'THSF': PACK_HA,
    'HSRD': PACK_HOLO_RARE,
    'WIRA': PACK_HOLO_RARE,
    'Duelist Pack 01':  PACK_DP_6, //jaden yuki
    'Duelist Pack 02':  PACK_DP_6, //chazz
    'Duelist Pack 03': PACK_DP_6, //jaden yuki 2
    'Duelist Pack 04': PACK_DP_6, //zane truesdale
    'Duelist Pack 05': PACK_DP_6, //aster phoenix
    'Duelist Pack 06': PACK_DP_6, //jaden yuki 3
    'Duelist Pack 07': PACK_DP_6, //jesse anderson
    'Duelist Pack 08': PACK_DP_5, //yusei
    'Duelist Pack Yugi': PACK_DP_5, //yugi
    'Duelist Pack 09': PACK_DP_5, //yusei 2
    'Duelist Pack Kaiba': PACK_DP_5, //kaiba
    'Duelist Pack 10': PACK_DP_5, //yusei 3
    'Duelist Pack 11': PACK_DP_5, //crow
    'Duelist Pack Battle City': PACK_DP_5, //battle city
    'Gold Pack 01': PACK_GOLD,
    'Gold Pack 02': PACK_GOLD,
    'Gold Pack 03': PACK_GOLD,
    'Gold Pack 04': PACK_GOLD,
    'Gold Pack 05': PACK_GOLD,
    //there are regional variations with the Premium golds packs
    'PGLD-NA': {
      cardGroups: ['GR1', 'GR2', 'GSCR'],
      groups    : { GR1 : ['GR1'], GR2 : ['GR2'], 'GSCR': ['GSCR'] },
      packDesc  : [ 'GR1', 'GR2', 'GR2', 'GSCR', 'GSCR'],
    },
    //UK, AU, SP, PG
    'PGLD': {
      cardGroups: ['GR', 'GSCR'],
      groups    : { GR : ['GR'],'GSCR': ['GSCR'] },
      packDesc  : [ 'GR', 'GR', 'GR', 'GR', 'GR', 'GR', 'GR', 'GR', 'GR',
                    'GSCR', 'GSCR', 'GSCR', 'GSCR', 'GSCR', 'GSCR'],
    },
    //FR, GE, IT
    'PGLD-EU': {
      cardGroups: ['GR', 'GSCR'],
      groups    : { GR : ['GR'], 'GSCR': ['GSCR'] },
      packDesc  : [ 'GR', 'GR', 'GR', 'GR', 'GR', 'GR',
                    'SCR', 'SCR', 'SCR', 'SCR'],
    },

    'PGL2-NA': {
      cardGroups: ['GR1', 'GR2', 'GSCR'],
      groups    : { GR1 : ['GR1'], GR2 : ['GR2'], 'GSCR': ['GSCR'] },
      packDesc  : [ 'GR1', 'GR2', 'GR2', 'GSCR', 'GSCR'],
    },
    //UK, AU, SP, PG
    'PGL2': {
      cardGroups: ['GR1', 'GR2', 'GSCR'],
      groups    : { GR1 : ['GR1'], GR2 : ['GR2'], 'GSCR': ['GSCR'] },
      packDesc  : [ 'GR1', 'GR1', 'GR1', 'GR1', 'GR1', 'GR1',
                    'GR2', 'GR2', 'GR2',
                    'GSCR', 'GSCR', 'GSCR', 'GSCR', 'GSCR', 'GSCR'],
    },
    //FR, GE, IT
    'PGL2-EU': {
      cardGroups: ['GR1', 'GR2', 'GSCR'],
      groups    : { GR1 : ['GR1'], GR2 : ['GR2'], 'GSCR': ['GSCR'] },
      packDesc  : [ 'GR1', 'GR1', 'GR1', 'GR1',
                    'GR2', 'GR2',
                    'GSCR', 'GSCR', 'GSCR', 'GSCR'],
    },

    //'PGL3'
    'PGL3': {
      cardGroups: ['GR', 'GSCR'],
      groups    : { GR : ['GR'], 'GSCR': ['GSCR'] },
      packDesc  : [ 'GR', 'GR', 'GR', 'GSCR', 'GSCR'],
    },

    'RYMP': {
      cardGroups: ['C', 'R', 'SR', 'UR', 'SCR'],
      groups    : { C : ['C'], SR : ['SR'], UR : ['UR'], 'SCR': ['SCR'] },
      packDesc  : ['C', 'C', 'C', 'C', 'C', 'C', 'C', 'R', 'SR', 'UR', 'SCR'],
    },
    'LCGX': PACK_LC,
    'LCYW': PACK_LC,
    'LCJW': PACK_LC,
    'LC5D': PACK_LC,
    'Premium Pack 01': PACK_PREMIUM, //3 SR, 2 SCR
    'Premium Pack 02': PACK_PREMIUM,
    'SP13': PACK_STAR, // 3 C
    'MP14': PACK_MP,
    'MP15': PACK_MP,

        //'DB1':
        //'DB2':
        //'DR1':
        //'DR2':
        //'DR3':
        //'DR04':
        //1-2 rares in a pack
        // 6M, 3T, 3S

        //'RP01':
        //'RP02' - 9 (8C + 1)

        //'DLG1' //11 C + 1
        //'MIL1' // 4C + 1 (R, SR, UR)
  },
};