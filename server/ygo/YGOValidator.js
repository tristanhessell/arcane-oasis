'use strict';
/*eslint-env node, es6*/

//TODO currently doesnt factor in for a new draft type
module.exports = exports = (params) => {
  const {draftType, packSize, draftFormat, numPlayers} = params;

   //of the draft type is booster, the packSize MUST be 9
  if (draftType === 'booster') {
    const PACK_SIZE = 9;

    if (packSize !== PACK_SIZE) {
      throw 'booster size must be 9 cards in booster drafts';
    }
  }

  if (draftFormat === 'rochester') {
    const CARDS_PER_PLAYER_PER_ROUND = 2;

    //there must be enough cards in the pack for 2 cards per player
    if (numPlayers > Math.floor(packSize/CARDS_PER_PLAYER_PER_ROUND)){
      throw 'invalid number of players for rochester draft';
    }
  }

  return true;
};
