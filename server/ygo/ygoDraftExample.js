'use strict';
/*eslint-env node, es6*/

module.exports = exports = function() {
    //3 packs for each draftPack
  const organisePacks = (packs, numPlayers, packsPerPlayer) => {
    let outPacks = [];

    for (let ii = 0; ii < packs.length; ii += packsPerPlayer) {
      let outPack = [];
      for (let jj = 0; jj < packsPerPlayer; jj += 1) {
        outPack = outPack.concat(packs[ii+jj]);
      }
      outPacks.push(outPack);
    }

    return outPacks;
  };

   //3 packs , 1 round2Pack, 3 packs each
  const organisePacksBP02 = (packs, numPlayers, round2Packs) => {
    let outPacks = [];

        //combine the first 3 packs for each player
    for (let ii = 0; ii < numPlayers*3; ii += 3) {
      let outPack = [].concat(packs[ii], packs[ii+1], packs[ii+2]);
      outPacks.push(outPack);
    }

        //then add the round 2 packs
    outPacks = outPacks.concat(round2Packs);

        //then the final 3 packs
    for (let ii = 0; ii < numPlayers*3; ii += 3) {
      let outPack = [].concat(packs[ii], packs[ii+1], packs[ii+2]);
      outPacks.push(outPack);
    }

    return outPacks;
  };

  return {
    organisePacks,
    organisePacksBP02,
  };
};
