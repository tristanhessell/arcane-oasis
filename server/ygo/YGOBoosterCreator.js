'use strict';
/*eslint-env node, es6*/

const winston = require('winston');

module.exports = exports = (cardCreator) => {
  const boosterData = require('./YGOBoosterData.js');
  const {order} = boosterData;

  //if the pack doesnt have short prints, SP will be ignored
  const getRarities = (set) => {
    const rarities = {
      SP : 1/27, //1 in 3 packs = 1/27
      SR : 1/5,
      UR : 1/12,
      SCR : 1/23,
    };

    //TODO need to capabilities for sets outside the order

    //if the set isnt in the list, then
    //the packs dont have a rarity
    if (order.indexOf(set) === -1) {
      return {};
    }

    if (order.indexOf('Soul of the Duelist') <= order.indexOf(set) &&
        order.indexOf(set) <= order.indexOf('Tactical Evolution')) {
      rarities.UR = 1/24;
    }

    if (order.indexOf(set) < order.indexOf('The Shining Darkness')) {
      rarities.SCR = 1/31;
    }

    return rarities;
  };

  const getPackSettings = (set) =>{
    const packSettings = boosterData.packSetting[set];

    packSettings.rarities = getRarities(set);

    return packSettings;
  };

  //this is very inefficient, but it gets the job done for now
  const getCardPool =  (allCards, packSettings) => {
    const cardGroups = packSettings.cardGroups;
    //hacky but works
    const pool = {
      all : allCards,
    };

    //take the array of types and put them into a map (with the relevant cards)
    cardGroups.forEach((type) => {
      pool[type] = allCards.filter((card) => card.rarity === type);
      if (!pool[type]) {
        winston.info(`card group '${type}' is empty`);
      }
    });

    return pool;
  };

  //get a random card from a card group
  //EG: Get a random card from the common group
  const getCard = (cardGroup) =>
    cardGroup[Math.floor(Math.random() * cardGroup.length)];

  //boosterDescription contains an array that
  const getPack = (cardPool, packSettings) => {
    const {packDesc, groups, rarities} = packSettings;

    return packDesc.map((group) => {
      const type = getType(groups[group], rarities);
      return getCard(cardPool[type]);
    });
  };

  //card group is an array that contains the objects
  //that
  const getType = (groupTypes, rarities) => {
    const num = Math.random();

    const recurseType = (types, currentTotal) => {
      const currType = types.pop();

      //if the type isnt in the rarities object
      //currType is the default - Common
      if (!rarities[currType]) {
        return currType;
      }

      //add the rarities to the current total
      //to see if you got the card
      const currTotal = currentTotal + rarities[currType];

      //if you did get the card, return that type
      if (num <= currTotal) {
        return currType;
      }

      //otherwise try the next one
      return recurseType(types, currTotal);
    };

    return recurseType(groupTypes.slice(), 0);
  };

  //get the number of packs as specified
  //number, cardCreator, string
  //CURRENTLY ONLY WORKS WITH ONE SET
  return (opts) => {
    const {numPacks, sets, owner} = opts;
    return cardCreator(sets, 'booster', owner)
      .then((allCards) => {
        const packSettings = getPackSettings(sets[0]);
        const cardPool = getCardPool(allCards, packSettings);
        const packs = [];

        for (let ii = 0; ii < numPacks; ii += 1) {
          packs.push(getPack(cardPool, packSettings));
        }

        return packs;
      });
  }
};
