'use strict';
/*eslint-env node, es6, mocha*/

const chai = require('chai');
const assert = chai.assert;

describe('YGOSort', () => {
  const sort = require('./YGOSort.js');

  describe('#sortCards()', () => {
    it('should order cards by cardType', () => {
      const testCards = [
        {cardType: 'Effect',  name: 'z'},
        {cardType: 'Synchro', name: 'm'},
        {cardType: 'Normal',  name: 'a'},
      ];

      testCards.sort(sort);

      const expectedCards = [
        {cardType: 'Effect',  name: 'z'},
        {cardType: 'Normal',  name: 'a'},
        {cardType: 'Synchro', name: 'm'},
      ];

      assert.deepEqual(expectedCards, testCards);
    });

    it('should order cards by name if cardType is equal', () => {
      const testCards = [
        {cardType: 'Effect', name: 'b'},
        {cardType: 'Effect', name: 'z'},
        {cardType: 'Normal', name: 'm'},
        {cardType: 'Normal', name: 'm'},
        {cardType: 'Normal', name: 'a'},
      ];

      testCards.sort(sort);

      const expectedCards = [
        {cardType: 'Effect', name: 'b'},
        {cardType: 'Effect', name: 'z'},
        {cardType: 'Normal', name: 'a'},
        {cardType: 'Normal', name: 'm'},
        {cardType: 'Normal', name: 'm'},
      ];

      assert.deepEqual(expectedCards, testCards);
    });
  });
});
