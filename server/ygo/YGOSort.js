'use strict';
/*eslint-env node, es6*/

const BEFORE = -1;
const EQUAL = 0;
const AFTER = 1;

module.exports = exports = (a, b) => {
  //if they are the same type, order on name
  if (a.cardType === b.cardType) {
    //      a comes B4 b           a comes after b       they are equal
    return (a.name < b.name) ? BEFORE : (a.name > b.name) ? AFTER : EQUAL;
  }

  //can't be equal as the if statement checks this
  return (a.cardType < b.cardType) ? BEFORE : AFTER;
};
