'use strict';
/*eslint-env node, es6, mocha*/

const chai = require('chai');
const assert = chai.assert;

describe('YGOBoosterCreator', () => {
  const mockCardCreator = (inCards) => () => Promise.resolve(inCards);
  const boosterCreator = require('./YGOBoosterCreator.js');

    //a set with short prints
  const tshd = [
    {name: 'XX-Saber Boggart Knight'             , rarity: 'SR' },
    {name: 'Blackwing - Ghibli the Searing Wind' , rarity: 'C'  },
    {name: 'Blackwing - Gust the Backblast'      , rarity: 'R'  },
    {name: 'Blackwing - Breeze the Zephyr'       , rarity: 'UR' },
    {name: 'Changer Synchron'                    , rarity: 'C'  },
    {name: 'Card Breaker'                        , rarity: 'C'  },
    {name: 'Second Booster'                      , rarity: 'C'  },
    {name: 'Archfiend Interceptor'               , rarity: 'C'  },
    {name: 'Dread Dragon'                        , rarity: 'R'  },
    {name: 'Trust Guardian'                      , rarity: 'SR' },
    {name: 'Flare Resonator'                     , rarity: 'C'  },
    {name: 'Synchro Magnet'                      , rarity: 'C'  },
    {name: 'Infernity Mirage'                    , rarity: 'SR' },
    {name: 'Infernity Randomizer'                , rarity: 'C'  },
    {name: 'Infernity Beetle'                    , rarity: 'R'  },
    {name: 'Infernity Avenger'                   , rarity: 'R'  },
    {name: 'Revival Rose'                        , rarity: 'R'  },
    {name: 'Morphtronic Vacuumen'                , rarity: 'C'  },
    {name: 'Bird of Roses'                       , rarity: 'SR' },
    {name: 'Spore'                               , rarity: 'C'  },
    {name: 'Fairy Archer'                        , rarity: 'C'  },
    {name: 'Biofalcon'                           , rarity: 'C'  },
    {name: 'Cherry Inmato'                       , rarity: 'R'  },
    {name: 'Magidog'                             , rarity: 'R'  },
    {name: 'Lyna the Light Charmer'              , rarity: 'C'  },
    {name: 'Wattgiraffe'                         , rarity: 'SR' },
    {name: 'Wattfox'                             , rarity: 'C'  },
    {name: 'Wattwoodpecker'                      , rarity: 'C'  },
    {name: 'Koa\'ki Meiru Sandman'               , rarity: 'C'  },
    {name: 'Memory Crush King'                   , rarity: 'C'  },
    {name: 'Delta Tri'                           , rarity: 'R'  },
    {name: 'Trigon'                              , rarity: 'C'  },
    {name: 'Testudo erat Numen'                  , rarity: 'SP' },
    {name: 'Ronintoadin'                         , rarity: 'C'  },
    {name: 'Batteryman AAA'                      , rarity: 'C'  },
    {name: 'Batteryman Fuel Cell'                , rarity: 'R'  },
    {name: 'Key Mouse'                           , rarity: 'C'  },
    {name: 'Ally of Justice Core Destroyer'      , rarity: 'R'  },
    {name: 'Hunter of Black Feathers'            , rarity: 'SP' },
    {name: 'Herald of Perfection'                , rarity: 'UR' },
    {name: 'Black-Winged Dragon'                 , rarity: 'UR' },
    {name: 'Chaos King Archfiend'                , rarity: 'UR' },
    {name: 'Infernity Doom Dragon'               , rarity: 'UR' },
    {name: 'Splendid Rose'                       , rarity: 'UR' },
    {name: 'Chaos Goddess'                       , rarity: 'SCR'},
    {name: 'Black-Winged Strafe'                 , rarity: 'C'  },
    {name: 'Cards for Black Feathers'            , rarity: 'C'  },
    {name: 'ZERO-MAX'                            , rarity: 'SR' },
    {name: 'Infernity Launcher'                  , rarity: 'SR' },
    {name: 'Into the Void'                       , rarity: 'UR' },
    {name: 'Intercept Wave'                      , rarity: 'UR' },
    {name: 'Pyramid of Wonders'                  , rarity: 'R'  },
    {name: 'The Fountain in the Sky'             , rarity: 'R'  },
    {name: 'Dragon Laser'                        , rarity: 'C'  },
    {name: 'Wattcube'                            , rarity: 'C'  },
    {name: 'Electromagnetic Shield'              , rarity: 'R'  },
    {name: 'Worm Call'                           , rarity: 'C'  },
    {name: 'Magic Triangle of the Ice Barrier'   , rarity: 'C'  },
    {name: 'Koa\'ki Meiru Initialize!'           , rarity: 'C'  },
    {name: 'Dawn of the Herald'                  , rarity: 'C'  },
    {name: 'Forbidden Graveyard'                 , rarity: 'C'  },
    {name: 'Leeching the Light'                  , rarity: 'C'  },
    {name: 'Corridor of Agony'                   , rarity: 'SP' },
    {name: 'Power Frame'                         , rarity: 'SR' },
    {name: 'Blackwing - Backlash'                , rarity: 'R'  },
    {name: 'Blackwing - Bombardment'             , rarity: 'C'  },
    {name: 'Black Thunder'                       , rarity: 'C'  },
    {name: 'Guard Mines'                         , rarity: 'R'  },
    {name: 'Infernity Reflector'                 , rarity: 'C'  },
    {name: 'Infernity Break'                     , rarity: 'C'  },
    {name: 'Damage Gate'                         , rarity: 'SR' },
    {name: 'Infernity Inferno'                   , rarity: 'R'  },
    {name: 'Phantom Hand'                        , rarity: 'C'  },
    {name: 'Assault Spirits'                     , rarity: 'C'  },
    {name: 'Blossom Bombardment'                 , rarity: 'C'  },
    {name: 'Morphtronics, Scramble!'             , rarity: 'C'  },
    {name: 'Power Break'                         , rarity: 'C'  },
    {name: 'Koa\'ki Meiru Shield'                , rarity: 'R'  },
    {name: 'Crevice Into the Different Dimension', rarity: 'C'  },
    {name: 'Synchro Ejection'                    , rarity: 'SR' },
    {name: 'Chaos Trap Hole'                     , rarity: 'SP' },
    {name: 'XX-Saber Darksoul'                   , rarity: 'UR' },
    {name: 'Koa\'ki Meiru Prototype'             , rarity: 'R'  },
    {name: 'Snyffus'                             , rarity: 'SCR'},
    {name: 'Nimble Sunfish'                      , rarity: 'SR' },
    {name: 'Akz, the Pumer'                      , rarity: 'R'  },
    {name: 'Saber Vault'                         , rarity: 'SCR'},
    {name: 'Core Overclock'                      , rarity: 'SR' },
    {name: 'Wave-Motion Inferno'                 , rarity: 'SCR'},
    {name: 'Infernity Barrier   Limited'         , rarity: 'SCR'},
    {name: 'Genex Controller'                    , rarity: 'C'  },
    {name: 'Genex Undine'                        , rarity: 'C'  },
    {name: 'Genex Searcher'                      , rarity: 'R'  },
    {name: 'X-Saber Palomuro'                    , rarity: 'C'  },
    {name: 'X-Saber Pashuul'                     , rarity: 'C'  },
    {name: 'Hydro Genex'                         , rarity: 'SR' },
    {name: 'Ally of Justice Light Gazer'         , rarity: 'SR' },
    {name: 'Genex Neutron'                       , rarity: 'SCR'},
    {name: 'Infernity Destroyer'                 , rarity: 'SCR'},
    {name: 'Koa\'ki Meiru Bergzak'               , rarity: 'SCR'},
  ];

    //a set with no short prints
  const taev = [
    {name: 'Gemini Summoner'                          , rarity: 'SCR'},
    {name: 'Alien Shocktrooper'                       , rarity: 'C'  },
    {name: 'Volcanic Rat'                             , rarity: 'C'  },
    {name: 'Renge, Gatekeeper of Dark World'          , rarity: 'C'  },
    {name: 'Hunter Dragon'                            , rarity: 'R'  },
    {name: 'Venom Cobra'                              , rarity: 'C'  },
    {name: 'Rainbow Dragon'                           , rarity: 'SCR'},
    {name: 'Chrysalis Pantail'                        , rarity: 'C'  },
    {name: 'Chrysalis Chicky'                         , rarity: 'C'  },
    {name: 'Chrysalis Pinny'                          , rarity: 'C'  },
    {name: 'Chrysalis Larva'                          , rarity: 'C'  },
    {name: 'Chrysalis Mole'                           , rarity: 'C'  },
    {name: 'Necro Gardna'                             , rarity: 'SR' },
    {name: 'Vennominaga the Deity of Poisonous Snakes', rarity: 'SCR'},
    {name: 'Vennominon the King of Poisonous Snakes'  , rarity: 'UR' },
    {name: 'Venom Snake'                              , rarity: 'C'  },
    {name: 'Venom Boa'                                , rarity: 'C'  },
    {name: 'Venom Serpent'                            , rarity: 'C'  },
    {name: 'Elemental Hero Neos Alius'                , rarity: 'SR' },
    {name: 'Chthonian Emperor Dragon'                 , rarity: 'UR' },
    {name: 'Aquarian Alessa'                          , rarity: 'SR' },
    {name: 'Lucky Pied Piper'                         , rarity: 'SR' },
    {name: 'Grasschopper'                             , rarity: 'R'  },
    {name: 'Goggle Golem'                             , rarity: 'C'  },
    {name: 'Dawnbreak Gardna'                         , rarity: 'C'  },
    {name: 'Doom Shaman'                              , rarity: 'SR' },
    {name: 'King Pyron'                               , rarity: 'C'  },
    {name: 'Shadow Delver'                            , rarity: 'C'  },
    {name: 'Flint Lock'                               , rarity: 'C'  },
    {name: 'Gravitic Orb'                             , rarity: 'C'  },
    {name: 'Phantom Cricket'                          , rarity: 'C'  },
    {name: 'Crystal Seer'                             , rarity: 'UR' },
    {name: 'Neo Space Pathfinder'                     , rarity: 'R'  },
    {name: 'Frost and Flame Dragon'                   , rarity: 'SCR'},
    {name: 'Desert Twister'                           , rarity: 'UR' },
    {name: 'Ritual Raven'                             , rarity: 'C'  },
    {name: 'Razor Lizard'                             , rarity: 'C'  },
    {name: 'Light Effigy'                             , rarity: 'C'  },
    {name: 'Dark Effigy'                              , rarity: 'C'  },
    {name: 'Zombie Master'                            , rarity: 'SR' },
    {name: 'Neo-Spacian Marine Dolphin'               , rarity: 'C'  },
    {name: 'Elemental Hero Marine Neos'               , rarity: 'R'  },
    {name: 'Elemental Hero Darkbright'                , rarity: 'UR' },
    {name: 'Elemental Hero Magma Neos'                , rarity: 'SCR'},
    {name: 'Ojama Knight'                             , rarity: 'C'  },
    {name: 'Fifth Hope'                               , rarity: 'SR' },
    {name: 'Reverse of Neos'                          , rarity: 'C'  },
    {name: 'Convert Contact'                          , rarity: 'C'  },
    {name: 'Cocoon Party'                             , rarity: 'C'  },
    {name: 'NEX'                                      , rarity: 'C'  },
    {name: 'Cocoon Rebirth'                           , rarity: 'C'  },
    {name: 'Venom Swamp'                              , rarity: 'C'  },
    {name: 'Snake Rain'                               , rarity: 'R'  },
    {name: 'Venom Shot'                               , rarity: 'C'  },
    {name: 'Cyberdark Impact!'                        , rarity: 'SCR'},
    {name: 'Flint Missile'                            , rarity: 'C'  },
    {name: 'Double Summon'                            , rarity: 'R'  },
    {name: 'Summoner\'s Art'                          , rarity: 'R'  },
    {name: 'Creature Seizure'                         , rarity: 'C'  },
    {name: 'Phalanx Pike'                             , rarity: 'R'  },
    {name: 'Symbols of Duty'                          , rarity: 'R'  },
    {name: 'Amulet of Ambition'                       , rarity: 'C'  },
    {name: 'Broken Bamboo Sword'                      , rarity: 'C'  },
    {name: 'Mirror Gate'                              , rarity: 'SR' },
    {name: 'Hero Counterattack'                       , rarity: 'C'  },
    {name: 'Cocoon Veil'                              , rarity: 'C'  },
    {name: 'Snake Whistle'                            , rarity: 'C'  },
    {name: 'Damage = Reptile'                         , rarity: 'R'  },
    {name: 'Snake Deit\'s Command'                    , rarity: 'R'  },
    {name: 'Rise of the Snake Deity'                  , rarity: 'C'  },
    {name: 'Ambush Fangs'                             , rarity: 'C'  },
    {name: 'Venom Burn'                               , rarity: 'C'  },
    {name: 'C Charity'                                , rarity: 'R'  },
    {name: 'Destructive Draw'                         , rarity: 'C'  },
    {name: 'Shield Spear'                             , rarity: 'C'  },
    {name: 'Strike Slash'                             , rarity: 'C'  },
    {name: 'Spell Reclamation'                        , rarity: 'R'  },
    {name: 'Trap Reclamation'                         , rarity: 'R'  },
    {name: 'Gift Card'                                , rarity: 'C'  },
    {name: 'The Gift of Greed'                        , rarity: 'C'  },
    {name: 'Counter Counter'                          , rarity: 'C'  },
    {name: 'Ocean\'s Keeper'                          , rarity: 'R'  },
    {name: 'Thousand-Eyes Jellyfish'                  , rarity: 'R'  },
    {name: 'Cranium Fish'                             , rarity: 'SCR'},
    {name: 'Abyssal Kingshark'                        , rarity: 'SCR'},
    {name: 'Mormolith'                                , rarity: 'SCR'},
    {name: 'Fossil Tusker'                            , rarity: 'R'  },
    {name: 'Phantom Dragonray Bronto'                 , rarity: 'R'  },
    {name: 'Il Blud'                                  , rarity: 'SCR'},
    {name: 'Blazewing Butterfly'                      , rarity: 'SR' },
  ];

  const bp01 = [
    {name: 'Witch of the Black Forest', cardType: 'Effect', rarity: 'S1'},
    {name: 'Cyber Jar'                , cardType: 'Effect', rarity: 'S1'},
    {name: 'Number 39: Utopia'        , cardType: 'Xyz'   , rarity: 'S1'},
    {name: 'Gachi Gachi Gantetsu'     , cardType: 'Xyz'   , rarity: 'S1'},
    {name: 'Raigeki'                  , cardType: 'Spell' , rarity: 'S1'},
    {name: 'Swords of Revealing Light', cardType: 'Spell' , rarity: 'S1'},
    {name: 'Solemn Judgment'          , cardType: 'Trap'  , rarity: 'S1'},
    {name: 'Mirror Force'             , cardType: 'Trap'  , rarity: 'S1'},
    {name: 'Ryko, Lightsworn Hunter'  , cardType: 'Effect', rarity: 'S2'},
    {name: 'Snowman Eater'            , cardType: 'Effect', rarity: 'S2'},
    {name: 'Infected Mail'            , cardType: 'Spell' , rarity: 'S2'},
    {name: 'Ego Boost'                , cardType: 'Spell' , rarity: 'S2'},
    {name: 'Kunai with Chain'         , cardType: 'Trap'  , rarity: 'S2'},
    {name: 'Dust Tornado'             , cardType: 'Trap'  , rarity: 'S2'},
    {name: 'Insect Knight'            , cardType: 'Normal', rarity: 'S3'},
    {name: 'Gene-Warped Warwolf'      , cardType: 'Normal', rarity: 'S3'},
    {name: 'Buster Blader'            , cardType: 'Effect', rarity: 'S3'},
    {name: 'Goblin Attack Force'      , cardType: 'Effect', rarity: 'S3'},
    {name: 'Giant Soldier of Stone'   , cardType: 'Normal', rarity: 'S4'},
    {name: 'Mask of Darkness'         , cardType: 'Effect', rarity: 'S4'},
  ];


    //function takes in an array of numbers, which returns a function that
    //with each call returns one of those numbers
  const returnInOrder = (inNums) => () => inNums.shift();
  const rand = Math.random;

  const count = (array, func) => array.filter(func).length;

  afterEach('After each test', () => {
    Math.random = rand;
  });


  describe('()', () => {
    it('battle pack should contain at least 1 S1, S2, S3, S4 ', (done) => {
      Math.random = () => 0.9;
      boosterCreator(mockCardCreator(bp01))({
        numPacks:1,
        sets: ['Battle Pack: Epic Dawn'],
      }).then((packs) => {
        const numS1 = count(packs[0], (card) => card.rarity === 'S1');
        const numS2 = count(packs[0], (card) => card.rarity === 'S2');
        const numS3 = count(packs[0], (card) => card.rarity === 'S3');
        const numS4 = count(packs[0], (card) => card.rarity === 'S4');

        assert.isTrue(numS1 >= 1, 'BP01 pack should contain at least 1 S1');
        assert.isTrue(numS2 >= 1, 'BP01 pack should contain at least 1 S2');
        assert.isTrue(numS3 >= 1, 'BP01 pack should contain at least 1 S3');
        assert.isTrue(numS4 >= 1, 'BP01 pack should contain at least 1 S4');
        done();
      });
    });


    it('pre tdgs pack should contain 8 common', (done) => {
      Math.random = () => 0.9;
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['Tactical Evolution'],
      })
        .then((packs) => {
          const numC = count(packs[0], (card) => card.rarity === 'C');

          assert.equal(numC, 8);
          done();
        });
    });

    it('pre tdgs pack should contain a R', (done) => {
      Math.random = () => 0.9;
      boosterCreator(mockCardCreator(taev))({
        numPacks:1,
        sets: ['Tactical Evolution'],
      })
        .then((packs) => {
          const numR = count(packs[0], (card) => card.rarity === 'R');

          assert.equal(numR, 1);
          done();
        });
    })

    it('pre tdgs pack should contain a SR', (done) => {
      Math.random = () => 0.25;
      boosterCreator(mockCardCreator(taev))({
        numPacks:1,
        sets: ['Tactical Evolution'],
      })
        .then((packs) => {
          const numSR = count(packs[0], (card) => card.rarity === 'SR');

          assert.equal(numSR, 1);
          done();
        });
    });

    it('pre tdgs pack should contain a UR', (done) => {
      Math.random = () => 1/26;
      boosterCreator(mockCardCreator(taev))({
        numPacks:1,
        sets: ['Tactical Evolution'],
      })
        .then((packs) => {
          const numUR = count(packs[0], (card) => card.rarity === 'UR');

          assert.equal(numUR, 1);
          done();
        });
    });

    it('pre tdgs pack should contain a SCR', (done) => {
      Math.random = () => 1/33;
      boosterCreator(mockCardCreator(taev))({
        numPacks:1,
        sets: ['Tactical Evolution'],
      })
         .then((packs) => {
           const numSCR = count(packs[0], (card) => card.rarity === 'SCR');

           assert.equal(numSCR, 1);
           done();
         });
    });

    it('post tdgs pack should contain 8 common, 1 R', (done) => {
      Math.random = () => 0.9; //.9 is higher chance than getting a shiny (.75)
      boosterCreator(mockCardCreator(taev))({
        numPacks:1,
        sets: ['The Shining Darkness'],
      })
        .then((packs) => {
          const numC  = count(packs[0], (card) => card.rarity === 'C');
          const numR  = count(packs[0], (card) => card.rarity === 'R');

          assert.equal(numC, 8);
          assert.equal(numR, 1);
          done();
        });
    });

    it('post tdgs pack should contain 1 R, 1 SR', (done) => {
      Math.random = () => 0.3; // .25 / .75 = 1/3
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['The Shining Darkness'],
      })
        .then((packs) => {
          const numR  = count(packs[0], (card) => card.rarity === 'R');
          const numSR = count(packs[0], (card) => card.rarity === 'SR');

          assert.equal(numR, 1);
          assert.equal(numSR, 1);
          done();
        });
    });

    it('post tdgs pack should contain 1 R, 1 UR', (done) => {
      Math.random = () => 1/13; // .50 / .75 = 2/3
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['The Shining Darkness'],
      })
        .then((packs) => {
          const numR  = count(packs[0], (card) => card.rarity === 'R');
          const numUR = count(packs[0], (card) => card.rarity === 'UR');

          assert.equal(numR, 1);
          assert.equal(numUR, 1);
          done();
        });
    });

    it('post tdgs pack should contain 1 R, 1 SCR', (done) => {
      Math.random  = () => 1/26; // 0.7 would have been fine
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['The Shining Darkness'],
      })
        .then((packs) => {
          const numR   = count(packs[0], (card) => card.rarity === 'R');
          const numSCR = count(packs[0], (card) => card.rarity === 'SCR');

          assert.equal(numR, 1);
          assert.equal(numSCR, 1);
          done();
        });
    });


    it('<= ast set should contain short prints', (done) => {
      Math.random = () => 0;
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['Ancient Sanctuary'],
      })
        .then((packs) => {
          const numSP = count(packs[0], (card) => card.rarity === 'SP');

          assert.equal(numSP, 8);
          done();
        });
    });

    it('ast < set < ptdn should NOT contain short prints', (done) => {
      Math.random = () => 0;
      boosterCreator(mockCardCreator(taev))({
        numPacks:1,
        sets: ['Tactical Evolution'],
      })
        .then((packs) => {
          const numSP = count(packs[0], (card) => card.rarity === 'SP');

          assert.equal(numSP, 0);
          done();
        });
    });

    it('>= ptdn set should contain short prints', (done) => {
      Math.random = () => 0;
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['Phantom Darkness'],
      })
        .then((packs) => {
          const numSP = count(packs[0], (card) => card.rarity === 'SP');

          assert.equal(numSP, 8);
          done();
        });
    });

    //because AST packs are generated using: 8C + 1 non-C way
    //1/23 is the chance of getting a SCR
    it('set < sod should have an UR rarity of 1/12 (shouldnt contain UR)', (done) => {
      Math.random = returnInOrder([ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, (1/31+ 1/12), 0]);
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['Ancient Sanctuary'],
      })
        .then((packs) => {
          const numUR = count(packs[0], (card) => card.rarity === 'UR');

          assert.equal(numUR, 1 , 'Pre SOD contains a UR at 1/12');
          done();
        });
    });

    it('set < sod should have an UR rarity of 1/12 (should contain UR)', (done) => {
      //check that just a bit more wont work
      Math.random = returnInOrder([ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 1/23+ 1/12 + 1/13, 0]);
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['Ancient Sanctuary'],
      })
        .then((packs) => {
          const numUR = count(packs[0], (card) => card.rarity === 'UR');

          assert.equal(numUR, 0, 'Pre SOD wont contain a UR if the number is a bit above the limit');
          done();
        });
    });

    it('set < sod should have an UR rarity of 1/12 (shouldnt contain UR)', (done) => {
      //check that just a bit less wont work
      Math.random = returnInOrder([ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 1/31 , 0]);
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['Ancient Sanctuary'],
      })
        .then((packs) => {
          const numUR = count(packs[0], (card) => card.rarity === 'UR');

          assert.equal(numUR, 0, 'Pre SOD wont contain a UR if the number is just under the required range');
          done();
        });
    });

    it('sod =< set =< taev should have an UR rarity of 1/24 (should contain UR)', (done) => {
      //taev is 8 + 1
      Math.random = returnInOrder([ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, (1/31+ 1/24), 0]);
      boosterCreator(mockCardCreator(taev))({
        numPacks:1,
        sets: ['Tactical Evolution'],
      })
        .then((packs) => {
          const numUR = count(packs[0], (card) => card.rarity === 'UR');

          assert.equal(numUR, 1, 'SOD =< set <= TAEV contains a UR at 1/24');
          done();
        });
    });

    it('sod =< set =< taev should have an UR rarity of 1/24 (shouldnt contain UR)', (done) => {
      //check that just a bit more wont work
      Math.random = returnInOrder([ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 1/31+ 1/24 + 1/24, 0]);
      boosterCreator(mockCardCreator(taev))({
        numPacks:1,
        sets: ['Tactical Evolution'],
      })
        .then((packs) => {
          const numUR = count(packs[0], (card) => card.rarity === 'UR');

          assert.equal(numUR, 0, 'SOD <= set <= TAEV wont contain a UR if the number is a bit above the limit');
          done();
        });
    });

    it('sod =< set =< taev should have an UR rarity of 1/24 (shouldnt contain UR)', (done) => {
      //check that just a bit less wont work
      Math.random = returnInOrder([ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 1/31 , 0]);
      boosterCreator(mockCardCreator(taev))({
        numPacks:1,
        sets: ['Tactical Evolution'],
      })
        .then((packs) => {
          const numUR = count(packs[0], (card) => card.rarity === 'UR');

          assert.equal(numUR, 0, 'SOD <= set <= TAEV wont contain a UR if the number is just under the required range');
          done();
        });
    });

    it('taev < set should have an UR rarity of 1/12 (should get UR)', (done) => {
      //7+R+UR
      Math.random = returnInOrder([ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 1/31 + 1/12, 0]);
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['The Shining Darkness'],
      })
        .then((packs) => {
          const numUR = count(packs[0], (card) => card.rarity === 'UR');

          assert.equal(numUR, 1, 'TAEV < set contains a UR at 1/12');
          done();
        });
    });

    it('taev < set should have an UR rarity of 1/12 (shouldnt get UR)', (done) => {
      //check that just a bit more wont work
      Math.random = returnInOrder([ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 1/31 + 1/12 + 1/13, 0]);
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['The Shining Darkness'],
      })
        .then((packs) => {
          const numUR = count(packs[0], (card) => card.rarity === 'UR');

          assert.equal(numUR, 0, 'TAEV < set wont contain a UR if the number is a bit above the limit');
          done();
        });
    });

    it('taev < set should have an UR rarity of 1/12 (shouldnt get UR)', (done) => {
      //check that just a bit less wont work
      Math.random = returnInOrder([ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 1/31 , 0]);
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['The Shining Darkness'],
      })
        .then((packs) => {
          const numUR = count(packs[0], (card) => card.rarity === 'UR');

          assert.equal(numUR, 0, 'TAEV < set wont contain a UR if the number is just under the required range');
          done();
        });
    });

    it('set < tshd should have an SCR rarity of 1/31', (done) => {
      //should contain a secret
      // 1/24 to take it just above the range for UR
      Math.random = returnInOrder([ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 1/31 , 0]);
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['The Duelist Genesis'],
      })
        .then((packs) => {
          const numSCR = count(packs[0], (card) => card.rarity === 'SCR');

          assert.equal(numSCR, 1, 'set < TSHD contains a SCR at 1/31');
          done();
        });
    });

    it('set < tshd should have an SCR rarity of 1/31 (shouldnt get SCR)', (done) => {
      //should not contain a secret
      Math.random = returnInOrder([ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 1/31 + 1/32, 0]);
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['The Duelist Genesis'],
      })
        .then((packs) => {
          const numSCR = count(packs[0], (card) => card.rarity === 'SCR');

          assert.equal(numSCR, 0, 'TSHD =< set wont contain a SCR if the number is a bit above 1/31');
          done();
        });
    });

    it('tshd =< set: SCR rarity of 1/23 (should contain SCR)', (done) => {
      // 1/24 to take it just above the range for UR
      Math.random = returnInOrder([ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 1/23 , 0]);
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['The Shining Darkness'],
      })
        .then((packs) => {
          const numSCR = count(packs[0], (card) => card.rarity === 'SCR');

          assert.equal(numSCR, 1, 'TSHD =< set contains a SCR at 1/23');
          done();
        });
    });

    it('tshd =< set: SCR rarity of 1/23 (should not contain SCR)', (done) => {
      Math.random = returnInOrder([ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 1/23 + 1/24, 0]);
      boosterCreator(mockCardCreator(tshd))({
        numPacks:1,
        sets: ['The Shining Darkness'],
      })
        .then((packs) => {
          const numSCR = count(packs[0], (card) => card.rarity === 'SCR');
          assert.equal(numSCR, 0, 'TSHD =< set wont contain a SCR if the number is a bit above 1/23');
          done();
        });
    });
  });
});
