'use strict';
/*eslint-env node, es6, mocha*/

//Tests that the packs are of the correct size and the correct number of cards
//TODO test that the contents of the packs is correct (will require fiddling with Math.random)

const chai = require('chai');
// const assert = chai.assert;
const expect = chai.expect;
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const sinon = require('sinon');

const dbClient = require('./DatabaseClient.js');
const cardCreator = require('../server/CardCreator.js');
describe('CardCreator', () => {
  const twentyCards = [
    {"name": "Asura Priest", "cardType":"Effect", "type":""},
    {"name": "Battle Fader", "cardType":"Effect", "type":""},
    {"name": "Beast King Barbaros", "cardType":"Effect", "type":""},
    {"name": "Blackwing - Zephyros the Elite", "cardType":"Effect", "type":""},
    {"name": "Blue Thunder T-45", "cardType":"Effect", "type":""},
    {"name": "Breaker the Magical Warrior", "cardType":"Effect", "type":""},
    {"name": "Caius the Shadow Monarch", "cardType":"Effect", "type":""},
    {"name": "Card Guard", "cardType":"Effect", "type":""},
    {"name": "Card Trooper", "cardType":"Effect", "type":""},
    {"name": "Cardcar D", "cardType":"Effect", "type":""},
    {"name": "Chaos Sorcerer", "cardType":"Effect", "type":""},
    {"name": "Cyber Dragon", "cardType":"Effect", "type":""},
    {"name": "Cyber Jar", "cardType":"Effect", "type":""},
    {"name": "D.D. Assailant", "cardType":"Effect", "type":""},
    {"name": "D.D. Warrior Lady", "cardType":"Effect", "type":""},
    {"name": "Don Zaloog", "cardType":"Effect", "type":""},
    {"name": "Doomcaliber Knight", "cardType":"Effect", "type":""},
    {"name": "Ehren, Lightsworn Monk", "cardType":"Effect", "type":""},
    {"name": "Evilswarm Thunderbird", "cardType":"Effect", "type":""},
    {"name": "Gorz the Emissary of Darkness", "cardType":"Effect", "type":""},
  ];

  const SINGLE_ELEM_ARR = [''];

  describe('#getCards()', () => {
    afterEach(() => {
      dbClient.getCards.restore();
    });

    it('should create an array containing card objects', () => {
      sinon.stub(dbClient,'getCards').returns(Promise.resolve(twentyCards));

      const createCards = cardCreator('ygo', dbClient);

      return expect(createCards(SINGLE_ELEM_ARR, '13'))
        .eventually.have.length(twentyCards.length);
    });
  });
});
