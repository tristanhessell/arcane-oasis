This will become open after some changes to the codebase.

This project has been created for two reasons:
	To create a tool that my friends can use to draft games.
	To learn about Node and different frameworks, libraries and tools.

For these reasons (mainly the second one), things arent always done in the best way possible.
I'm learning how to do things and why the JS community has moved towards certain technologies.

Learning Markdown is a must too!

I have just found out that AWS is free for the first year!
So movement to AWS will occur at some point

********************************************
Deploy instructions (on heroku):
Push the code to heroku repo
(the db will get set up postinstall)