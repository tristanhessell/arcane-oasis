'use strict';
/*eslint-env browser, es6, jasmine, protractor, commonjs*/

const {login} = require('./utilityFunctions.js');
//these tests can take a while to do all the movements
//"Moave-menT" - Connor McGreggor
jasmine.DEFAULT_TIMEOUT_INTERVAL = 200000;
//pack size is always 14
module.exports = (browser, config, guid) => {
  const BASE_URL = 'http://localhost:3000/#';
  const {numPlayers, deckSize} = config;
  const players = [];

  for (let ii = 0; ii < numPlayers; ii += 1) {
    players.push(browser.forkNewDriverInstance());
  }

  //go to the game page
  players.forEach((player) => {
    player.get(`${BASE_URL}/${config.draftFormat}/${guid}`);
    login(player);
  });

  playBoosterRounds(players, deckSize);

  //check that the dialog containing the deck is open and contains the deck
  players.forEach((player) => {
    expect(player.element.all(by.repeater('card in vm.deckList')).count())
      .toEqual(deckSize);
  });
};


function playBoosterRounds (players, deckSize) {
  players.forEach((player) => {
    player.waitForAngular();
    player.actions()
      .doubleClick(player.element.all(by.repeater('card in vm.pack')).get(0))
      .perform();
  });

  //recheck the deck size
  players[0].element.all(by.repeater('card in vm.selectedCards'))
    .count()
    .then((size) => {
      if (size < deckSize) {
        playBoosterRounds(players, deckSize);
      }
    });
}