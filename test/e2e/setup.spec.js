'use strict';
/*eslint-env es6, jasmine, protractor, commonjs*/

const {
  login,
  logout,
  setUpCubeDraft,
  setUpSetDraft,
  clickButton,
  getGameElement,
  openSettingsDialog,
  openSetSelectDialog,
  clickRepeaterElement,
} = require('./utilityFunctions.js');


describe('Testing Arcane Oasis Setup', () => {
  const BASE_URL = 'http://localhost:3000/#';
  const LOBBY_URL = `${BASE_URL}/lobby`;

  afterEach(() => {
    logout(browser);
  });

	//Just cant get the test to pass anymore
	//TODO fix at a later date
  it('should load the active drafts when refresh is pressed', () => {
    const browser2 = browser.forkNewDriverInstance();
    const config = {
      gameType: 'ygo',
      draftType: 'cube',
      draftFormat: 'rochester',
      numPlayers: 4,
      deckSize: 56,
      packSize: 8,
      //numRounds: 28,
    };

    browser2.get(LOBBY_URL);
    login(browser2);

    setUpCubeDraft(browser, config)
      .then((guid) => {
        getGameElement(browser2.element, guid)
          .then((noDrafts) => {
            //show that the game doesnt exist
            expect(noDrafts.length).toEqual(0);
            // click on the refresh button
            clickButton(browser2, 'Refresh');
            //show that the game does exist now
            getGameElement(browser2.element, guid)
              .then((drafts) => {
                expect(drafts.length).toEqual(1);
              });
          })
      });
  });


  it('Changing the draft format should empty the selected sets', () => {
    const config = {
      gameType: 'ygo',
      draftType: 'booster',
      draftFormat: 'booster',
      numPlayers: 2,
      deckSize: 54,
      // packSize: 9,
      numRounds: 6,
    };

    browser.get(LOBBY_URL);
    login(browser);

    //click on the set up button (to bring up the settings modal)
    openSettingsDialog(browser, config);
    //select a set
    openSetSelectDialog(browser, config, 1);
    //check the number of selected sets
    expect(element.all(by.repeater('set in vm.config.sets')).count()).toEqual(1);
    //change draft type
    clickRepeaterElement('draftType in vm.draftTypes', 'cube');
    //check that there are no sets
    expect(element.all(by.repeater('set in vm.config.sets')).count()).toEqual(0);
  });

  //not working: CBF trying to figure it out why
  xit('should show the settings in the in-game settings dialog', () => {
    const config = {
      gameType: 'ygo',
      draftType: 'cube',
      draftFormat: 'booster',
      numPlayers: 2,
      deckSize: 56,
      packSize: 14,
      numRounds: 4,
    };

    const browser1 = browser;
    const browser2 = browser1.forkNewDriverInstance();

    setUpSetDraft(browser1, config)
      .then((guid) => {
        browser1.get(`${BASE_URL}/${config.draftFormat}/${guid}`);
        browser2.get(`${BASE_URL}/${config.draftFormat}/${guid}`);
        login(browser2);

        // Booster Dialog
        clickButton(browser2, 'Settings');

        //staticSettingsModal
        //check the contents of the page
        expect(browser1.element(by.binding('uid')).getText()).toEqual(guid);
        expect(browser1.element(by.binding('vm.config.gameType')).getText()).toEqual(config.gameType);
        expect(browser1.element(by.binding('vm.config.draftType')).getText()).toEqual(config.draftType);
        expect(browser1.element(by.binding('vm.config.draftFormat')).getText()).toEqual(config.draftFormat);
        expect(browser1.element(by.binding('vm.config.numPlayers')).getText()).toEqual(String(config.numPlayers));
        expect(browser1.element(by.binding('vm.config.numRounds')).getText()).toEqual(String(config.numRounds));
        expect(browser1.element(by.binding('vm.config.packSize')).getText()).toEqual(String(config.packSize));
        expect(browser1.element.all(by.repeater('set in vm.config.sets')).count()).toEqual(1);
      });
  });

  //TODO
  xit('should show the error dialog if the game doesnt exist :(', () => {
    //
  });

  //todo move this test
  it('should show the "game hasnt started yet" dialog if not enough players have joined', () => {
    const config = {
      gameType: 'ygo',
      draftType: 'cube',
      draftFormat: 'booster',
      numPlayers: 2,
      deckSize: 56,
      packSize: 14,
      numRounds: 4,
    };

    const browser1 = browser;
    const browser2 = browser1.forkNewDriverInstance();

    return setUpSetDraft(browser1, config)
      .then((guid) => {
        browser1.get(`${BASE_URL}/${config.draftFormat}/${guid}`);

        expect(browser1.element(by.binding('vm.title')).getText()).toEqual('Waiting for Players');

        browser2.get(`${BASE_URL}/${config.draftFormat}/${guid}`);
        login(browser2);

        expect(browser1.element.all(by.repeater('card in vm.pack')).count()).not.toEqual(0);
        expect(browser2.element.all(by.repeater('card in vm.pack')).count()).not.toEqual(0);
      });
  });

  //todo move this test
  it('should show the dialog if a player joins a game that is already full :(', () => {
    const config = {
      gameType: 'ygo',
      draftType: 'cube',
      draftFormat: 'booster',
      numPlayers: 2,
      deckSize: 56,
      packSize: 14,
      numRounds: 4,
    };
    const browser1 = browser;
    const browser2 = browser1.forkNewDriverInstance();
    const browser3 = browser1.forkNewDriverInstance();

    return setUpSetDraft(browser1, config)
      .then((guid) => {
        browser1.get(`${BASE_URL}/${config.draftFormat}/${guid}`);

        browser2.get(`${BASE_URL}/${config.draftFormat}/${guid}`);
        login(browser2);

        browser3.get(`${BASE_URL}/${config.draftFormat}/${guid}`);
        login(browser3);

        expect(browser3.element(by.binding('vm.title')).getText()).toEqual('Draft is Full');
      });
  });

  it('should set the defaults for a cube rochester format draft', () => {
    const config = {
      gameType: 'ygo',
      draftType: 'cube',
      draftFormat: 'rochester',
      numPlayers: 3,
    // deckSize: 56,
      packSize: 10,
    // numRounds: 6,
    };

    browser.get(LOBBY_URL);
    login(browser);

  //open the settings dialog
    openSettingsDialog(browser, config);

  //check the packSize and numRounds are non-defaults
    // expect(element(by.model('config.numRounds')).getAttribute('value')).toEqual(String(28));
    // expect(element(by.model('config.packSize')).getAttribute('value')).toEqual(String(config.packSize));

  //click on the default button
    clickButton(browser, 'Default');

  //check that the ps and nr are the default values
    expect(element(by.model('vm.config.numRounds')).getAttribute('value')).toEqual(String(28));
    expect(element(by.model('vm.config.packSize')).getAttribute('value')).toEqual(String(8));
  });

  it('should set the defaults for a cube booster type draft', () => {
    const config = {
      gameType: 'ygo',
      draftType: 'cube',
      draftFormat: 'booster',
      numPlayers: 2,
      //deckSize: 54,
      packSize: 9,
      numRounds: 6,
    };

    browser.get(LOBBY_URL);
    login(browser);

  //open the settings dialog
    openSettingsDialog(browser, config);

  //check the packSize and numRounds are non-defaults
    expect(element(by.model('vm.config.numRounds')).getAttribute('value')).toEqual(String(config.numRounds));
    expect(element(by.model('vm.config.packSize')).getAttribute('value')).toEqual(String(config.packSize));

  //click on the default button
    clickButton(browser, 'Default');

  //check that the ps and nr are the default values
    expect(element(by.model('vm.config.numRounds')).getAttribute('value')).toEqual(String(4));
    expect(element(by.model('vm.config.packSize')).getAttribute('value')).toEqual(String(14));
  });

  it('should set the defaults for a booster booster type draft', () => {
    const config = {
      gameType: 'ygo',
      draftType: 'booster',
      draftFormat: 'booster',
      numPlayers: 2,
      deckSize: 54,
      // packSize: 9,
      numRounds: 13,
    };

    browser.get(LOBBY_URL);
    login(browser);

    //open the settings dialog
    openSettingsDialog(browser, config);

    //check the packSize and numRounds are non-defaults
    expect(element(by.model('vm.config.numRounds')).getAttribute('value')).toEqual(String(13));
    expect(element(by.model('vm.config.packSize')).getAttribute('value')).toEqual(String(9));

    //click on the default button
    clickButton(browser, 'Default');

    //check that the ps and nr are the default values
    expect(element(by.model('vm.config.numRounds')).getAttribute('value')).toEqual(String(6));
    expect(element(by.model('vm.config.packSize')).getAttribute('value')).toEqual(String(9));
  });

  it('should set the defaults for a booster rochester type draft', () => {
    const config = {
      gameType: 'ygo',
      draftType: 'booster',
      draftFormat: 'rochester',
      numPlayers: 4,
      // deckSize: 56,
      // packSize: 9,
      // numRounds: 6,
    };

  //Niether PackSize or NumRounds can be changed - they are disabled on the UI

    browser.get(LOBBY_URL);
    login(browser);

  //open the settings dialog
    openSettingsDialog(browser, config);

  //check the packSize and numRounds are non-defaults
    // expect(element(by.model('config.numRounds')).getAttribute('value')).toEqual(String(28));
    // expect(element(by.model('config.packSize')).getAttribute('value')).toEqual(String(9));

  //click on the default button
    clickButton(browser, 'Default');

  //check that the ps and nr are the default values
    expect(element(by.model('vm.config.numRounds')).getAttribute('value')).toEqual(String(28));
    expect(element(by.model('vm.config.packSize')).getAttribute('value')).toEqual(String(9));
  });

});