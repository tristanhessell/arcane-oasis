'use strict';
/*eslint-env browser, es6, jasmine, protractor, commonjs*/

const {login} = require('./utilityFunctions.js');
//pack size is always 14
module.exports = (browser, config, guid) => {
  const BASE_URL = 'http://localhost:3000/#';
  const {numPlayers} = config;
  const players = [];

  for (let ii = 0; ii < numPlayers; ii += 1) {
    players.push(browser.forkNewDriverInstance());
  }

  //go to the game page
  players.forEach((player) => {
    player.get(`${BASE_URL}/${config.draftFormat}/${guid}`);
    login(player);
  });

  //determine the starting player
  getStartingPlayerIndex(players)
    .then((startingPlayer) => {
      playRochesterRounds(players, startingPlayer);

  //For some reason this doesnt work
  // //check that the dialog containing the deck is open and contains the deck
  // players.forEach(function(player){
  //   expect(player.element.all(by.repeater('card in deckList')).count()).toEqual(56);
  // });
    });
};

const getStartingPlayerIndex = (players) =>
  players[0].element(by.binding('vm.currentPlayer'))
    .getText()
    .then((currentPlayer) =>
      protractor.promise.all(
        players.map((player) =>
          player.element(by.binding('vm.username')).getText()
        )
      )
      .then((userNames) => userNames.indexOf(currentPlayer))
    );


const playRochesterRounds = (players, startingPlayerIndex) => {
  //get the order for this round and get each player clicks on the cards
  getRoundOrder(players, startingPlayerIndex, [])
    .forEach((player) => {
      player.actions()
        .doubleClick(player.element.all(by.repeater('card in vm.pack')).get(0))
        .perform();
    });

  //recheck the deck size
  players[0].element.all(by.repeater('card in vm.selectedCards'))
    .count()
    .then((size) => {
      if (size < 56) {
        playRochesterRounds(players, (startingPlayerIndex + 1) % players.length);
      }
    });
};

const getRoundOrder = (players, currIndex, order) => {
  const nextIndex = (currIndex + 1) % players.length;
  //add the player when you get to the node
  order.push(players[currIndex]);

  //if you havent looped around to the starting player
  //add the person to the players left
  if (order.length*2 !== players.length) {
    getRoundOrder(players, nextIndex, order);
  }

  //add the player when you are leaving the node
  order.push(players[currIndex]);

  return order;
}