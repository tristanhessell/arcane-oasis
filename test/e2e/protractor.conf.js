'use strict';
/*eslint-env node, es6*/

exports.config = {
    // seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['*.spec.js'],
    multiCapabilities: [{
        browserName: 'chrome',
    }],
    directConnect: true,
};