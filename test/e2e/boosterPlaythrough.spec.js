'use strict';
/*eslint-env es6, jasmine, protractor, commonjs*/

const {
  logout,
  setUpCubeDraft,
  setUpSetDraft,
} = require('./utilityFunctions.js');
const playBoosterGame = require('./playBooster.js');

//pack size is always 14
describe('Testing Booster Draft playthroughs', () => {
  afterEach(() => {
    logout(browser);
  });

  it('should play a booster booster draft correctly :)', () => {
    const config = {
      gameType: 'ygo',
      draftType: 'booster',
      draftFormat: 'booster',
      numPlayers: 2,
      deckSize: 54,
      //packSize: 9,
      numRounds: 6,
    };

    setUpSetDraft(browser, config)
      .then((guid) => { playBoosterGame(browser, config, guid) });
  });

  it('should play a cube booster draft correctly :)', () => {
    const config = {
      gameType: 'ygo',
      draftType: 'cube',
      draftFormat: 'booster',
      numPlayers: 2,
      deckSize: 56,
      packSize: 14,
      numRounds: 4,
    };

    setUpCubeDraft(browser, config)
      .then((guid) => { playBoosterGame(browser, config, guid) });
  });
});