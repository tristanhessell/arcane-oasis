'use strict';
/*eslint-env es6, jasmine, protractor, commonjs*/

const {
  setUpCubeDraft,
  setUpSetDraft,
  login,
  logout,
} = require('./utilityFunctions.js');
const playRochesterGame = require('./playRochester.js');

const BASE_URL = 'http://localhost:3000/#';

//pack size is always 14

describe('Rochester Draft Playthroughs:', () => {
  afterEach(() => {
    logout(browser);
  });

  it('should play a cube rochester draft correctly :)', () => {
    const config = {
      gameType: 'ygo',
      draftType: 'cube',
      draftFormat: 'rochester',
      numPlayers: 2,
      deckSize: 56,
      packSize: 4,
      // numRounds: 28,
    };

    setUpCubeDraft(browser, config)
      .then((guid) => { playRochesterGame(browser, config, guid) });
  });

  it('should play a set rochester draft correctly :)', () => {
    const config = {
      gameType: 'ygo',
      draftType: 'booster',
      draftFormat: 'rochester',
      numPlayers: 2,
      deckSize: 56,
      //packSize: 9,
      // numRounds: 28,
    };

    setUpSetDraft(browser, config)
      .then((guid) => { playRochesterGame(browser, config, guid) });
  });


  it('should stop a player from selecting a card when its not their turn', () => {
    const config = {
      gameType: 'ygo',
      draftType: 'booster',
      draftFormat: 'rochester',
      numPlayers: 2,
      deckSize: 56,
      //packSize: 9,
      // numRounds: 28,
    };

    const browser1 = browser;
    const browser2 = browser1.forkNewDriverInstance();

    setUpSetDraft(browser1, config)
      .then((guid) => {
        browser1.get(`${BASE_URL}/${config.draftFormat}/${guid}`);

        browser2.get(`${BASE_URL}/${config.draftFormat}/${guid}`);
        login(browser2, 'testuser2', 'password');

        browser1.element(by.binding('currentPlayer')).getText().then((currentPlayer) => {
          browser1.element(by.binding('username')).getText().then((user1) => {
            //if player1 is not the current player, try pick a card
            if (currentPlayer !== user1) {
              browser1.actions()
                .doubleClick(browser1.element.all(by.repeater('card in vm.pack')).get(0))
                .perform();
              expect(browser1.element.all(by.repeater('card in vm.deckList')).count()).toEqual(0);
            } else {
              browser2.actions()
                .doubleClick(browser2.element.all(by.repeater('card in vm.pack')).get(0))
                .perform();
              expect(browser2.element.all(by.repeater('card in vm.deckList')).count()).toEqual(0);
            }
          });
        });
      });
  });

});