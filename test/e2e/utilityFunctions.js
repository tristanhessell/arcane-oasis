'use strict';
/*eslint-env es6, jasmine, protractor, commonjs*/

//Utility functions for making e2e testing easier

const BASE_URL = 'http://localhost:3000/#';
const LOBBY_URL = `${BASE_URL}/lobby`;

function login (browser, username = 'testuser1', password = 'password') {
  browser.element(by.model('vm.username')).clear().sendKeys(username);
  browser.element(by.model('vm.password')).clear().sendKeys(password);
  clickButton(browser, 'Log In');
}

function logout (browser) {
  browser.executeScript('window.sessionStorage.clear();');
  browser.executeScript('window.localStorage.clear();');
}

const clickButton = (browser, btnName) => {
  browser.element(by.buttonText(btnName)).click();
  browser.waitForAngular();
};

const setUpCubeDraft = (browser, config) => {
  //START PAGE
  const element = browser.element; //need to do this so element from the parameter is used
  //(rather than the global browser object)
  const numSets = Math.floor(config.numPlayers/2);
  //go to the page
  browser.get(LOBBY_URL);

  login(browser);

  openSettingsDialog(browser, config);

  //SET DIALOG
  openSetSelectDialog(browser, config, numSets);

  //SETTINGS DIALOG
  //click on the start button (to send the request to the server)
  clickButton(browser, 'Start Draft');

  //JOIN DIALOG
  //check the settings on the join page
  expect(element(by.binding('vm.config.gameType')).getText())
    .toEqual(config.gameType);
  expect(element(by.binding('vm.config.draftType')).getText())
    .toEqual(config.draftType);
  expect(element(by.binding('vm.config.draftFormat')).getText())
    .toEqual(config.draftFormat);
  expect(element(by.binding('vm.config.numPlayers')).getText())
    .toEqual(String(config.numPlayers));
  // expect(element(by.binding('config.numRounds')).getText()).toEqual(String(config.numRounds));
  // expect(element(by.binding('config.packSize')).getText()).toEqual(String(config.packSize));
  expect(element.all(by.repeater('set in vm.config.sets')).count())
    .toEqual(numSets);

  //the join dialog (that contains the games settings) will pop up.
  //get the uid
  return element(by.binding('vm.url'))
    .getText()
    .then((url) => {
      const guid = url.slice(url.indexOf(`${config.draftType}/`) + `${config.draftType}/`.length);
      //close the join dialog
      clickButton(browser, 'Close');

      //check that the game is there (so the friends can access it)
      return getGameElement(element, guid)
        .then((games) => checkGame(games[0], config, guid));
    });
};

const openSettingsDialog = (browser, config) => {
  clickButton(browser, 'Set Up Draft');
  browser.waitForAngular();

  //SETTINGS DIALOG
  element(by.cssContainingText('option', config.gameType)).click();

  //select draft type
  clickRepeaterElement('draftType in vm.draftTypes', config.draftType);

  //select the draft format
  clickRepeaterElement('draftFormat in vm.draftFormats', config.draftFormat);

  //set the number of players
  element(by.model('vm.config.numPlayers')).clear().sendKeys(config.numPlayers);

  if (config.packSize) {
    element(by.model('vm.config.packSize')).clear().sendKeys(config.packSize);
  }

  if (config.numRounds) {
    element(by.model('vm.config.numRounds')).clear().sendKeys(config.numRounds);
  }
};

//click the element with text 'neededText' from repeater (assumed to only be 1)
const clickRepeaterElement = (repeaterString, neededText) => {
  element.all(by.repeater(repeaterString))
    .filter((elem) => elem.getText().then((text) => text === neededText))
    .then((filteredElements) => {
      filteredElements[0].click();
    });
};

const openSetSelectDialog = (browser, config, numSets) => {
  clickButton(browser, 'Select Sets');

  //select the sets
  element.all(by.repeater('set in vm.sets')).then((sets) => {
    for (let ii = 0; ii < numSets; ii += 1) {
      sets[ii].element(by.css('[type="checkbox"]')).click();
    }
  });

  //close the set dialog
  clickButton(browser, 'Select');
};

const setUpSetDraft = (browser, config) => {
  const numSets = 1;
  //need to do this so element from the parameter is used
  //(rather than the global browser object)
  const element = browser.element;

  browser.get(LOBBY_URL);
  //START PAGE
  login(browser);

  //SETTINGS DIALOG
  openSettingsDialog(browser, config);

  //SET DIALOG
  openSetSelectDialog(browser, config, numSets);

  //SETTINGS DIALOG
  //click on the start button (to send the request to the server)
  clickButton(browser, 'Start Draft');

  //check the settings on the join page
  expect(element(by.binding('vm.config.gameType')).getText())
    .toEqual(config.gameType);
  expect(element(by.binding('vm.config.draftType')).getText())
    .toEqual(config.draftType);
  expect(element(by.binding('vm.config.draftFormat')).getText())
    .toEqual(config.draftFormat);
  expect(element(by.binding('vm.config.numPlayers')).getText())
    .toEqual(String(config.numPlayers));
  expect(element.all(by.repeater('set in vm.config.sets')).count())
    .toEqual(numSets);

  //the join dialog (that contains the games settings) will pop up.
  //get the uid
  return element(by.binding('vm.url'))
    .getText()
    .then((url) => {
      const guid = url.slice(url.indexOf(`${config.draftType}/`) + `${config.draftType}/`.length);
      //close the join dialog
      clickButton(browser, 'Close');
      //check that the game is there (so the friends can access it)
      return getGameElement(element, guid)
        .then((gme) => checkGame(gme[0], config, guid));
    });
};

const checkGame = (gameElement, config, guid) => {
  //check that the other draft settings are correct
  expect(gameElement.element(by.binding('draft.gameType')).getText())
    .toEqual(config.gameType);
  expect(gameElement.element(by.binding('draft.draftType')).getText())
    .toEqual(config.draftType);
  expect(gameElement.element(by.binding('draft.numPlayers')).getText())
    .toEqual(String(config.numPlayers));

  //return the GUID so other tests can be done
  return guid;
};

//get the row that has the GUID that we found before
const getGameElement = (element, guid) =>
  element.all(by.repeater('draft in vm.activeDrafts'))
    .filter((elem) =>
      elem.element(by.binding('draft.uid'))
        .getText()
        .then((uid) =>  uid === guid)
    );

module.exports = {
  clickButton,
  setUpCubeDraft,
  setUpSetDraft,
  checkGame,
  openSettingsDialog,
  openSetSelectDialog,
  getGameElement,
  clickRepeaterElement,
  login,
  logout,
};