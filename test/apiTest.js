'use strict';
/*eslint-env node, es6, mocha*/

require('dotenv').config();
const supertest = require('supertest');
const sinon = require('sinon');
//AUT
const server = require('../server.js');

const accountClient = require('../server/AccountClient.js');
const cardClient = require('../server/DatabaseClient.js');

describe('Server Routing', () => {
  let token;

  before((done)=> {
    const accountClientMock = sinon.stub(accountClient, 'validateAccount')
      .returns(Promise.resolve());

    supertest(server)
      .post('/api/login')
      .end((err, res) => {
        token = res.body.token;
        accountClientMock.restore();
        done();
      });
  });

  after((done)=> {
    done();
  });

  //TODO wju does this work? i havent used tokens
  it('GET should get lists for a game and draft', (done) => {
    const lists = [{}];
    const cardClientMock = sinon.stub(cardClient, 'getLists')
      .returns(Promise.resolve(lists));

    supertest(server)
      .get('/api/lists/ygo/cube')
      .set('authorization', token)
      .expect('Content-Type', /json/)
      .expect(200, {lists})
      .end((err) => {
        cardClientMock.restore();
        if (err) throw err;
        done();
      });
  });

  it('GET should get a specified list for a game and draft', (done) => {
    const list = [{
      cardType: 'Spell',
    }];
    const transformedList = [{
      cardType: 'Spell',
      textColour: '#FFFFFF',
      colour: ["#1e8725"],
    }];
    const cardClientMock = sinon.stub(cardClient, 'getCards')
      .returns(Promise.resolve(list));

    supertest(server)
      .get('/api/lists/ygo/cube/list')
      .set('authorization', token)
      .expect('Content-Type', /json/)
      .expect(200, {cards: transformedList})
      .end((err) => {
        cardClientMock.restore();
        if (err) throw err;
        done();
      });
  });

  it('POST should add a specified list to draft', (done) => {
    const postData = {
      name: '',
      list: '',
      owner: '',
    };
    //TODO change this to what is meaningful
    const status = [{
      cardType: 'Spell',
    }];
    const cardClientMock = sinon.stub(cardClient, 'addList')
      .returns(Promise.resolve(status));

    supertest(server)
      .post('/api/lists/ygo/cube/')
      .set('authorization', token)
      .send(postData)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err) => {
        cardClientMock.restore();
        if (err) throw err;
        done();
      });
  });

  //TODO finish this test
});