'use strict';
/*eslint-env node, es6*/

const express = require('express');
const app     = express();
const server  = require('http').createServer(app);
const bdParser = require('body-parser');
const winston = require('winston');

app.use(bdParser.urlencoded({extended: true}));
app.use(bdParser.json());

//used to serve the files required for the HTML pages
app.use(express.static('public'));

app.use('/api', require('./routes'));

server.listen(Number(process.env.PORT), () => {
  winston.info(`AO Server started on port ${process.env.PORT}`);
});

module.exports = server;
