'use strict';
/*eslint-env node, es6*/
const winston = require('winston');
const socketio = require('socket.io');
const uuid  = require('uuid');
const socketJwt = require('socketio-jwt');

const activeDrafts = require('./server/ActiveDrafts.js');
const server = require('./server.js');

const io = socketio.listen(server);

const gameMap = {
  ygo: require('./server/ygo/'),
};

const draftFormatMap = {
  booster  : require('./server/BoosterDraft.js'),
  rochester: require('./server/RochesterDraft.js'),
};

const tableFactory = require('./server/Table.js');
const config = require('./config.js');

const cardDbClient = require('./server/DatabaseClient.js');
const getSet = require('./getSet.js');

module.exports = io;

//TODO obviously need to split up this file

io.use(socketJwt.authorize({
  secret: 'secret',
  handshake: true,
}));

io.on('connection', (socket) => {
  let currentDraftId;
  let username = socket.decoded_token.username;


  socket.on('get drafts', () => {
    emitDrafts();
  });

  function emitDrafts () {
    const drafts = Object.keys(activeDrafts).map((draftid) => {
      return {
        uid:     draftid,
        gameType:  activeDrafts[draftid].settings.gameType,
        numPlayers:  activeDrafts[draftid].settings.numPlayers,
        draftType:   activeDrafts[draftid].settings.draftType,
        draftFormat: activeDrafts[draftid].settings.draftFormat,
      };
    });

    io.emit('drafts', {drafts});
  }

  socket.on('get draft settings', (inUid) => {
    if (!activeDrafts[inUid]) {
      return emitDraftError({
        title: 'Cannot find draft',
        message: 'No draft matching the given draft id',
      });
    }

    socket.emit('draft settings', activeDrafts[inUid].settings);
  });

  //if its booster rochester, max 4 players,
  //if its a booster, the pack size needs to be 9
  //if its a cube rochester, min size is 2*num Players

  //this doesnt need to be a promise, but just future proofing it for later
  const validateSettings = (params) => new Promise((resolve, reject) => {
    const numPlayers  = Number(params.numPlayers);
    const draftFormat = params.draftFormat && params.draftFormat.toLowerCase();
    const gameType  = params.gameType && params.gameType.toLowerCase();

    const MAX_PLAYERS = 8;
    const MIN_PLAYERS = 2;

    if (MIN_PLAYERS <= numPlayers && numPlayers  <= MAX_PLAYERS && //check valid players
      (config.draftFormats.find((df) => df === draftFormat)) &&  //check valid draft format
      (config.gameTypes.find((gt) => gt === gameType))) {    //check valid game type

      if (gameMap[gameType].validateParams(params)) {
        let numPacks = params.numRounds * numPlayers;

        //rochester drafs are one pack per round (between everyone
        if (draftFormat === 'rochester') {
          numPacks = params.numRounds;
        }

        resolve({
          numPlayers,
          draftFormat,
          gameType,
          numPacks,
          sets : params.sets,
          draftType: params.draftType,
          packSize: params.packSize,
          numRounds: params.numRounds,
        });
        return;
      }
    }
    //if the parameters aren't valid, return undefined
    reject('Invalid draft parameter/s');
  });

  //ASSERTION the params have been sorted at this point
  const getPacks = (draftParams) => {
    const {gameType, draftType, sets, numPacks, packSize, owner} = draftParams;
    const packCreator = gameMap[gameType].createBoosters(draftType);

    return packCreator({numPacks, sets, packSize, owner});
  };


  socket.on('start draft', (inConfig) => {
    validateSettings(inConfig)
      .then((settings) => getPacks(settings)
        .then((packs) => {
          const table = tableFactory(settings.numPlayers);
          const draftConfig = {
            packs,
            table,
          };
          const uid = uuid.v4();

          activeDrafts[uid] = {
            settings,
            draft :draftFormatMap[settings.draftFormat](draftConfig),
            table,
          };

          winston.info(`Draft Created: ID: ${uid} Settings:`);
          winston.info(settings);

          socket.emit('new draft', uid);
          emitDrafts();
        })
      ).catch((message) => {
        winston.info(message);
        emitDraftError({message:message});
      });
  });


  //called when a player elects to end the game
  const endDraft = () => {
    if (activeDrafts[currentDraftId]) {
      winston.info(`Draft ${currentDraftId} end by election`);

      winston.info(`Draft ${currentDraftId} deleted`);
      delete activeDrafts[currentDraftId];
      //tell everyone (ignored by the people in actual drafts)
      //the game is over so it can be removed from the lists

      emitDrafts();
      //send message to everyone in that draft that the game is over
      activeDrafts[currentDraftId].table.broadcastMessage('draft over',
        'The draft has manually ended');
    }
  };

  //TODO this will need to change i think
  socket.on('error', (err) => {
    emitError(err);
  });

  //cant emit 'error' as its reserved
  function emitError (err) {
    winston.error(`socket Error: ${currentDraftId}`);
    winston.error(err);
    socket.emit('socket error', err);
  }

  ///emits {title, message}
  function emitDraftError (err) {
    winston.error(`Draft Error: ${currentDraftId}`);
    winston.error(err);
    socket.emit('draft error', err);
  }

  socket.on('join draft', (details) => {
    username = details.username;
    currentDraftId = details.uid;

    if (!activeDrafts[currentDraftId]) {
      emitDraftError({
        title: 'Draft does not exist',
      });
      socket.disconnect();
      return;
    }

    try {
      activeDrafts[currentDraftId].draft.addPlayer(socket, username);

      winston.info(`Draft: ${currentDraftId} Player Added: ${username},` +
        `socketid: ${socket.id}`);

    } catch (ex) {
      //TODO draft may also not exist
      socket.emit('draft full', currentDraftId);
      //Close the socket if the draft is full
      socket.disconnect();
      winston.info(`Draft: ${currentDraftId} socket rejected: ${ex}`);
    }
  });

  socket.on('end', endDraft);

  socket.on('card selected', (card) => {
    activeDrafts[currentDraftId].draft &&
      activeDrafts[currentDraftId].draft.pickCard(socket, card);
  });

  socket.on('disconnect', () => {
    if (currentDraftId && activeDrafts[currentDraftId]) {
      const {table, draft} = activeDrafts[currentDraftId];

      //if the socket was actually a part of the current draft
      if (table && table.removePlayer(socket.id)) {
        if (draft.isFinished()) {
          //if the draft is finished, its okay when people leave
          //if the draft hasnt started, its also fine
          return;
        }
        if (draft.hasStarted()) {
          //but if the draft has started,
          //broadcast to everyone that the draft is
          //over because someone has left
          table.broadcastMessage('draft error',
            'Player left the draft before it finished.');
          winston.info(`Draft ${currentDraftId} Ended: Player left early`);

          winston.info(`Draft ${currentDraftId} deleted`);
          delete activeDrafts[currentDraftId];
          //tell everyone (ignored by the people in actual drafts)
          //the game is over so it can be removed from the lists
          emitDrafts();
        }
        //if the draft hasnt started, no worries
      }
    }
  });

  //////////////////LIST SOCKET STUFF
  socket.on('get list', (gameType, draftType, name, inOwner) => {
    const owner = inOwner || undefined; //dont want it to be null
    cardDbClient.getCards(gameType, draftType, name, owner)
      .then((listObj) => {
        socket.emit('list', {name, list:  listObj.list, owner: listObj.owner});
      })
      .catch((err) => {
        winston.info(err);
        emitError({
          message: `could not get list ${name} for ${gameType}:${draftType}`,
        });
      });
  });

  socket.on('get lists', (gameType, draftType) => {
    cardDbClient.getLists(gameType, draftType)
      .then((lists) => {
        socket.emit('lists', lists);
      })
      .catch((err) => {
        winston.info(err);
        emitError({message: `could not get lists for ${gameType}:${draftType}`});
      });
  });

  socket.on('add list', (gameType, draftType, listObj) => {
    listObj.owner = username;

    cardDbClient.addList(gameType, draftType, listObj)
      .then(() => {
        socket.emit('added list', listObj.name);
      })
      .catch((err) => {
        winston.info(err);
        emitError({message: `could not add list ${listObj.name} for ${gameType}:${draftType}`});
      });
  });

  socket.on('get sets', (cards) => {
    Promise.all(cards.map((card) => getSet(card).catch(() => '')))
      .then((sets) => {
        socket.emit('set names', sets);
      });
  });
  ////////////////////END LIST SOCKET STUFF
});
