'use strict';
/*eslint-env node, es6*/

require('dotenv').config({silent: true});
require('./server.js');
require('./socket.js');