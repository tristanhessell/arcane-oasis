'use strict';
/*eslint-env browser, es6, commonjs*/

const map = {
  mtg: '',
  ygo: 'https://yugiohprices.com/api/card_image',
};

module.exports = exports = (gameType) => {
  return map[gameType];
};
