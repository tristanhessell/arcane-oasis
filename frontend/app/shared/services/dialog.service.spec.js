'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals assert, inject*/

const angular = require('angular');
require('angular-mocks');

//TODO not sure of a good way to test here
xdescribe('dialogService', () => {
    let $httpBackend;
    // let dialogService;


    beforeEach(angular.mock.module('arcaneOasis', ($urlRouterProvider) => {
        $urlRouterProvider.deferIntercept();
    }));

    beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');

        // dialogService = $injector.get('dialogService');
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    it('should open a dialog when requested', () => {
        assert.fail();
    });

    it('should be able to close a dialog programatically', () => {
        assert.fail();
    });

    it('should be able to get the result of a dialog when closing', () => {
        assert.fail();
    });

    it('should be able to track when a dialog is dismissed', () => {
        assert.fail();
    });
});
