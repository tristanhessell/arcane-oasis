'use strict';
/*eslint-env browser, es6, commonjs*/
const angular = require('angular');

angular.module('arcaneOasis')
  .factory('userService', userService);

userService.$inject = ['store'];

function userService (store) {
  return {
    setUser,
    getUser,
    removeUser,
  };

  function getUser () {
    return store.get('user');
  }

  function setUser (user) {
    store.set('user', user);
  }

  function removeUser () {
    store.remove('user');
  }
}