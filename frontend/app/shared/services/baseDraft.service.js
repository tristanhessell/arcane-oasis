'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular
  .module('arcaneOasis')
  .factory('baseDraft', baseDraftService);

baseDraftService.$inject = ['socket', '$rootScope'];

function baseDraftService (socket, $rootScope) {
  let uid;

  socket.on('draft full', (inUid) => {
    if (inUid === uid) {
      $rootScope.$broadcast('draft error', {
        title: 'Draft is Full',
        message: 'You didnt get in quick enough! There is already enough players in this drafting session',
      });
    }
  })

  socket.on('draft over', () => {
    $rootScope.$broadcast('draft error', {
      title: 'Draft Ended',
    });
  });

  socket.on('draft error', (error) => {
    $rootScope.$broadcast('draft error', {
      title: error.title || 'Draft Ended',
      message: error.message,
    });
  });

  socket.on('all players', (allPlayers) => {
    $rootScope.$broadcast('all players', allPlayers);
  });

  socket.on('new pack', (newPack) => {
    $rootScope.$broadcast('new pack', newPack);
  });

  socket.on('game started', () => {
    $rootScope.$broadcast('draft started');
  });

  return {
    selectCard,
    joinDraft,
  };

  function joinDraft (data) {
    uid = data.uid;

    socket.emit('join draft', {
      uid: data.uid,
      username: data.username,
    });
  }

  function selectCard (card) {
    socket.emit('card selected', card);
  }
}
