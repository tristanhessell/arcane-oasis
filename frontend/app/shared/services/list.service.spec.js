'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('cardService', () => {
    let $httpBackend;
    let cardService;

    beforeEach(angular.mock.module('arcaneOasis', ($urlRouterProvider) => {
        $urlRouterProvider.deferIntercept();
    }));


    beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');

        cardService = $injector.get('cardService');
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    it('should get the card data from the server', (done) => {
        const expectedCards = [1,2,3,4,5];
        const gameType = 'a';
        const draftType = 'b';
        const selectedList = 'c';

        $httpBackend
            .expectGET(`api/lists/${gameType}/${draftType}/${selectedList}`)
            .respond({
                cards: expectedCards,
            });

        cardService
            .getCards(gameType, draftType, selectedList)
            .then((cards) => {
                assert.deepEqual(cards, expectedCards);
                done();
            });

        $httpBackend.flush();
    });

    it('should get the lists data from the server', (done) => {
        const expectedLists = [1,2,3,4,5];
        const gameType = 'a';
        const draftType = 'b';

        $httpBackend
            .expectGET(`api/lists/${gameType}/${draftType}`)
            .respond({
                lists: expectedLists,
            });

        cardService
            .getLists(gameType, draftType)
            .then((lists) => {
                assert.deepEqual(lists, expectedLists);
                done();
            });

        $httpBackend.flush();
    });
});
