'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular
  .module('arcaneOasis')
  .factory('authService', authService)

authService.$inject = ['$http', '$q', 'userService', 'socket'];

function authService ($http, $q, userService, socket) {
  return {
    login,
    isAuthenticated,
  };

  //login is used to get a token from the server
  //using the supplied credentials and saving it
  function login (username, password) {
    const deferred = $q.defer();

    $http.post('/api/login', {
      username,
      password,
    }).then((result) => {
      const user = result.data;
      userService.setUser(user);
      socket.connect();
      deferred.resolve(user);
    }, (error) => {
      deferred.reject(error);
    });

    return deferred.promise;
  }

  //isAuthenticated is used to check whether the
  //user's token is still valid
  function isAuthenticated () {
    const deffered = $q.defer();
    const user = userService.getUser();

    if (!user) {
      return $q.reject({
        authenticated: false,
        reason: 'No user in memory',
      });
    }

    $http.post('/api/auth', {
      token: user.token,
    }).then((/*result*/) => {
      deffered.resolve(true);
    }, (/*error*/) => {
      deffered.reject({
        authenticated: false,
        reason: 'Invalid Token',
      });
    });

    return deffered.promise;
  }
}
