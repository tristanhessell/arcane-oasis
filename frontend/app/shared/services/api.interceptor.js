'use strict';
/*eslint-env browser, es6, commonjs*/
const angular = require('angular');

//Implementation based off
////http://onehungrymind.com/winning-http-interceptors-angularjs/

//response error should reject the promise
//// https://github.com/angular/angular.js/issues/2609
angular.module('arcaneOasis')
  .factory('apiInterceptor', apiInterceptor);

apiInterceptor.$inject = ['$rootScope', 'userService', '$q'];

function apiInterceptor ($rootScope, userService, $q) {
  return {
    request,
    responseError,
  };

  function request (config) {
    const user = userService.getUser();
    //add the user token if the request is local
    if (user && user.token && !config.url.includes('http')) {
      config.headers.authorization = user.token;
    }

    return config;
  }

  //TODO not actually using this part yet
  function responseError (response) {
    console.log('Response Error')
    console.log(response);

    // if (response.status === 401) {
    //   $rootScope.$broadcast('unauthorized');
    // }

    // return response;
    return $q.reject(response);
  }
}