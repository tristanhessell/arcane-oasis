'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular
  .module('arcaneOasis')
  .factory('signupService', signupService);

signupService.$inject = ['$http'];

function signupService ($http) {
  return {
    signUp,
    checkUsername,
  };

  function signUp (params) {
    return $http.post('api/users', params);
  }

  function checkUsername (username) {
    return $http.get(`api/users/${username}/`)
      .then((res) => res.data.found);
  }
}