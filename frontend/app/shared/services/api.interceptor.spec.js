'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals sinon, assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('apiInterceptor', () => {
    let $httpBackend;
    let rootScope;
    let apiInterceptor;
    let userService;

    beforeEach(angular.mock.module('arcaneOasis', ($urlRouterProvider) => {
        $urlRouterProvider.deferIntercept();
    }));

    beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');
        rootScope = $injector.get('$rootScope');
        userService = $injector.get('userService');
        apiInterceptor = $injector.get('apiInterceptor');

        //need to stub this so it doesnt go and do things for the
        //app (as we are broadcasting around the whole app)
        //need to return {} because events that are broadcasted
        //return the event object when they are finished
        ////See: http://stackoverflow.com/questions/22721657/typeerror-cannot-read-property-defaultprevented-of-undefined
        sinon.stub(rootScope, '$broadcast').returns({});
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should add the token to the request headers', () => {
        const config = {
            headers: {},
        };
        const user = {
            token: 123456,
        };

        sinon.stub(userService, 'getUser').returns(user);

        apiInterceptor.request(config);
        assert.equal(config.headers.authorization, user.token);
    });

    it('should not change the request if there is no user authenticated', () => {
        const config = {
            headers: {},
        };
        const user = {};

        const getUserStub = sinon.stub(userService, 'getUser').returns(undefined);

        apiInterceptor.request(config)
        assert.deepEqual(apiInterceptor.request(config), config);

        getUserStub.returns(user);
        apiInterceptor.request(config)
        assert.deepEqual(apiInterceptor.request(config), config);
    });

    //TODO not testing as not sure if will use this behaviour
    xit('should broadcast error when request is unauthorized', () => {
        const response = {
            status: 401,
        };

        apiInterceptor.responseError(response);
        rootScope.$broadcast.calledWith('unauthorized');
    });

});
