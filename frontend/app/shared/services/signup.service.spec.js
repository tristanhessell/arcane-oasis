'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('draftService', () => {
  let $httpBackend;
  let draftService;

  beforeEach(angular.mock.module('arcaneOasis', ($urlRouterProvider) => {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(($injector) => {
    $httpBackend = $injector.get('$httpBackend');

    draftService = $injector.get('draftService');
  }));

  afterEach(() => {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });


  it('should get the drafts from the server', (done) => {
    const expectedDrafts = [1, 2, 3, 4, 5];

    $httpBackend
      .expectGET('api/drafts')
      .respond({
        drafts: expectedDrafts,
      });

    draftService
      .getDrafts()
      .then((drafts) => {
        assert.deepEqual(drafts, expectedDrafts);
        done();
      });

    $httpBackend.flush();
  });

  it('should get the draft settings', (done) => {
    const expectedSettings = [1, 2, 3, 4, 5];
    const uid = 12345;

    $httpBackend
      .expectGET(`api/drafts/${uid}/settings`)
      .respond({
        settings: expectedSettings,
      });

    draftService
      .getDraftSettings(uid)
      .then((settings) => {
        assert.deepEqual(settings, expectedSettings);
        done();
      });

    $httpBackend.flush();
  });

  it('should start a draft and return the draft id', (done) => {
    const config = { config: 13 };
    const expectedUid = 12345;

    $httpBackend
      .expectPOST(`api/drafts/`, config)
      .respond({
        uid: expectedUid,
      });

    draftService
      .startDraft(config)
      .then((uid) => {
        assert.deepEqual(uid, expectedUid);
        done();
      });

    $httpBackend.flush();
  });

});
