'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

//TODO dont use the sockets like they are used in the last 2

angular
  .module('arcaneOasis')
  .factory('draftService', draftService);

draftService.$inject = ['socket', '$rootScope'];

function draftService (socket, $rootScope) {
  socket.on('drafts', (resp) => {
    $rootScope.$broadcast('drafts', resp.drafts);
  });

  socket.on('draft settings', (settings) => {
    $rootScope.$broadcast('draft settings', settings);
  });

  socket.on('new draft', (uid) => {
    $rootScope.$broadcast('new draft', uid);
  });

  return {
    getDrafts,
    getDraftSettings,
    startDraft,
  };

  function getDrafts () {
    socket.emit('get drafts');
  }

  function getDraftSettings (uid) {
    socket.emit('get draft settings', uid);
  }

  function startDraft (config) {
    socket.emit('start draft', config);
  }
}
