'use strict';
/*eslint-env browser, es6, commonjs*/
const angular = require('angular');

angular.module('arcaneOasis')
  .factory('dialogService', dialogService);

dialogService.$inject = ['$uibModal'];

//TODO change params to 'options'?
////SPIKE: what is the JS standard?
function dialogService ($uibModal) {
  return {
    openGeneric,
    openConfirmation,
    openDecklist,
    openError,
    openJoin,
    openSettings,
    openSets,
    openStaticSettings,
    openSave,
    openLoad,
  };

  function openGeneric (params) {
    const options = {
      templateUrl: '/app/shared/dialogs/message/message.html',
      controller: 'MessageDialogController',
      params,
    };

    return openDialog(options);
  }

  function openConfirmation (params) {
    const options = {
      templateUrl: '/app/shared/dialogs/confirm/confirm.html',
      controller: 'ConfirmDialogController',
      params,
    };

    return openDialog(options);
  }

  function openDecklist (params) {
    const options = {
      templateUrl: '/app/shared/dialogs/decklist/deckList.html',
      controller: 'DeckListDialogController',
      params,
    };

    return openDialog(options);
  }

  function openError (params) {
    const options = {
      templateUrl: '/app/shared/dialogs/error/error.html',
      controller: 'ErrorDialogController',
      params,
    };

    return openDialog(options);
  }

  function openJoin (params) {
    const options = {
      templateUrl: '/app/shared/dialogs/join/join.html',
      controller: 'JoinDialogController',
      params,
    };

    return openDialog(options);
  }

  function openStaticSettings (params) {
    const options = {
      templateUrl: '/app/shared/dialogs/settings/staticSettings.html',
      controller: 'StaticSettingsDialogController',
      params,
    };

    return openDialog(options);
  }

  function openSettings () {
    const options = {
      templateUrl: '/app/shared/dialogs/settings/settings.html',
      controller: 'SettingsDialogController',
    };

    return openDialog(options);
  }

  function openSets (params) {
    const options = {
      templateUrl: 'app/shared/dialogs/set/set.html',
      controller: 'SetDialogController',
      params,
    };

    return openDialog(options);
  }

  function openSave (params) {
    const options = {
      templateUrl: 'app/shared/dialogs/input/input.html',
      controller: 'SaveDialogController',
      params,
    };

    return openDialog(options);
  }

  function openLoad (params) {
    const options = {
      templateUrl: 'app/shared/dialogs/input/load.html',
      controller: 'LoadDialogController',
      params,
    };

    return openDialog(options);
  }


  function openDialog(options) {
    return $uibModal.open({
      animation: true,
      templateUrl: options.templateUrl,
      controller: options.controller,
      size: 'lg',
      controllerAs: 'vm',
      backdrop: 'static',
      resolve: {
        params : () => options.params,
      },
    });
  }

}
