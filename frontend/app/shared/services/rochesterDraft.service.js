'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular
  .module('arcaneOasis')
  .factory('rochesterDraft', rochesterDraftService);

rochesterDraftService.$inject = ['socket', '$rootScope'];

function rochesterDraftService (socket, $rootScope) {
  socket.on('new turn player', (turnPlayer) => {
    $rootScope.$broadcast('new turn player', turnPlayer);
  });

  socket.on('card taken', (card) => {
    $rootScope.$broadcast('card taken', card);
  });

  return {};
}
