'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular
  .module('arcaneOasis')
  .factory('listService', listService);

listService.$inject = ['$q', '$rootScope', 'socket'];

function listService ($q, $rootScope, socket) {
  socket.on('list', (listObj) => {
    $rootScope.$broadcast('list', listObj);
  });

  socket.on('lists', (lists) => {
    $rootScope.$broadcast('lists', lists);
  });

  socket.on('added list', (listName) => {
    $rootScope.$broadcast('added list', listName);
  });

  socket.on('set names', (sets) => {
    $rootScope.$broadcast('set names', sets);
  });

  return {
    getCards,
    getLists,
    addList,
    getSets,
  };

  //TODO rename to getList
  function getCards (gameType, draftType, selectedList, owner) {
    socket.emit('get list', gameType, draftType, selectedList, owner);
  }

  function getLists (gameType, draftType) {
    socket.emit('get lists', gameType, draftType);
  }

  function addList (gameType, draftType, listObj) {
    socket.emit('add list', gameType, draftType, listObj);
  }

  //TODO move this somewhere appropriate/remove it when able to
  function getSets (cards) {
    socket.emit('get sets', cards.map((card) => card.name));
  }
}
