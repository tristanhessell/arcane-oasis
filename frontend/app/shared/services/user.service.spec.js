'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals sinon, assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('userService', () => {
    let $httpBackend;
    let store;
    //AUT
    let userService;

    beforeEach(angular.mock.module('arcaneOasis', ($urlRouterProvider) => {
        $urlRouterProvider.deferIntercept();
    }));

    beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');

        userService = $injector.get('userService');
        store = $injector.get('store');
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should get the user from storage', () => {
        const user = {name: 'tristan'};

        sinon.stub(store, 'get').returns(user);
        assert.deepEqual(userService.getUser(), user, 'could not get user from storage');
    });

    it('should set the user into storage', () => {
        const user = {name: 'tristan'};
        const setStub = sinon.stub(store, 'set');

        userService.setUser(user);
        assert(setStub.calledWith('user', user), 'could not set user into storage');
    });

});
