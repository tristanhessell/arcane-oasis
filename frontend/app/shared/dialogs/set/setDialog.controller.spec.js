'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals sinon, assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('SetDialogController', () => {
  let createController;
  let modalInstance;
  let cardService;
  let $httpBackend;
  let scope;
  let $q;

  const mockModalInstance = () => {
    let data;
    let dismissReason;

    return {
      dismiss: (reason) => { dismissReason = reason;},
      close: (inData) => {data = inData;},
      getData: () => data,
      getReason: () => dismissReason,
    };
  }

  beforeEach(angular.mock.module('arcaneOasis', ($urlRouterProvider) => {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(angular.mock.module('myDialogs'));

  beforeEach(inject(($injector) => {
    $httpBackend = $injector.get('$httpBackend');
    cardService = $injector.get('cardService');

    scope = $injector.get('$rootScope').$new();
    $q = $injector.get('$q');

    const deferred = $q.defer();
    deferred.resolve([1,2,3,4]);

    sinon.stub(cardService, 'getLists')
      .returns(deferred.promise);

    createController = (config) => {
      modalInstance = mockModalInstance();
      const controller = $injector.get('$controller')('SetDialogController', {
        '$uibModalInstance': modalInstance,
        'config' : config,
        cardService,
      });
      scope.$apply();

      return controller;
    };
  }));

  afterEach(() => {
    cardService.getLists.restore();
  });

  afterEach(() => {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should enforce maximum selection if specified', () => {
    const vm = createController({
      maxSets: 2,
      gameType: 'ygo',
      draftType: 'booster',
    });

    assert.equal(vm.deckText, 'Select 2 Set/s');
    //not enough picked
    vm.sets[0].winner = true;
    vm.checkChanged(vm.sets[0]);
    vm.select(); //try to close the dialog
    assert.equal(modalInstance.getData(), undefined);

    //too many picked
    vm.sets[1].winner = true;
    vm.checkChanged(vm.sets[1]);
    vm.sets[2].winner = true;
    vm.checkChanged(vm.sets[2]);
    vm.select();
    assert.equal(modalInstance.getData(), undefined);

    //just enough
    vm.sets[2].winner = false;
    vm.checkChanged(vm.sets[2]);
    vm.select();
    assert.deepEqual(modalInstance.getData(), [1,2]);
  });

  it('should enforce minimum selection if specified', () => {
    const vm = createController({
      minSets: 2,
      gameType: 'ygo',
      draftType: 'booster',
    });

    assert.equal(vm.deckText, 'Select 2 Set/s minimum');

    //not enough sets collected
    vm.sets[0].winner = true;
    vm.checkChanged(vm.sets[0]);
    vm.select(); //try to close the dialog
    assert.equal(modalInstance.getData(), undefined);

    //more than enough selected
    vm.sets[1].winner = true;
    vm.checkChanged(vm.sets[1]);
    vm.sets[2].winner = true;
    vm.checkChanged(vm.sets[2]);
    vm.select(); //should actually close the dialog
    assert.deepEqual(modalInstance.getData(), [1,2,3]);
  });

  it('should close the dialog when close is called', () => {
    const vm = createController({
      gameType: 'ygo',
      draftType: 'booster',
    });

    vm.close();
    assert.equal(modalInstance.getReason(), 'closed');
  });
});
