'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('myDialogs')
  .controller('SetDialogController', SetDialogController);

SetDialogController.$inject = ['$uibModalInstance', 'params', 'listService', '$scope'];

function SetDialogController ($uibModalInstance, params, listService, $scope) {
  const {draftType, gameType} = params;
  const inSets = params.sets || [];
  const vm = this;

  vm.minSets = params.minSets;
  vm.maxSets = params.maxSets;
  vm.checked = 0;
  vm.searchText = '';
  vm.sets = [];

  vm.checkChanged = checkChanged;
  vm.close = close;
  vm.select = select;
  vm.enoughSets = enoughSets;
  vm.getSelectedSets = getSelectedSets;

  //this is scrappy - TODO improve this
  const setNum = vm.minSets || vm.maxSets;
  //if there is a max, they need to take exactly that number of sets
  const text = vm.minSets ? ' minimum' : '';
  vm.deckText = `Select ${setNum} Set/s${text}`;

  $scope.$on('lists', (event, lists) => {
    vm.sets = lists.map((set) => {
      const winner = inSets.indexOf(set.name) !== -1;

      if (winner) {
        vm.checked += 1;
      }

      return {
        name: set.name,
        //select it already if its come in from the params
        //TODO change the name of this not to be winner
        winner,
      };
    });
  })

  listService.getLists(gameType, draftType);
    // .catch((message) => {
    //   vm.alert = {
    //     type: 'danger',
    //     msg: 'Draft Setup Unsuccessful',
    //     hover: message,
    //   };
    // });


  function checkChanged (item) {
    if (item.winner) {
      vm.checked += 1;
    } else {
      vm.checked -= 1;
    }
  }

  function select () {
    if (vm.enoughSets()) {
      const selected = getSelectedSets();

      $uibModalInstance.close(selected);
    }
  }

  //TODO this is really lazy - should calculate selected sets when
  //things are selected and rather than this - distracted ATM
  function getSelectedSets () {
    return vm.sets
        .filter((set) => set.winner)
        .map((set) => set.name);
  }

  //a bit cheeky - the UI should contrain if you try
  // to take more sets - this is lazy
  function enoughSets () {
    if (vm.minSets >= 0) {
      return vm.checked >= setNum;
    }

    if (vm.maxSets >= 0) {
      return vm.checked === setNum;
    }
  }

  function close () {
    $uibModalInstance.dismiss('closed');
  }
}
