'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('myDialogs', []);

require('./error/errorDialog.controller.js');
require('./set/setDialog.controller.js');
require('./settings/settingsDialog.controller.js');
require('./settings/staticSettingsDialog.controller.js');
require('./decklist/deckListDialog.controller.js');
require('./message/messageDialog.controller.js');
require('./join/joinDialog.controller.js');
require('./confirm/confirmDialog.controller.js');
require('./input/saveDialog.controller.js');
require('./input/loadDialog.controller.js');

//TODO restructure this better
//not a dialog, a shared component
require('./decklistExport/decklistExport.component.js');