'use strict';
/*eslint-env browser, es6, commonjs*/
const angular = require('angular');

angular.module('myDialogs')
  .controller('LoadDialogController', LoadDialogController);

LoadDialogController.$inject = ['$uibModalInstance', 'params', 'listService', '$scope'];

function LoadDialogController($uibModalInstance, params, listService, $scope) {
  const vm = this;

  vm.listName   = params.listName || '';
  vm.gameType = params.gameType;
  vm.draftType = params.draftType;

  vm.load = load;
  vm.cancel = cancel
  vm.selectList = selectList;

  $scope.$on('lists', (event, lists) => {
    vm.lists = lists;
  });

  $scope.$on('list', (event, list) => {
    $uibModalInstance.close(list);
  });

  activate();

  function activate () {
    listService.getLists(vm.gameType, vm.draftType);
  }

  function selectList (list) {
    vm.listName = list.name;
    vm.owner = list.owner;
  }

  function load () {
    listService.getCards(vm.gameType, vm.draftType, vm.listName, vm.owner);
  }

  function cancel () {
    $uibModalInstance.dismiss('cancel');
  }

}
