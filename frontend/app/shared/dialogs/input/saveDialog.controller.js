'use strict';
/*eslint-env browser, es6, commonjs*/
const angular = require('angular');

angular.module('myDialogs')
  .controller('SaveDialogController', SaveDialogController);

SaveDialogController.$inject = ['$uibModalInstance', 'params', 'listService', '$scope'];

function SaveDialogController($uibModalInstance, params, listService, $scope) {
  const vm = this;

  vm.listName   = params.listName || '';
  vm.list = params.list;
  vm.gameType = params.gameType;
  vm.draftType = params.draftType;

  vm.save = save;
  vm.cancel = cancel
  vm.selectList = selectList;

  $scope.$on('lists', (event, lists) => {
    vm.lists = lists;
  });

  $scope.$on('added list', (event, name) => {
    $uibModalInstance.close({name});
  });

  listService.getLists(vm.gameType, vm.draftType);

  function selectList (list) {
    vm.listName = list.name;
  }

  function save () {
    listService
      .addList(vm.gameType, vm.draftType, {
        name: vm.listName,
        list: vm.list,
      });
  }

  function cancel () {
    $uibModalInstance.dismiss('cancel');
  }

}
