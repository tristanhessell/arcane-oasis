'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('myDialogs')
  .controller('JoinDialogController', JoinDialogController);

JoinDialogController.$inject = ['$uibModalInstance', '$state', 'params'];

//TODO think about moving the state change to the calling controller
function JoinDialogController ($uibModalInstance, $state, params) {
  const vm = this;
  vm.config = params.config;

  vm.close = close;
  vm.join = join;
  vm.url = `https://arcane-oasis.herokuapp.com/#/${vm.config.draftType}/${vm.config.uid}`;

  function close () {
    $uibModalInstance.dismiss('close');
  }

  function join () {
    $state.go(vm.config.draftType, {
      draftid: vm.config.uid,
    }, {
      location: 'replace',
    });

    close();
  }

}
