'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals sinon, assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('JoinDlgController', () => {
  let createController;
  let modalInstance;
  let state;

  beforeEach(angular.mock.module('ui.router'));
  beforeEach(angular.mock.module('myDialogs'));
  beforeEach(inject(($injector) => {
    modalInstance = {
      dismiss: () => {},
    };
    state = $injector.get('$state');

    createController = (params) => {
      return $injector.get('$controller')('JoinDialogController', {
        '$uibModalInstance'  : modalInstance,
        '$state': state,
        params,
      });
    };
  }));

  it('should change location when join is called', () => {
    const params = {
      config: {
        draftType : 'memes',
        uid: 131313,
      },
    };
    const stateGoStub = sinon.stub(state, 'go');
    const vm = createController(params);

    vm.join();

    assert(stateGoStub.calledWith(params.config.draftType,
      {draftid: params.config.uid})
    );
  });

  it('should close the dialog when dismiss is called', () => {
    const dismissStub = sinon.stub(modalInstance, 'dismiss');
    const vm = createController({});

    vm.close();

    assert(dismissStub.calledOnce);
  });
});
