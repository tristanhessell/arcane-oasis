'use strict';
/*eslint-env browser, es6, commonjs*/
const angular = require('angular');

angular.module('myDialogs')
  .controller('DeckListDialogController', DeckListDialogController);

DeckListDialogController.$inject = ['$uibModalInstance', 'params'];

function DeckListDialogController($uibModalInstance, params) {
  const vm = this;

  vm.deckList = params.deckList;
  vm.deckText = params.message;

  if (vm.deckList.length === 0) {
    vm.deckText = 'You have no cards!';
  }

  vm.close = () => {
    $uibModalInstance.dismiss('closed');
  };

}