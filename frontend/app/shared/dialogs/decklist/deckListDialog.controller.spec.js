'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals sinon, assert, inject*/


const angular = require('angular');
require('angular-mocks');

describe('deckDialogController', () => {
    let createController;
    let modalInstance;

    beforeEach(angular.mock.module('myDialogs'));
    beforeEach(inject(($injector) => {
        modalInstance = {
            dismiss: () => {},
        };

        createController = (params) => {
            return $injector.get('$controller')('DeckListDialogController', {
                '$uibModalInstance'  : modalInstance,
                params,
            });
        };
    }));


    it('should show the decklist and not deckText', () => {
        const params =  {
            deckList: ['c1','c2'],
            message: "maymays",
        };

        const vm = createController(params);

        assert.deepEqual(vm.deckList, params.deckList);
        assert.equal(vm.deckText, params.message);
    });

    it('should show the deckText if the deck is empty', () => {
        const params =  {
            deckList: [],
            message: "maymays",
        };

        const vm = createController(params);

        assert.deepEqual(vm.deckList, params.deckList);
        assert.equal(vm.deckText, 'You have no cards!');
    });

    it('should close the dialog when close is called', () => {
        const params = {
            deckList: [],
            message: "maymays",
        };
        const dismissStub = sinon.stub(modalInstance, 'dismiss');

        const vm = createController(params);

        vm.close();
        assert(dismissStub.calledOnce);
    });
});
