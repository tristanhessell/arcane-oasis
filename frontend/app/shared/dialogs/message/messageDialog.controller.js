'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('myDialogs')
  .controller('MessageDialogController', MessageDlgController);

MessageDlgController.$inject = ['params', '$uibModalInstance'];

function MessageDlgController (params, $uibModalInstance) {
  const vm = this;

  vm.title  = params.title;
  vm.message  = params.message;

  vm.endDraft = () => {
    $uibModalInstance.close('endDraft');
  };
}
