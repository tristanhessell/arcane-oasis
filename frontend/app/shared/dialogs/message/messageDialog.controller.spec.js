'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals sinon, assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('MessageDialogController', () => {
    let createController;
    let modalInstance;

    beforeEach(angular.mock.module('myDialogs'));

    beforeEach(inject(($injector) => {
        modalInstance = {
            close: () => {},
        };

        createController = (params) => $injector.get('$controller')('MessageDialogController', {
            '$uibModalInstance'  : modalInstance,
            params,
        });
    }));


    it('should show the title and message', () => {
        const params = {
            title: 'title',
            message: 'message',
        };
        const vm = createController(params);

        assert.equal(vm.title, params.title);
        assert.equal(vm.message, params.message);
    });

    it('should close the dialog', () => {
        const closeStub = sinon.stub(modalInstance, 'close');

        const vm = createController({});

        vm.endDraft();

        assert(closeStub.calledOnce);
    });
});
