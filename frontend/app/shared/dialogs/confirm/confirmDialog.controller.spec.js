'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals sinon, assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('ConfirmDialogController', () => {
    let createController;
    let modalInstance;

    beforeEach(angular.mock.module('myDialogs'));

    beforeEach(inject(($injector) => {
        modalInstance = {
            close: () => {},
            dismiss: () => {},
        };

        createController = (params) => $injector.get('$controller')('ConfirmDialogController', {
            '$uibModalInstance'  : modalInstance,
            params,
        });
    }));


    it('should show the title and message', () => {
        const params = {
            title: 'title',
            message: 'message',
        };

        const vm = createController(params);

        assert.equal(vm.title, params.title);
        assert.equal(vm.message, params.message);
    });

    it('should close the dialog if yes is selected', () => {
        const closeStub = sinon.stub(modalInstance, 'close');
        const vm = createController({});

        vm.yes();

        assert(closeStub.calledOnce);
    });

    it('should close the dialog if no is selected', () => {
        const dismissStub = sinon.stub(modalInstance, 'dismiss');
        const vm = createController({});

        vm.no();

        assert(dismissStub.calledOnce);
    });

});
