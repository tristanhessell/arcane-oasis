'use strict';
/*eslint-env browser, es6, commonjs*/
const angular = require('angular');

angular.module('myDialogs')
  .controller('ConfirmDialogController', ConfirmDialogController);

ConfirmDialogController.$inject = ['$uibModalInstance', 'params'];

function ConfirmDialogController($uibModalInstance, params) {
  const vm = this;

  vm.title   = params.title;
  vm.message = params.message;

  vm.yes = () => {
    $uibModalInstance.close('confirmed');
  };

  vm.no = () => {
    $uibModalInstance.dismiss('cancel');
  };
}