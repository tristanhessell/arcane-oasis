'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('myDialogs')
  .controller('ErrorDialogController', ErrorDialogController);

ErrorDialogController.$inject = ['$uibModalInstance', 'params'];

function ErrorDialogController ($uibModalInstance, params) {
  const vm = this;

  vm.title  = params.title;
  vm.message  = params.message;
  vm.deckList = params.deckList;

  vm.endDraft = endDraft;

  if (vm.deckList.length === 0) {
    vm.deckText = 'You have no cards!';
  }

  function endDraft () {
    $uibModalInstance.close('endDraft');
  }
}
