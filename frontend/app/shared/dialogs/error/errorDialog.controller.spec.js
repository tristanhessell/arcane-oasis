'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals sinon, assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('errorDialogController', () => {
    let createController;
    let modalInstance;

    beforeEach(angular.mock.module('myDialogs'));

    beforeEach(inject(($injector) => {
        modalInstance = {
            close: () => {},
        };
        createController = (params) => {
            return $injector.get('$controller')('ErrorDialogController', {
                '$uibModalInstance': modalInstance,
                params,
            });
        };
    }));


    it('should show the decklist and not deckText', () => {
        const params = {
            title : 'The Title',
            deckList: ['c1','c2'],
            message: 'A message',
        };

        const vm = createController(params);

        assert.deepEqual(vm.title, params.title);
        assert.deepEqual(vm.deckList, params.deckList);
        assert.deepEqual(vm.message, params.message);
    });

    it('should show the deckText if the deck is empty', () => {
        const params = {
            title : 'The Title',
            deckList: [],
            message: 'A message',
        };

        const vm = createController(params);

        assert.deepEqual(vm.title, params.title);
        assert.deepEqual(vm.deckList, params.deckList);
        assert.deepEqual(vm.message, params.message);
        assert.deepEqual(vm.deckText, 'You have no cards!');
    });

    it('should close the dialog', () => {
        const params = {
            title : 'The Title',
            deckList: [],
            message: 'A message',
        };
        const closeStub = sinon.stub(modalInstance, 'close');

        const vm = createController(params);

        vm.endDraft();

        assert(closeStub.calledOnce);
    });
});
