'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals sinon, assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('settingsDialogController', () => {
    let modalInstance;
    let $httpBackend;
    let configService;
    let draftService;
    let dialogService;
    let $q;

    let createController;

    const getMockModalInstance = () => {
        return {
            close: () => {},
            dismiss: () => {},
        };
    }

    beforeEach(angular.mock.module('arcaneOasis', ($urlRouterProvider) => {
        $urlRouterProvider.deferIntercept();
    }));

    beforeEach(angular.mock.module('ui.router'));
    beforeEach(angular.mock.module('myDialogs'));

    beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');
        configService = $injector.get('configService');
        draftService = $injector.get('draftService');
        dialogService = $injector.get('dialogService');
        modalInstance = getMockModalInstance();

        $q = $injector.get('$q');

        const config = {
            draftTypes: ['cube','booster'],
            draftFormats: ['rochester', 'booster'],
            gameTypes: ['ygo'],
        };

        const deferred = $q.defer();
        deferred.resolve(config);

        sinon.stub(configService, 'getConfig')
            .returns(deferred.promise);

        createController = () =>
            $injector.get('$controller')('SettingsDialogController', {
                '$uibModalInstance': modalInstance,
                dialogService,
                configService,
                draftService,
            });
    }));

    afterEach(() => {
        configService.getConfig.restore();
    });


    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    //change this to 5 tests
    it('should set the default values of each draft combination correctly', () => {
        const vm = createController();
        const config = vm.config;

        config.gameType = 'ygo';
        config.draftType = 'booster';
        config.draftFormat = 'booster';
        config.numPlayers = 8;  //could be anything though
        vm.setDefaults();

        assert.equal(9, config.packSize);
        assert.equal(6, config.numRounds);

        config.draftType = 'booster';
        config.draftFormat = 'rochester';
        config.numPlayers = 4; //could be anything though
        vm.setDefaults();

        assert.equal(9, config.packSize);
        assert.equal(28, config.numRounds);

        config.draftType = 'cube';
        config.draftFormat = 'booster';
        config.numPlayers = 8;  //could be anything though
        vm.setDefaults();

        assert.equal(14, config.packSize);
        assert.equal(4, config.numRounds);

        config.draftType = 'cube';
        config.draftFormat = 'rochester';
        config.numPlayers = 8; //with even number of players
        vm.setDefaults();

        assert.equal(16, config.packSize);
        assert.equal(28, config.numRounds);

        config.draftType = 'cube';
        config.draftFormat = 'rochester';
        config.numPlayers = 5; //with odd number of players
        vm.setDefaults();

        assert.equal(12, config.packSize);
        assert.equal(28, config.numRounds);
    });

    //TODO change this to two seperate tests
    it('should change the values when the draft type is changed', () => {
        const vm = createController();
        const config = vm.config;

        //when booster type is selected, packsize is enforced at 9
        config.gameType = 'ygo';
        config.draftType = 'booster';
        config.draftFormat = 'booster';
        config.sets = ['a', 'b', 'c'];
        vm.draftTypeChanged();

        assert.equal(config.packSize, 9);
        assert.equal(config.sets.length, 0);
    });


    it('should show the deck size', () =>{
        const vm = createController();
        const config = vm.config;

        config.draftFormat = 'rochester';
        config.numRounds = 5;

        vm.settingsChanged();

        assert.equal(vm.deckSize, 10);
    });

    it('should show \'-\' when the deck size is NaN', () => {
        const vm = createController();
        const config = vm.config;

        config.draftFormat = 'asdas';
        vm.settingsChanged();

        assert.equal(vm.deckSize, '-');
    });

    //TODO move this to seperatre tests
    it('should enforce the minimum pack size for rochester drafts when the format is changed', () => {
        const vm = createController();
        const config = vm.config;
        const DEFAULT_PACK_SIZE = 2;

        config.gameType = 'ygo';
        config.draftFormat = 'rochester';
        config.numPlayers = 3;
        vm.settingsChanged();

        assert.equal(vm.minPackSize, 6);

        config.packSize = 0;
        config.numPlayers = 4;
        vm.settingsChanged();
        assert.equal(vm.minPackSize, 8);

        config.draftFormat = 'booster';
        vm.settingsChanged();
        assert.equal(vm.minPackSize, DEFAULT_PACK_SIZE);
    });

    //TODO move this to seperatre tests
    it('should enforce the minimum pack size for rochester drafts when the num players is changed', () => {
        const vm = createController();
        const config = vm.config;
        const DEFAULT_PACK_SIZE = 2;

        config.gameType = 'ygo';
        config.draftFormat = 'rochester';
        config.numPlayers = 5;
        vm.settingsChanged();

        assert.equal(vm.minPackSize, 10);

        config.packSize = 0;
        config.numPlayers = 4;
        vm.settingsChanged();
        assert.equal(vm.minPackSize, 8);

        config.draftFormat = 'booster';
        vm.settingsChanged();
        assert.equal(vm.minPackSize, DEFAULT_PACK_SIZE);
    });

    //TODO break this into 4 tests
    it('should check that the settings are valid', () => {
        const vm = createController();
        const config = vm.config;

        config.gameType = 'ygo';
        config.sets = [];

        assert.equal(vm.validSettings(), false);

        config.sets = ['a', 'b'];
        config.draftType = 'booster';
        assert.equal(vm.validSettings(), false);

        config.sets = [1];
        config.draftFormat = 'rochester';
        config.packSize = 9;
        config.numPlayers = 13;
        assert.equal(vm.validSettings(), false);

        config.numPlayers = 2;
        assert.equal(vm.validSettings(), true);
    });

});
