'use strict';
/*eslint-env browser, es6, commonjs*/

const draftMap = {
    booster: (settings) => {
        const {packSize, numRounds} = settings;

        return packSize * numRounds;
    },
    rochester: (settings) =>  {
        const {numRounds} = settings;

        return numRounds * 2;
    },
};

module.exports = exports = (settings) => {
    const sizeFunction = draftMap[settings.draftFormat];
    const size = sizeFunction && sizeFunction(settings);

    if (isNaN(size)) {
        return NaN;
    }

    return size;
};

//TODO find the correct location for this file
