'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('myDialogs')
    .controller('StaticSettingsDialogController', StaticSettingsDialogController);

StaticSettingsDialogController.$inject = ['$uibModalInstance', 'params'];

function StaticSettingsDialogController ($uibModalInstance, params) {
    const vm = this;

    vm.config = params.config;
    vm.uid = params.uid;

    vm.close = () => {
        $uibModalInstance.dismiss('closed');
    };
}