'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals assert*/

const sizeCalculator = require('./deckSizeCalculator.js');

describe('deckSizeCalculator', () => {
    it('should return the size for Booster drafts', () => {
        const settings = {
            draftFormat: 'booster',
            packSize: 9,
            numRounds: 6,
        };

        assert.equal(54, sizeCalculator(settings));
    });

    it('should return the size for Rochester drafts', () => {
        const settings = {
            draftFormat: 'rochester',
            packSize: 9,
            numRounds: 28,
        };

        assert.equal(56, sizeCalculator(settings));
    });

    it('should return NaN if any of the required values are not Numbers', () => {
        const settings = {
            draftFormat: 'rochester',
            packSize: 'asds',
            numRounds: 'asds',
        };

        assert.isNaN(sizeCalculator(settings));
    });

    it('should return NaN if there isn\'t a function for the draftFormat', () => {
        const settings = {
            draftFormat: 'asdsadsa',
            packSize: 9,
            numRounds: 6,
        };

        assert.isNaN(sizeCalculator(settings));
    });
});
