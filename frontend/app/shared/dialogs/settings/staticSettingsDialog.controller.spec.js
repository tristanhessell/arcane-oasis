'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals sinon, assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('staticSettingsController', () => {
    let createController;
    let modalInstance;

    beforeEach(angular.mock.module('myDialogs'));

    beforeEach(inject(($injector) => {
        modalInstance = {
            dismiss: () => {},
        };

        createController = (params) =>
            $injector.get('$controller')('StaticSettingsDialogController', {
                '$uibModalInstance': modalInstance,
                params,
            });
    }));

    it('should set the values from params', () => {
        const config =  {
            message: 'maymays',
            uid: '13-13-13',
        };
        const vm = createController(config);

        assert.equal(vm.config, config.config);
        assert.equal(vm.uid, config.uid);
    });

    it('should close the dialog when close is called', () => {
        const dismissStub = sinon.stub(modalInstance, 'dismiss');
        const vm = createController({});

        vm.close();

        assert(dismissStub.calledOnce);
    });
});
