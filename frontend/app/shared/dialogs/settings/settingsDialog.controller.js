'use strict';
/*eslint-env browser, es6, commonjs*/

//TODO get this into the correct areas
const deckSizeCalculator = require('./deckSizeCalculator.js');
const angular = require('angular');

angular.module('myDialogs')
  .controller('SettingsDialogController', SettingsDialogController);

SettingsDialogController.$inject = ['$uibModalInstance', 'dialogService', 'configData', 'draftService', '$scope'];

function SettingsDialogController ($uibModalInstance, dialogService, configData, draftService, $scope) {
  const DEFAULT_MIN_PACK_SIZE = 2;
  const vm = this;

  vm.draftStarted = false;
  vm.alert = undefined;
  vm.minPackSize = DEFAULT_MIN_PACK_SIZE;
  vm.config = {
    numPlayers : 2,
    sets : [], //the selected sets
    packSize: 9,
    numRounds: 6,
  };

  vm.draftTypeChanged   = draftTypeChanged;
  vm.draftFormatChanged = draftFormatChanged;
  vm.settingsChanged    = settingsChanged;
  vm.startDraft         = startDraft;
  vm.setDefaults        = setDefaults;
  vm.openSetPicker      = openSetPicker;
  vm.validSettings      = validSettings;
  vm.close              = close;

  vm.gameTypes    = configData.gameTypes;
  vm.draftTypes   = configData.draftTypes;
  vm.draftFormats = configData.draftFormats;

  vm.config.draftFormat = vm.draftFormats[0];
  vm.config.gameType    = vm.gameTypes[0];
  vm.config.draftType   = vm.draftTypes[0];

  $scope.$on('new draft', (event, uid) => {
    vm.config.uid = uid;
    $uibModalInstance.close(vm.config);
  });

  calculateDeckSize();

  //TODO look into ang filter(?) to show something if the value is NaN
  //calculate the decksize from the relevant settings
  function calculateDeckSize () {
    vm.deckSize = deckSizeCalculator(vm.config);

    if (isNaN(vm.deckSize)) {
      vm.deckSize = '-';
    }
  }

  //when eithe the draftType of the draft Format is changed,
  //check if the various values need to change
  function draftTypeChanged () {
    vm.config.sets = [];
    if (vm.config.draftType === 'booster') {
      vm.config.packSize = 9;
    }
    vm.setDefaults();
    calculateDeckSize();
  }

  //TODO improve this
  function draftFormatChanged () {
    if (vm.config.draftFormat === 'rochester') {
      //change the min field of the input element
      vm.minPackSize = vm.config.numPlayers*2;
      //if the value is less than min, change it to the min
      if (vm.config.packSize < vm.minPackSize) {
        vm.config.packSize = vm.minPackSize;
      }
    }
    else {
      vm.minPackSize = DEFAULT_MIN_PACK_SIZE;
    }
    vm.setDefaults();
    calculateDeckSize();
  }

  //TODO improve this
  function settingsChanged () {
    if (vm.config.draftFormat === 'rochester') {
      //change the min field of the input element
      vm.minPackSize = vm.config.numPlayers*2;
      //if the value is less than min, change it to the min
      if (vm.config.packSize < vm.minPackSize) {
        vm.config.packSize = vm.minPackSize;
      }
    }
    else {
      vm.minPackSize = DEFAULT_MIN_PACK_SIZE;
    }
    calculateDeckSize();
  }


  //send the request to the server
  function startDraft () {
    //TODO remove this alert stuff to just use toastr
    draftService.startDraft(vm.config);
  }

  function setDefaults () {
    const {
      gameType,
      draftType,
      draftFormat,
      numPlayers,
    } = vm.config;

    const defaults = {
      ygo: {
        booster: {
          booster: {
            packSize: 9,
            numRounds: 6,
          },
          rochester: {
            packSize: 9,
            numRounds: 28,
          },
        },
        cube: {
          booster: {
            packSize: 14,
            numRounds: 4,
          },
          rochester: {
            packSize: (numPlayers % 2 === 0 ? 2*numPlayers : 2*numPlayers+2),
            numRounds: 28,
          },
        },
      },
    };

    vm.config.packSize = defaults[gameType][draftType][draftFormat].packSize;
    vm.config.numRounds = defaults[gameType][draftType][draftFormat].numRounds;
    calculateDeckSize();
  }


  //for now, in a booster (type) draft, you can only have one set
  function openSetPicker () {
    let maxSets;
    let minSets;

    if (vm.config.draftType === 'booster') {
      maxSets = 1;
    }
    else if (vm.config.draftType === 'cube') {
      minSets = 1;
    }

    dialogService.openSets({
      minSets,
      maxSets,
      draftType: vm.config.draftType,
      gameType: vm.config.gameType,
      sets: vm.config.sets,
    }).result.then((selectedSets) => {
      vm.config.sets = selectedSets;
    });
  }

  function validSettings () {
    if (vm.config.sets.length === 0) {
      return false;
    }

    //booster drafts currently only support one draft type
    if (vm.config.draftType === "booster" && vm.config.sets.length !== 1) {
      return false;
    }

    //if its a rochester draft, and the pack size is less 2*number of players
    if (vm.config.draftFormat === 'rochester' && vm.config.numPlayers > Math.round(vm.config.packSize/2)) {
      return false;
    }

    return true;
  }

  function close () {
    $uibModalInstance.dismiss('closed');
  }

}
