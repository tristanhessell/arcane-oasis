'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('myDialogs')
  .component('deckListExport', {
    controller: deckListExportController,
    templateUrl: '/app/shared/dialogs/deckListExport/deckListExport.html',
    bindings: {
      deckList: '<',
    },
  });

deckListExportController.$inject = ['listService', '$scope'];

function deckListExportController (listService, $scope) {
  const ctrl = this;

  ctrl.toYvd = toYvd;
  ctrl.toNormal = toNormal;

  ctrl.yvdEnabled = true;
  let cardSets;

  toNormal();

  $scope.$on('set names', (event, sets) => {
    cardSets = sets;
    ctrl.deck = ctrl.deckList
      .map((card, index) => `${card.quantity}|${sets[index] || ''}|${card.name}`);
    ctrl.yvdEnabled = true;
  });

  function toYvd () {
    if (!cardSets) {
      ctrl.yvdEnabled = false;
      //bring up confirmation modal to ask if this is okay
      listService.getSets(ctrl.deckList);
    } else {
      ctrl.deck = ctrl.deckList
        .map((card, index) => `${card.quantity}|${cardSets[index] || ''}|${card.name}`);
    }
  }

  function toNormal () {
    ctrl.deck = ctrl.deckList
      .map((card) => `${card.quantity} ${card.name}`);
  }
}
