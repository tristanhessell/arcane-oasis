'use strict';
/*eslint-env browser, es6, commonjs*/

//TODO Named it socketFact to avoid a collision with socket.io-clinet
//- need to investigate if socketFactory is used

const sockClient = require('socket.io-client');
const angular = require('angular');

angular.module('arcaneOasis')
  .factory('socket', SocketFactory);

SocketFactory.$inject = ['socketFactory', 'toastr', 'userService'];
function SocketFactory (socketFactory, toastr, userService) {
  const listeners = [];
  const events = [];

  let socket;

  function connect () {
    socket = socketFactory({
      ioSocket: sockClient.connect({
        query: `token=${userService.getUser().token}`,
      }),
    });

    listeners.forEach((listener) => {
      socket.on(listener.name, listener.handler);
    });

    events.forEach((event) => {
      socket.emit(event.name, event.args);
    });

    events.length = 0;

    socket.forward('broadcast');
    socket.on('error', (err) => {
      console.log('socket error');
      console.log(err);
      toastr.error(err.message || 'Something went wrong'); //TODO make this print properly
    });

    socket.on('socket error', (err) => {
      console.log('socket error');
      console.log(err);
      toastr.error(err.message || 'Something went wrong'); //TODO make this print properly
    });
  }

  function disconnect () {
    socket = undefined;
    socket.disconnect() //TODO not sure if this works
  }

  function on (name, handler) {
    if (socket) {
      socket.on(name, handler);
    }

    listeners.push({
      name,
      handler,
    });
  }

  function emit (name, ...args) {
    if (!socket) {
      events.push({
        name,
        args,
      });
    } else {
      socket.emit(name, ...args);
    }
  }

  return {
    connect,
    disconnect, //to use when log out
    on,
    emit,
  };
}
