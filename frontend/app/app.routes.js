'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('arcaneOasis')
  .filter('encodeURL', encodeURL)
  .run(run)
  .config(configure);

encodeURL.$inject = ['$window']
function encodeURL ($window) {
  return $window.encodeURIComponent;
}

run.$inject = ['$rootScope', '$state', 'authService', 'socket'];

function run ($rootScope, $state, authService, socket) {
  authService.isAuthenticated().then(() => {
    socket.connect();
  });

  $rootScope.$on('$stateChangeStart', (event, toState, toParams/*, fromState, fromParams*/) => {
    if (toState.authenticate) {
      //if the user isnt authenticated
      //change this to not use catch - use then and just have false
      authService.isAuthenticated().catch((/*ff*/) => {
        $state.returnToState = toState.name;
        $state.returnToStateParams = toParams;
        // User isn’t authenticated
        $state.go('login', {}, {
          location: 'replace',
        });
        event.preventDefault();
      });
    }
  });

  // $rootScope.$on('unauthorized', function() {
  //   console.log('unauthorized api access')
  //   $state.go('login', {}, {
  //     location: 'replace',
  //   });
  // });
}

configure.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', 'toastrConfig'];

function configure ($stateProvider, $urlRouterProvider, $httpProvider, toastrConfig) {
  angular.extend(toastrConfig, {
    positionClass: 'toast-top-center',
  });

  $urlRouterProvider.otherwise('/home');
  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'app/components/landing/landing.html',
    })
    .state('login', {
      url: '/login',
      controller: 'LoginController as vm',
      templateUrl: 'app/components/login/login.html',
    })
    .state('signup', {
      url: '/signup',
      controller: 'SignupController as vm',
      templateUrl: 'app/components/signup/signup.html',
    })
    .state('lobby', {
      url: '/lobby',
      authenticate: true,
      controller: 'LobbyController as vm',
      templateUrl: 'app/components/lobby/lobby.html',
    })
    .state('sets', {
      url: '/sets',
      authenticate: true,
      controller: 'SetsController as vm',
      templateUrl: 'app/components/sets/sets.html',
    })
    .state('addSet', {
      url: '/sets/add',
      authenticate: true,
      controller: 'AddSetController as vm',
      templateUrl: 'app/components/sets/addSets.html',
    })
    //TODO draft states should have one url?
    .state('rochester', {
      url: '/rochester/:draftid',
      authenticate: true,
      'controller': 'RochesterController as vm',
      templateUrl: 'app/components/rochester/rochester.html',
    })
    .state('booster', {
      url: '/booster/:draftid',
      authenticate: true,
      controller: 'BoosterController as vm',
      templateUrl: 'app/components/booster/booster.html',
    });

  $httpProvider.interceptors.push('apiInterceptor');
}
