'use strict';
/*eslint-env node, es6*/

//TODO change this to use css classes or some shit

//TODO fix up this file
const COLOUR_WHITE = '#FFFFFF';
const COLOUR_BLACK = '#000000';


const getMonsterTextColour = (card) => {
  switch (card.monsterType[0]) {
    case "Effect"://black text
      return COLOUR_BLACK;
    case "Pendulum"://black text
      return COLOUR_BLACK;
    case "Ritual"://black text
      return COLOUR_BLACK;
    case "Normal": //black text
      return COLOUR_BLACK;
    case "Synchro": //black text
      return COLOUR_BLACK;
    case "Xyz": //white text
      return COLOUR_WHITE;
    case "Fusion": //white text
      return COLOUR_WHITE;
    default:
      console.log(`Invalid monster type '${card.monsterType}' in card '${card.name}'`);
  }

  return COLOUR_BLACK;
};

//if there is an invalid colour type, return undefined?
const getMonsterBackgroundColour = (card) => {
  return card.monsterType.map((type) => {
    switch (type) {
      case "Effect"://orange
        return "#E08E22";
      case "Pendulum"://spell green
        return "#1E8725";
      case "Ritual"://blue
        return "#6078D6";
      case "Normal": //yellowish
        return "#F7E257";
      case "Synchro": //white
        return COLOUR_WHITE;
      case "Xyz": //black
        return COLOUR_BLACK;
      case "Fusion": //purple
        return "#912CEE";
      default:
        console.log(`Invalid monster type '${type}' in card '${card.name}'`);
        return COLOUR_WHITE
    }
  });
};


const getTextColour = (card) => {

  switch (card.cardType) {
    case "Monster":
      return getMonsterTextColour(card);
    case "Spell": //green, white text
      return COLOUR_WHITE;
    case "Trap": //pink, white text
      return COLOUR_WHITE;
    default:
      console.log(`Invalid card type '${card.cardType}' in card '${card.name}'`);
  }

  return COLOUR_BLACK; //black
};

const getBackgroundColours = (card) => {
  switch (card.cardType) {
    case "Monster":
      return getMonsterBackgroundColour(card);
    case "Spell": //green, white text
      return ["#1e8725"];
    case "Trap": //pink, white text
      return ["#E0227B"];
    default:
      console.log(`Invalid card type '${card.cardType}' in card '${card.name}'`);
  }

  return [COLOUR_WHITE]; //default is white background
};

module.exports = {
  getTextColour,
  getBackgroundColours,
};
