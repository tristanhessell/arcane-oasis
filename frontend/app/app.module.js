'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');
require('angular-ui-router');
require('angular-ui-bootstrap');
require('angular-socket-io');
require('angular-animate');
require('socket.io-client');
require('angular-storage');
require('angular-toastr');
require('angular-messages');

require('./shared/dialogs/dialogs.js');

angular.module('arcaneOasis', ['btford.socket-io','ngAnimate',
  'ui.bootstrap', 'myDialogs', 'ui.router', 'angular-storage',
  'toastr', 'ngMessages']);

require('./shared/socket/socket.js')

require('./shared/services/api.interceptor.js');

require('./shared/services/dialog.service.js');
require('./shared/services/user.service.js');
require('./shared/services/auth.service.js');
require('./shared/services/list.service.js');
require('./shared/services/draft.service.js');
require('./shared/services/signup.service.js');
require('./shared/services/baseDraft.service.js');
require('./shared/services/rochesterDraft.service.js');

require('./app.routes.js');
require('./app.constants.js');

require('./components/lobby/lobby.controller.js');
require('./components/signup/signup.controller.js');
require('./components/login/login.controller.js');
require('./components/landing/landing.controller.js');
require('./components/booster/booster.controller.js');
require('./components/rochester/rochester.controller.js');
require('./components/sets/sets.controller.js');
require('./components/sets/addSets.controller.js');
require('./components/baseDraft.controller.js');
