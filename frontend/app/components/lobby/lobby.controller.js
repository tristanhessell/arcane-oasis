'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('arcaneOasis')
  .controller('LobbyController', LobbyController);

LobbyController.$inject = ['dialogService', 'draftService', 'userService', '$state', 'toastr', '$scope'];

function LobbyController (dialogService, draftService, userService, $state, toastr, $scope) {
  const vm = this;

  vm.activeDrafts = [];
  vm.gamesRequested = false;

  vm.openSettings = openSettings;
  vm.openJoinDraft = openJoinDraft;
  vm.getActiveDrafts = getActiveDrafts
  vm.logOut = logOut;
  vm.username = userService.getUser().username;

  getActiveDrafts();

  $scope.$on('drafts', (event, drafts) => {
    vm.activeDrafts = drafts;
    vm.draftsRequested = false;
  });

  function openSettings () {
    dialogService
      .openSettings()
      .result.then((config) => {
        vm.getActiveDrafts();
        vm.openJoinDraft(config);
      });
  }

  function openJoinDraft (config) {
    dialogService.openJoin({config});
  }

  //TODO how do we dael with errors with sockets
  function getActiveDrafts () {
    vm.draftsRequested = true;
    draftService.getDrafts();
      // .catch((message) => {
      //   console.log(message);
      //   vm.draftsRequested = false;
      // });
  }

  function logOut () {
    userService.removeUser();
    toastr.success('You have been logged out');
    $state.go('home');
  }
}
