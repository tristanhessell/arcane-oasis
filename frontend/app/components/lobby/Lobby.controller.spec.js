'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals sinon, assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('lobbyController', () => {
    let createController;
    let $httpBackend;
    let draftService;
    let dialogService;
    let $q;
    let scope;

    beforeEach(angular.mock.module('arcaneOasis', ($urlRouterProvider) => {
        $urlRouterProvider.deferIntercept();
    }));

    beforeEach(inject(($injector) => {
        scope   = $injector.get('$rootScope');
        $httpBackend = $injector.get('$httpBackend');
        draftService = $injector.get('draftService');
        dialogService = $injector.get('dialogService');

        $q = $injector.get('$q');

        const drafts = ['', ''];

        const deferred = $q.defer();
        deferred.resolve(drafts);

        sinon.stub(draftService, 'getDrafts')
            .returns(deferred.promise);

        createController = () => {
            const vm = $injector.get('$controller')('LobbyController', {
                dialogService,
                draftService,
            });
            scope.$apply();

            return vm;
        };
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should get the active games on getActiveDrafts()', () => {
        const vm = createController();

        vm.getActiveDrafts();
        assert.equal(vm.draftsRequested, true);
        scope.$apply();

        assert.equal(vm.draftsRequested, false);
        assert.deepEqual(vm.activeDrafts, ['', '']);
    });

    //testing that the dialog is opened and with the correct settings
    //so: animation: true, backdrop: static
    it('should open the settings dialog', () => {
        const settingsStub = sinon.stub(dialogService, 'openSettings')
            .returns({
                result: Promise.resolve(),
            });
        const vm = createController();

        vm.openSettings();
        assert(settingsStub.calledOnce);
    });


    //testing that the join dialog is opened and with the correct settings
    //so: animation: true, backdrop: static
    it('should open the join dialog', () => {
        const joinStub = sinon.stub(dialogService, 'openJoin')
            .returns({
                result: Promise.resolve(),
            });
        const vm = createController();

        vm.openJoinDraft();
        assert(joinStub.calledOnce);
    });

});
