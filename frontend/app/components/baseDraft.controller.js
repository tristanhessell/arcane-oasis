'use strict';
/*eslint-env browser, es6, commonjs*/

const angular   = require('angular');
const sorterMap = require('../../../server/sorters.js');
const urlMap  = require('../imageResource.js');

const cardColours = require('../YGOCardDecorator.js');

angular.module('arcaneOasis')
  .controller('BaseDraftController', BaseDraftController);

  //TODO reduce the number of listeners later

BaseDraftController.$inject = ['vm', 'baseDraft', 'dialogService', '$stateParams', 'draftService', '$state', 'userService', '$scope'];

function BaseDraftController (vm, baseDraft, dialogService, $stateParams, draftService, $state, userService, $scope) {
  vm.uid       = $stateParams.draftid;
  vm.selectedCards = [];
  vm.players     = [];
  vm.userName    = '';
  vm.config = {};

  vm.sortCardFunction = sorterMap.YGO;
  vm.getColourStr   = getColourStr;
  vm.getTextColour = getTextColour;
  vm.cardClick    = cardClick;
  vm.onCardHover    = onCardHover;

  vm.getMonsterCount = getMonsterCount;
  vm.getSpellCount = getSpellCount;
  vm.getTrapCount = getTrapCount;

  vm.confirmToLobby   = backToLobby;
  vm.openDeckList   = openDeckList;
  vm.openErrorDialog  = openErrorDialog;
  vm.openSettings   = openSettings;

  const gameStartedDlg = openGenericDialog({
    title: 'Waiting for Players',
    message: 'Game will start when enough players have joined the drafting session',
  });

  $scope.$on('new pack', (event, pack) => {
    vm.pack = pack;
  });

  $scope.$on('selected cards', (event, inSelected) => {
    vm.selectedCards = inSelected;
  });

  $scope.$on('all players', (event, players) => {
    vm.players = players;
  });

  $scope.$on('draft started', () => {
    gameStartedDlg.dismiss();
  });

  $scope.$on('draft settings', (event, settings) => {
    vm.config = settings;
    vm.cardURL = urlMap(vm.config.gameType);
  });

  $scope.$on('draft error', (event, err) => {
    openErrorDialog({
      title: err.title,
      message: err.message,
    });
  });

  vm.username = userService.getUser().username;
  draftService.getDraftSettings(vm.uid);
  baseDraft.joinDraft({
    uid: vm.uid,
    username: vm.username,
  });

  function getMonsterCount () {
    return vm.selectedCards.filter((card) => card.cardType === 'Monster').length;
  }

  function getSpellCount () {
    return vm.selectedCards.filter((card) => card.cardType === 'Spell').length;
  }

  function getTrapCount () {
    return vm.selectedCards.filter((card) => card.cardType === 'Trap').length;
  }

  function toLobby () {
    $state.go('lobby', {}, {
      location: 'replace',
    });
  }

  function onCardHover (card) {
    vm.hoveredURL = `${vm.cardURL}/${encodeURIComponent(card.name)}`;
  }

  //linear gradient doesnt work with only one colour
  function getColourStr (card) {
    const colourArr = cardColours.getBackgroundColours(card);

    if (colourArr.length === 1) {
      return `${colourArr[0]}`;
    }

    return `linear-gradient(90deg, ${colourArr.join()})`;
  }

  function getTextColour (card) {
    return cardColours.getTextColour(card);
  }

  function cardClick (card) {
    const listCard = vm.selectedCards.find((selCard) => {
      return selCard.name === card.name;
    });

    if (!listCard) {
      card.quantity = 1;
      vm.selectedCards.push(card);
    } else {
      listCard.quantity += 1;
    }

    baseDraft.selectCard(card.name);
  }

  function openGenericDialog (params) {
    const modal = dialogService.openGeneric(params);

    modal.result.then(() => {
      toLobby();
    });

    return modal;
  }

  function backToLobby () {
    dialogService
      .openConfirmation({
        title: 'Are you sure you want to leave?',
        message: 'If you leave now, the drafting session will end for all participants',
      })
      .result
      .then(() => {
        toLobby();
      });
  }

  function openDeckList (message) {
    dialogService.openDecklist({
      message,
      deckList: vm.selectedCards,
    });
  }

  function openErrorDialog (params) {
    params.deckList = vm.selectedCards;
    dialogService.openError(params).result.then(() => {
      toLobby();
    });
  }

  function openSettings () {
    dialogService.openStaticSettings({
      config: vm.config,
      uid: vm.uid,
    });
  }

}
