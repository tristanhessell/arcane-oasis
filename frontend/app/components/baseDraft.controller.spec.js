'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals assert, inject*/

const angular = require('angular');
require('angular-mocks');

//TODO figure out best way to test this
xdescribe('BaseDraftController', () => {
    let $httpBackend;
    let socket;

    const createSocket = () =>  {
        return {
            on: () => {},
            emit: () => {},
        };
    };

    beforeEach(angular.mock.module('arcaneOasis', ($urlRouterProvider) => {
        $urlRouterProvider.deferIntercept();
    }));

    beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');

        socket = createSocket();
        socket.on();
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    it('should open the settings dialog', () => {
        assert.fail();
    });

    it('should open the error dialog', () => {
        assert.fail();
    });

    it('should open decklist dialog', () => {
        assert.fail();
    });

//socket
    it('should open open the dialog if the draft is full (and you joining late)', () => {
        assert.fail();
    });

    it('should open the error dialog when the draft is over', () => {
        assert.fail();
    });

    it('should set the players name', () => {
        assert.fail();
    });

    it('should set all the players names', () => {
        assert.fail();
    });

    it('should add player to list', () => {
        assert.fail();
    });

    it('should dismiss the dialog when the game is started', () => {
        assert.fail();
    });

    it('should add selected card and tell others', () => {
        assert.fail();
    });

    it('should open confirmation dialog for back to lobby', () => {
        assert.fail();
    });
});
