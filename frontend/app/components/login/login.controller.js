'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('arcaneOasis')
  .controller('LoginController', LoginController);

LoginController.$inject = ['authService', '$state', 'toastr'];

function LoginController (authService, $state, toastr) {
  const vm = this;

  vm.login = login;

  function login () {
    authService.login(vm.username, vm.password)
      .then(() => {
        //if you logged in correctly, go to the page that
        //is they originally asked for OR the lobby
        if (!$state.returnToState) {
          $state.returnToState = 'lobby';
        }
        $state.go($state.returnToState, $state.returnToStateParams, {
          location: 'replace',
        });
        event.preventDefault();
      })
      .catch(() => {
        //login didnt work - show something?
        console.log('login didnt work');
        toastr.error('Failed to log in');
      });
  }
}
