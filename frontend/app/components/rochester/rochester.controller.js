'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('arcaneOasis')
  .controller('RochesterController', RochesterController);

RochesterController.$inject = ['rochesterDraft', '$controller', '$scope'];

function RochesterController(rochesterDraft, $controller, $scope) {
  const vm = this;
  let yourTurn = false;

  vm.onCardClick = onCardClick;
  vm.cardsTaken = [];

  $controller('BaseDraftController', {vm, $scope});

  $scope.$on('new turn player', (event, turnPlayer) => {
    vm.currentPlayer = turnPlayer;
    yourTurn = vm.username === turnPlayer;
  });

  $scope.$on('card taken', (event, cardTaken) => {
    const cardInPack = vm.pack.some((card) => cardTaken === card.name);

    if (cardInPack) {
      vm.cardsTaken.push(cardTaken);
      vm.pack = vm.pack.filter((card) => card.name !== cardTaken);
    }
  });

  function onCardClick (card) {
    if (yourTurn) {
      vm.cardClick(angular.copy(card));
    }
  }

}
