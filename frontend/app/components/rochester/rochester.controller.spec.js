'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals sinon, inject, assert*/

const angular = require('angular');
require('angular-mocks');

describe('rochesterController', () => {
    let $httpBackend;
    let socket;
    let vm;
    let cardClickSpy;

    const PLAYER_NAME = 'player';

    beforeEach(angular.mock.module('arcaneOasis', ($controllerProvider, $urlRouterProvider) => {
        cardClickSpy = sinon.spy();
        const BaseDraftController = (baseVm) => {
            baseVm.username = PLAYER_NAME;
            baseVm.pack = [
                {name:'c1'},
                {name:'c2'},
            ];
            baseVm.cardClick = cardClickSpy;
        };

        BaseDraftController.$inject = ['vm', 'socket', 'dialogService', '$stateParams', 'draftService'];

        $controllerProvider.register('BaseDraftController', BaseDraftController);

        $urlRouterProvider.deferIntercept();
    }));

    beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');

        vm = $injector.get('$controller')('RochesterController', {
            '$controller': $injector.get('$controller'),
            'socketFact' : mockSocketFactory(),
        });
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    const mockSocketFactory = () => {
        const events = {};

        socket =  {
            on: (eventName, func) => {
                events[eventName] = func;
            },
            emit: (eventName, data) => {
                events[eventName] && events[eventName](data);
            },
        };

        return {
            createSocket: () =>  socket,
        };
    };

    it('should change the current player on the "new turn player" event', () => {
        const p1 = 'p1';
        const p2 = 'p2';

        socket.emit('new turn player', p1);
        assert.equal(p1, vm.currentPlayer);

        socket.emit('new turn player', p2);
        assert.equal(p2, vm.currentPlayer);
    });


    it('should remove the selected card from the pack', () => {
        vm.cardsTaken = [];
        vm.pack = [
            {name:'c1'},
            {name:'c2'},
        ];

        socket.emit('card taken', {cardName:'c1'});
        assert.deepEqual(vm.cardsTaken, [{cardName:'c1'}]);
        assert.deepEqual(vm.pack, [{name:'c2'}]);

        //a card not in the pack should not change the pack/cardsTaken
        socket.emit('card taken', {
            cardName: 'notin',
        });
        assert.deepEqual(vm.cardsTaken, [{cardName:'c1'}]);
        assert.deepEqual(vm.pack, [{name:'c2'}]);
    });


    it('should move a clicked pack card to selected', (done) => {
        const card = {
            cardName: 'card1',
        };
        const playerName = 'player1';
        vm.pack = [card];
        vm.userName = playerName;

        //you can only pick a card when its your turn
        socket.emit('new turn player', playerName);

        vm.cardClick = (selectedCard) => {
            assert.deepEqual(selectedCard, card);
            done();
        };

        vm.onCardClick(card);

    });

    it('should not move a clicked card if its not your turn', () => {
        const card = {
            cardName: 'card1',
        };
        const playerName = 'player1';
        vm.pack = [card];
        vm.userName = playerName;

        //its someone elses turn, so you cant pick a card
        socket.emit('new turn player', playerName+'asdsdada');

        vm.cardClick = () => {
            assert.fail();
        };

        vm.onCardClick(card);
    });

});
