'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('boosterController', () => {
    let $httpBackend;
    let socketFact;
    let vm;

    const mockSocketFactory = () => {
        return {
            createSocket: () =>  {
                return {
                    on: () => {},
                    emit: () => {},
                };
            },
        };
    };

    beforeEach(angular.mock.module('arcaneOasis', ($controllerProvider, $urlRouterProvider) => {
        const BaseDraftController = (baseVm) => {
            baseVm.cardClick = () => {
            };
        };

        BaseDraftController.$inject = ['vm', 'socket', 'dialogService', '$stateParams', 'draftService'];

        $controllerProvider.register('BaseDraftController', BaseDraftController);

        //required otherwise there are requests for .html
        //due some internals of how router-ui does some things
        ////http://stackoverflow.com/a/26613169/6304194
        $urlRouterProvider.deferIntercept();
    }));


    beforeEach(inject(($injector) => {
        $httpBackend = $injector.get('$httpBackend');

        socketFact = mockSocketFactory();

        vm = $injector.get('$controller')('BoosterController', {
            '$controller': $injector.get('$controller'),
            'socketFact' : socketFact,
        });
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    it('should take the card and clear the pack', () => {
        vm.onCardClick('c1');
        assert.deepEqual(vm.pack, []);
    });
});
