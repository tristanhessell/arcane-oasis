'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('arcaneOasis')
  .controller('BoosterController', BoosterController);

BoosterController.$inject = ['$controller', '$scope'];

function BoosterController ($controller, $scope) {
  const vm = this;

  $controller('BaseDraftController', {vm, $scope});

  vm.onCardClick = (card) => {
    vm.cardClick(angular.copy(card));
    vm.pack = [];
  };
}
