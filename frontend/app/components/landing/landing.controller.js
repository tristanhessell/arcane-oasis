'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('arcaneOasis')
    .controller('LandingController', LandingController);

LandingController.$inject = [''];

function LandingController () {}
