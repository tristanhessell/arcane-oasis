'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('arcaneOasis')
  .controller('SignupController', SignupController);

SignupController.$inject = ['signupService', '$state', 'toastr'];

function SignupController (signupService, $state, toastr) {
  const vm = this;

  vm.username = '';

  vm.checkUsername = checkUsername;
  vm.join = join;

  function join () {
    const {password, username} = vm;

    signupService.signUp({
      username,
      password,
    })
      .then(() => {
        //if it works, then go to the login page
        toastr.success('You may now log in')
        $state.go('lobby', {}, {
          location: 'replace',
        });
      })
      .catch((error) => {
        toastr.error('Could not sign up');
        console.log(error);
      });
  }

  function checkUsername () {
    const {username} = vm;

    signupService.checkUsername(username)
      .then((found) => {
        console.log(found)
        vm.usernameTaken = found;
      })
  }
}
