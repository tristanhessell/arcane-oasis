'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('arcaneOasis')
  .controller('AddSetController', AddSetController);

AddSetController.$inject = ['$state', 'dialogService'];

function AddSetController ($state, dialogService) {
  const vm = this;

  vm.toSets = toSets;
  vm.loadList = loadList;
  vm.saveList = saveList;

  function toSets () {
    $state.go('sets', {}, {
      location: 'replace',
    });
  }

  //opens up a dialog that shows a list of all the sets you have made
  //dialog doesnt close until the list has been loaded
  function loadList () {
    const params = {
      listName: vm.listName,
      gameType: 'ygo',
      draftType: 'cube',
    };

    dialogService
      .openLoad(params)
      .result.then((listObj) => {
        vm.listName = listObj.name;
        vm.list = listObj.list.map((card) => card.name);
      });
  }

  //opens a dialog that asks what you want to call the list
  //dialog doesnt close until the list has been saved and acked
  function saveList () {
    //need to convert card strings into objects
    const list = vm.list.map((card) => {
      return {
        name: card,
      };
    });

    dialogService
      .openSave({
        listName:vm.listName,
        list,
        gameType: 'ygo',
        draftType: 'cube',
      }).result.then((listObj) => {
        vm.listName = listObj.name;
      });
  }

}
