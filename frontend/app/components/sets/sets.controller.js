'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');
const urlMap = require('../../imageResource.js');

const cardColours = require('../../YGOCardDecorator.js');

angular.module('arcaneOasis')
  .controller('SetsController', SetsController);

SetsController.$inject = ['configData', 'listService', '$state', '$scope', 'dialogService'];

function SetsController (configData, listService, $state, $scope, dialogService) {
  const vm = this;

  vm.cards = [];
  vm.viewType = 'List';

  vm.cardHover       = cardHover;
  vm.getColourStr    = getColourStr;
  vm.getTextColour   = getTextColour;
  vm.loadList        = loadList;
  vm.toLobby         = toLobby;
  vm.toAddSet        = toAddSet;

  vm.gameTypes  = configData.gameTypes;
  vm.draftTypes = configData.draftTypes;

  vm.draftType  = vm.draftTypes[0];
  vm.gameType   = vm.gameTypes[0];

  function cardHover (card) {
    vm.hoveredURL = `${urlMap(vm.gameType)}/${encodeURIComponent(card.name)}`;
  }

  //linear gradient doesnt work with only one colour
  function getColourStr (card) {
    const colourArr = cardColours.getBackgroundColours(card);

    if (colourArr.length === 1) {
      return `${colourArr[0]}`;
    }

    return `linear-gradient(90deg, ${colourArr.join()})`;
  }

  function getTextColour (card) {
    return cardColours.getTextColour(card);
  }

  //opens up a dialog that shows a list of all the sets you have made
  //dialog doesnt close until the list has been loaded
  function loadList () {
    const params = {
      listName: vm.listName,
      gameType: 'ygo',
      draftType: vm.draftType,
    };

    dialogService
      .openLoad(params)
      .result.then((listObj) => {
        vm.listName = listObj.name;
        vm.list = listObj.list.map((card) => card.name);
      });
  }

  function toLobby () {
    $state.go('lobby', {}, {
      location: 'replace',
    });
  }

  function toAddSet () {
    $state.go('addSet', {}, {
      location: 'replace',
    });
  }
}
