'use strict';
/*eslint-env commonjs, es6, mocha*/
/*globals sinon, assert, inject*/

const angular = require('angular');
require('angular-mocks');

describe('SetsController', () => {
  let getController;
  let configService;
  let cardService;
  let scope;
  let $q;

  beforeEach(angular.mock.module('arcaneOasis', ($urlRouterProvider) => {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(($injector) => {
    cardService = $injector.get('cardService');
    configService = $injector.get('configService');

    scope = $injector.get('$rootScope').$new();
    $q = $injector.get('$q');

    const config = {
      draftTypes: ['cube','booster'],
      draftFormats: ['rochester', 'booster'],
      gameTypes: ['ygo'],
    };

    const configDeferred = $q.defer();
    configDeferred.resolve(config);

    sinon.stub(configService, 'getConfig')
      .returns(configDeferred.promise);

    const listDeferred = $q.defer();
    const lists = [1,2,3,4];

    listDeferred.resolve(lists);

    sinon.stub(cardService, 'getLists')
      .returns(listDeferred.promise);

    getController = () => {
      const vm = $injector.get('$controller')('SetsController', {
        configService,
        cardService,
      });
      scope.$apply();

      return vm;
    };
  }));

  describe('On initialisation', () => {
    it('should request the lists from the server', () => {
      const vm = getController();
      assert.deepEqual(vm.lists, [1,2,3,4]);
    });
  });

  describe('getCards()', () => {
    it('should request the cards from the server', () => {
      const expectedCards = [1,2,3,4];
      const deferred = $q.defer();

      deferred.resolve(expectedCards);

      sinon.stub(cardService, 'getCards')
        .returns(deferred.promise);

      const vm = getController();

      vm.getCards();
      scope.$apply();

      assert.deepEqual(vm.cards, expectedCards);
    });
  });

  describe('cardHover()', () => {
    it('should change the hovered card when called', () => {
      const vm = getController();
      const card = {
        name: 'tristan',
      };

      const expectedURL = `https://yugiohprices.com/api/card_image/${card.name}`;
      vm.hoveredURL = 'memer';
      vm.cardHover(card);

      assert.equal(vm.hoveredURL, expectedURL);
    });
  });
});
