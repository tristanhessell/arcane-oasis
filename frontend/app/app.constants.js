'use strict';
/*eslint-env browser, es6, commonjs*/

const angular = require('angular');

angular.module('arcaneOasis')
  .constant('configData', require('../../config.js'));