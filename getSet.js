'use strict';
/*eslint-env node, es6*/

//TODO move this somewhere appropriate/remove it when
const request = require('request-promise-native');

module.exports = (card) => {
  return request.get({
    uri: `https://yugiohprices.com/api/card_versions/${card}`,
    json: true,
  })
    .then((resp) => {
      const cardTag = resp.data[0].print_tag;
      const setName = cardTag.split('-')[0];

      return setName;
    });
}
